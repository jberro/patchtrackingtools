// PatchTrackingTools.ijm
//
// Toolsets patch tracking and molecule counting using quantitative spinning disc fluorescence microsocpy
//
// Julien Berro 2010-2015
// Yale University
var version="2015.12.16";

// Known problems:
//  - Sometimes ImageJ throws random Java exceptions when duplicating, cropping,etc images. They seem to have no consequences


//------------------------------------------------------------------------------
//--------- Initialisation of the toolset and Parameter defaults ---------------
//------------------------------------------------------------------------------

var batchMode=true, diameter = 7, dia2 = 1, nbSliceMeasured=3; //VolodiaStyle=false,
var sizeWindowRecentering=2*round(diameter*3/4)+1; //2*diameter+1; //15// search window for recentering roi. NOTE: Has to be odd integer
var ROIFileNamepattern="RoiSet", timeDelay=1;
var generateFileIntensities=true, generateFileDisplacements=false, generateFileMSDs=false, generateFilePositions=true,generateFileConcvsDist=true; 
var positionsInConcVsDist=false;
var distTrack = 0, maxGaussFittingRuns=0; //maxGaussFittingRuns=3; 
var diameterIncrease=1, exposureTimeCorrection=1000, calibrationFactor=1;
var offSetXReducedSlices=0, offSetYReducedSlices=0, widthReducedSlices=0, heightReducedSlices=0;
var mainImageName="",mainImageId=-1, nbSlices=5, sliceWindowIDArray=0;
var GlobalPhotoBleachingRate=-1;//0.027;
var defaultWindowSizeForMSD=-1; // -1 to take all the data 
var intensities=newArray(1), percentages=newArray(1), backgrounds=newArray(1);

var IJSynchronizationLag=200; //time to wait for synchronization of macros

var photoBleachingCorrection=0, photoBleachingSet=false;

var fileSep=File.separator;
var OS=getOS();

var dbg=false;

var playing=false, imagePerSec=5;

var getPosDataFromMaxSlice=true;

var mainChannelNumber=0;

var beginFrame=1, endFrame=10,	scaleMontage=5; // Default parameters for the montage macro

var tmpflag=0;

var iQ10Version=false;// default is iQ2 version

var patternChannelName="_GFP_";

var LUTnb=1; // 1: Grays; 2: inverted grays; 3: fire

// Saved display setup
var redsSetup1, greensSetup1, bluesSetup1;
var minIntensitySetup1, maxIntensitySetup1;

//var photobleachingEq="y = a+b*exp(-c*x)";
var photobleachingEq="y = a*exp(-b*x)";

var modifyRenderDeslice=1;

setOption("QueueMacros", true);

// Deprecated
var pcThreshold=0.10;
var modeThreshold="MaxMin"; // Max/min +/- percentage
//var modeThreshold="AveragePercentage"; //Average +/- percentage
//var modeThreshold="AverageSD"; //Average +/- 2 SD

//For data alignment
var ON_WHOLE_TRACK=0;
var ON_BEGINNING=1;
var ON_END=2;

var alignment=ON_WHOLE_TRACK;
var nbPointForAlignment=6;

var extraTimePointsEachSide=2; // 5 Extra points for data alignment

//For montage all slices
var makeAllSliceMontageWhenDeslicing=true; //Makes montage during deslicing [d]
var scaleMontageAllSlices=2;
var plusMinusTime=10;

macro "Unused Tool-1 - " {}  // leave empty toolbar slot

macro "Brightness & Contrast/Color Balance  Dialog Action Tool - C000D0dD0eD13D14D15D16D17D18D19D1aD1bD1cD1dD20D21D22D23D24D25D26D27D28D29D2aD2bD2cD2fD33D34D35D36D37D38D39D3aD3bD3eD3fD47D48D49D4aD4dD4eD4fD5cD5dD5eD5fD6bD6cD6dD6eD6fD7dD7eD7fD8dD8eD8fD9dD9eD9fDaeDafDbeDbfDcfDdfDefDffC000C111C222C333C444D1fD2eD3dD4cD5bD6aD79D87D88D96Da5Db4Dc3Dd2De1Df0C444C555C666C777C888C999CaaaCbbbCcccD0fD1eD2dD3cD4bD5aD69D78D86D95Da4Db3Dc2Dd1De0CcccCdddCeeeCfff"{

	if(bitDepth() ==24)
		{run("Color Balance...");}
	else{	
		run("Brightness/Contrast...");}

}

macro "ROI Manager Action Tool - C902D00D01D02D10D11D12D20D21D22D30D31D32D40D41D42D50D51D52D60D61D62D70D71D72D80D81D82D90D91D92Da0Da1Da2Db0Db1Db2Dc0Dc1Dc2CfffD03D04D05D06D07D08D09D0aD0bD0cD0dD0eD0fD13D14D15D16D17D18D19D1aD1bD1cD1dD1eD1fD23D24D25D26D27D28D29D2aD2bD2cD2dD2eD2fD33D34D35D36D37D38D39D3aD3bD3cD3dD3eD3fD43D44D45D46D47D48D49D4aD4bD4cD4dD4eD4fD53D54D55D56D57D58D59D5aD5bD5cD5dD5eD5fD63D64D65D66D67D68D69D6aD6bD6cD6dD6eD6fD73D74D75D76D77D78D79D7aD7bD7cD7dD7eD7fD83D84D85D86D87D88D89D8aD8bD8cD8dD8eD8fD93D96D99D9cD9fDa3Da6Da9DacDafDb3Db6Db9DbcDbfDc3Dc6Dc9DccDcfDd3Dd6Dd9DdcDdfDe3De6De9DecDefDf3Df6Df9DfcDffCbbbD94D95D97D98D9aD9bD9dD9eDa4Da5Da7Da8DaaDabDadDaeDb4Db5Db7Db8DbaDbbDbdDbeDc4Dc5Dc7Dc8DcaDcbDcdDceDd4Dd5Dd7Dd8DdaDdbDddDdeDe4De5De7De8DeaDebDedDeeDf4Df5Df7Df8DfaDfbDfdDfeCf00Dd0Dd1Dd2De0De1De2Df0Df1Df2"{

run("ROI Manager...");
}

macro "Unused Tool-1 - " {}  // leave empty toolbar slot

macro "PatchTracking Toolbar . . . Action Tool - C000D15D24D25D33D34D35D43D44D45D46D47D48D49D4aD4bD4cD53D54D55D56D57D58D59D5aD5bD5cD63D64D65D73D74D75Da9DaaDabDacDb3Db4Db5Db6Db7Db8Db9DbaDbbDbcDc9DcaDcbDccCfffD00D01D02D03D04D05D06D07D08D09D0aD0bD0cD0dD0eD0fD10D11D12D13D14D16D17D18D19D1aD1bD1cD1dD1eD1fD20D21D22D23D26D27D28D29D2aD2bD2cD2dD2eD2fD30D31D32D36D37D38D39D3aD3bD3cD3dD3eD3fD40D41D42D4dD4eD4fD50D51D52D5dD5eD5fD60D61D62D66D67D68D69D6aD6bD6cD6dD6eD6fD70D71D72D76D77D78D79D7aD7bD7cD7dD7eD7fD80D81D82D83D84D85D86D87D88D89D8aD8bD8cD8dD8eD8fD90D91D92D93D94D95D96D97D98D99D9aD9bD9cD9dD9eD9fDa0Da1Da2Da3Da4Da5Da6Da7Da8DadDaeDafDb0Db1Db2DbdDbeDbfDc0Dc1Dc2Dc3Dc4Dc5Dc6Dc7Dc8DcdDceDcfDd0Dd1Dd2Dd3Dd4Dd5Dd6Dd7Dd8Dd9DdaDdbDdcDddDdeDdfDe0De1De2De3De4De5De6De7De8De9DeaDebDecDedDeeDefDf0Df1Df2Df3Df4Df5Df6Df7Df8Df9DfaDfbDfcDfdDfeDff"{
	//macro "Toolbar [Z]"{
	run("Action Bar","/macros/PatchTrackingBar.txt");
	exit();
}

macro "Circle Tool [c]- C00cO11ee" {
	checkMainImageName();
        curImageName=getTitle();
	getCursorLoc(x, y, z, flags);
        run("Specify...", "width="+diameter+" height="+diameter+" x="+(x+1)+" y="+(y+1)+" oval centered");
        registrationRecentering();
	roiManager("Add");
	numROIs=roiManager("count");
	roiManager("select",numROIs-1);
	selectWindow(curImageName);
	if (nImages>1){
		getSlicesIntensities(nbSlices,offSetXReducedSlices,offSetYReducedSlices,-1,true);
	}	
}

macro "About PatchTrackingTools... Action Tool - Cfc4o11dd - C902O11dd -C902T5c0c?"{
	txt="PatchTrackingTools version "+version+"\n";
	txt+="Julien Berro \n";
	txt+="Yale University \n";
	txt+="Ask questions or report glitches to julien.berro@yale.edu"; 
	waitForUser(txt);
	}


/********************************************************* 
******* Andor iQ2 movies reorganization ****************
***********************************************************/
macro '** Andor iQ2 movies reorganization **'{} 

function myCopy(oldPath, newPath){
			open(oldPath);
			save(newPath);
			wait(500);
			run("Close");
		}
		
macro 'Split multi-field movies'{ // Not maintained anymore	
	//setOption("QueueMacros", true);
	dir=getDirectory("Select Directory for Renaming:");
	curFiles=getFileList(dir);
	nFiles=curFiles.length;
	for(i=0;i<nFiles;i++){
		if(endsWith(curFiles[i],".txt")){
			txtFileName=curFiles[i];
			pathTxt=dir+fileSep+txtFileName;
			strFile=File.openAsString(pathTxt);
			lines=split(strFile,"\n");
			name=substring(txtFileName,0,lengthOf(txtFileName)-4);
			tifFileName=name+".tif";
			pathTif=dir+fileSep+tifFileName;
			//print(pathTif);
			nLines=lengthOf(lines);
			j=1;
			cnt=0;
			List.clear();
			while (j<nLines){ //Figure out if it is a mutli-field protocol
				if (matches(lines[j],"^\t*(Move XY).*")){
					nextLine=split(lines[j+1],"\t ");
					lengthMovieCurField=nextLine[3];
					List.set(cnt,lengthMovieCurField);
					cnt++;
					j++;
				}
				j++;
			}
			if (cnt>0){// It is a multi-field protocol
				if(File.exists(pathTif)){// All the fields are in the same file : make 1 tif and 1 txt file per field
					print("Splitting into "+cnt+" fields "+pathTif);
					print("In progress... Still "+(nFiles-i+1)+" files to analyze!");
					run("Bio-Formats Importer", "open=["+pathTif+"] view=Hyperstack stack_order=XYCZT");
					tifId=getImageID();
					Stack.getDimensions(tmpwidth, tmpheight, tmpchannels, tmpslices, tmpframes);
					first=1;
					last=1;
					for (j=0;j<cnt;j++){
						newNameTif=name+"-field"+j+".tif";
						newNameTxt=name+"-field"+j+".txt";
						//newPathTxt=dir+fileSep+newNameTxt;
						//newPathTif=dir+fileSep+newNameTif;
						newPathTxt=dir+newNameTxt;
						newPathTif=dir+newNameTif;
						//copy(pathTxt, newPathTxt, 1);
						myCopy(pathTxt, newPathTxt);
						wait(500);
						last=first+List.get(j)-1;
						run("Duplicate...", "title=["+tifFileName+"] duplicate slices=1-"+tmpslices+" frames="+first+"-"+last);
						curId=getImageID();
						save(newPathTif);
						//wait(500);
						first=first+List.get(j);
						selectImage(curId);
						close();						
					}
					selectImage(tifId);
					close();
					//myGarbageCollector();
				}
				else{// The fields are already in separate files: Create one txt file for each movie
					
					for(j=0;j<cnt;j++){
						newTxtFileName=name+"_t"+IJ.pad(j,4)+".txt";
						newPathTxt=dir+fileSep+newTxtFileName;
						myCopy(pathTxt, newPathTxt);
						print("Creating the text file "+newTxtFileName);
						print("In progress... Still "+(nFiles-i+1)+" files to analyze!");
					}
					File.delete(pathTxt);
				}
			}
			
		}
	}
		print("All fields split! ");
}

macro 'Reorganize movies from microscope'{ // Not maintained anymore
	if (iQ10Version==false){ // using iQ2 data
		run("ReorganizeFilesiQ2 ");
	}
	else{
		dir=getDirectory("Select Directory for Renaming:");
		curFiles=getFileList(dir);
		nFiles=curFiles.length;
		for(i=0;i<nFiles;i++){
			if(endsWith(curFiles[i],".txt")){
				fileName=curFiles[i];
				path=dir+fileSep+fileName;
				strFile=File.openAsString(path);
				lines=split(strFile,"\n");
				name=substring(fileName,0,lengthOf(fileName)-4);
				nLines=lengthOf(lines);
				j=1;
				found=0;
				while (found==0&&j<nLines)
				{
					if (startsWith(lines[j],"Date=")){
						found=1;
					}
					else{
						j++;
					}
				}
				lineDate=split(lines[j],"=");
				lineTime=split(lines[j+1],"=");		
				date=split(lineDate[1],"/");
				time=split(lineTime[1],"[: ]");
				newName=""+add0(date[2])+add0(date[1])+add0(date[0])+" "+time[3]+" "+add0(time[0])+add0(time[1])+add0(time[2])+" "+name;
				File.makeDirectory(dir+fileSep+newName);
				File.rename(path,dir+fileSep+newName+fileSep+newName+".txt");
				File.rename(substring(path,0,lengthOf(path)-3)+"tif",dir+fileSep+newName+fileSep+newName+".tif");
			}
		}
		print("Images reorganized! "+dir);
		
	}
}

macro 'Combine Channels'{ // Not maintained anymore
	setBatchMode(true); // Has to be done outside batch mode unless it doesn't work (as of ImageJ 1.43u)
	dir=getDirectory("Select Directory for Image Conversion:");
	
	curFilesFolders=getFileList(dir);
	
	var sep=File.separator();
	separator=sep+sep;
	
	if(isOpen("Log")){
		selectWindow("Log");
		run("Close");
	}
	
	for(i=0;i<curFilesFolders.length;i++){
		if(File.isDirectory(dir+curFilesFolders[i])){
			print(" "+(i+1)+"/"+curFilesFolders.length+" Exploring "+curFilesFolders[i]);
			protocolTxtFileName=substring(curFilesFolders[i],0,lengthOf(curFilesFolders[i])-1)+".txt";
			localPath=dir+curFilesFolders[i];
			if (File.exists(localPath+protocolTxtFileName)){
				protocolList=findProtocols(localPath,protocolTxtFileName);
				nbChannels=0;
				commonSliceNb=0;
				for (j=0;j<protocolList.length;j++)
				{
					if (!matches(protocolList[j],".*((DIC)|(Current)).*")){
						//if protocol is not DIC, open the image stacks in the same order as in the protocol
						curFileList=getFileList(localPath);
						for (k=0;k<curFileList.length;k++){
							regexp=".*("+protocolList[j]+")[(]"+j+"[)][0-9]*(\.tif)";
							found=false;
							if (matches(curFileList[k],regexp)){
								print(curFileList[k]+" found");
								nbChannels++;
								curFileToOpen=localPath+curFileList[k];
								found=true;
							}
						}
						if (!found){ // if no tif file found, maybe search the folders
							regexp=".*("+protocolList[j]+")[(]"+j+"[)][0-9]*.*";
							for (k=0;((k<curFileList.length)&&(!found));k++){
								//Finds folder
								localPath2=localPath+curFileList[k];
								if (matches(curFileList[k],regexp)&&File.isDirectory(localPath2)){ // if folder matching channel name is found 
									curFileList2=getFileList(localPath2);
									regexp2=".*("+protocolList[j]+")[(]"+j+"[)][0-9]*(\.tif)";
									for (k2=0;k2<curFileList2.length;k2++){// search for tif file in the folder matching channel name
										if (matches(curFileList2[k2],regexp2)){
											print(curFileList2[k2]+" found");
											nbChannels++;
											curFileToOpen=localPath2+curFileList2[k2];
											found=true;
										}
									}
								}
							}
						}
						if (found){
							run("Bio-Formats Importer", "open=["+curFileToOpen+"] view=Hyperstack stack_order=XYCZT");
							Stack.getDimensions(curWidth, curHeight, curChannels, curSlices, curFrames);
							commonSliceNb=maxOf(curSlices,commonSliceNb);
						}
						else{
							print("File Not Found for channel "+protocolList[j]);
						}						
					}
				}
				for (j=1; j<=nImages; j++) {
					selectImage(j);
					Stack.getDimensions(curWidth, curHeight, curChannels, curSlices, curFrames);
					if(curSlices<commonSliceNb){
						nbSlicesToAddBefore=floor(commonSliceNb-curSlices)/2;
						nbSlicesToAddAfter=commonSliceNb-curSlices-nbSlicesToAddBefore;
						print("nbSlicesToAddBefore="+nbSlicesToAddBefore);
						print("nbSlicesToAddAfter="+nbSlicesToAddAfter);
						print("curSlices="+curSlices);
						print("commonSliceNb="+commonSliceNb);
						addSlicesToSingleChannelHyperstack(nbSlicesToAddBefore,nbSlicesToAddAfter);
					}
				}
				concatFileName=substring(curFilesFolders[i],0,lengthOf(curFilesFolders[i])-1)+".tif";
				run("Concatenate ", "all_open title=["+concatFileName+"]");
				run("Stack to Hyperstack...", "order=xyztc channels="+nbChannels+" slices="+commonSliceNb+" frames="+curFrames+" display=Grayscale");
				if (nbChannels>1){
					run("Make Composite");
				}
				save(localPath+concatFileName);
				run("Close all [X]");
				myGarbageCollector();
			}
			else{
				print(" Protocol file "+protocolTxtFileName+" not found");
			}
		}
	}
	print("All done concatenating iQ2 files");
}


/***************************************** 
******* Movie Corrections ****************
*******************************************/
macro '** Movie Corrections **'{} 

macro 'Generate Correction Files'{
// 1. Averages camera noise from 1 movie, saves the file AverageCameraNoise.tif in the experiment root directory
	// 2. Substracts camera noise from each slice of each Uneven illumination stack. 
	//    Normalizes the intensity of each slice to the max value.
	//    Average all the normalized files
	//    Saves the file AverageUnevenIllumination.tif in the experiment root directory
	
	// camera noise
	var sep=File.separator();
	separator=sep+sep;
	fileCamNoise=File.openDialog("Select a file with Camera Noise movie");
	fileUneven=File.openDialog("Select a file with Uneven Illumination movie:");
	dirExperiment=File.getParent(File.getParent(fileCamNoise));
		
	run("Bio-Formats Importer", "open=["+fileCamNoise+"] view=Hyperstack stack_order=XYCZT"); 
	
	getDimensions(width, height, channels, slices, frames);
	if (channels>1){
		channelNumber=setupChannel(fileCamNoise);
		Stack.setChannel(channelNumber); 
		run("Reduce Dimensionality...", "  slices frames");
	}
	
	run("32-bit");
	run("Z Project...", "start=1 stop="+nSlices+" projection=[Average Intensity]");
	
	averageCamNoiseFileName="AverageCameraNoise.tif";
	rename(averageCamNoiseFileName);
	
	save(dirExperiment+sep+averageCamNoiseFileName);
	print(dirExperiment+sep+averageCamNoiseFileName+" saved successfully!");
	
	run("Set Measurements...", "area mean min centroid center bounding stack redirect=None decimal=5");
	
	// Uneven Illumination
	run("Bio-Formats Importer", "open=["+fileUneven+"] view=Hyperstack stack_order=XYCZT"); 
	getDimensions(width, height, channels, slices, frames);
	if (channels>1){
		channelNumber=setupChannel(fileUneven);
		Stack.setChannel(channelNumber);
		run("Reduce Dimensionality...", "  slices frames");
		tmpName=getTitle(); // necessary to select the new file created
	}
	run("32-bit");
	run("Z Project...", "start=1 stop="+nSlices+" projection=[Average Intensity]");
	curSumImageName=getTitle();
	run("32-bit");
	run("Image Calculator...", "image1=["+curSumImageName+"] operation=Subtract image2=AverageCameraNoise.tif");
	curImagetmp=getTitle();
	run("Measure");
	maxIntensity=getResult("Max",nResults-1);
	run("Divide...", "value="+maxIntensity+" slice");
	AverageUnevenIlluminationFileName="AverageUnevenIllumination.tif";
	save(dirExperiment+sep+AverageUnevenIlluminationFileName);
	rename(AverageUnevenIlluminationFileName);
	print(dirExperiment+sep+AverageUnevenIlluminationFileName+" saved successfully!");
	print("Done saving averaged uneven illumination and camera noise!");
	
	selectWindow("AverageUnevenIllumination.tif");
	run("Close All");
	myGarbageCollector();
}

macro 'Generate Neutral Correction Files'{
	
	// camera noise
	var sep=File.separator();
	separator=sep+sep;
	sampleFile=File.openDialog("Select a sample file:");
	dirExperiment=File.getParent(File.getParent(sampleFile))+sep;
	run("Bio-Formats Importer", "open=["+sampleFile+"] view=Hyperstack stack_order=XYCZT");
	sampleImageName=getTitle();
	run("Duplicate...", "title=["+sampleImageName+"]");
	run("Multiply...", "value=0");
	run("Add...", "value=1");
	run("32-bit");
	run("Save", "save=["+dirExperiment+"AverageUnevenIllumination.tif]");
	run("Duplicate...", "title=["+getTitle()+"]");
	run("Multiply...", "value=100");
	run("32-bit");
	run("Save", "save=["+dirExperiment+"AverageCameraNoise.tif]");
	run("Close all [X]");
}

macro 'Correct all movies'{
	// Select the "Experiment folder" with all the data in subfolders (1 series per subfolder)
	// Renames with the names that were given originally
	
	setBatchMode(batchMode);
	dir=getDirectory("Select Directory for Image Conversion:");
	curFilesFolders=getFileList(dir);
	
	var sep=File.separator();
	separator=sep+sep;
	
	success=0;
	
	open(dir+"AverageCameraNoise.tif");
	open(dir+"AverageUnevenIllumination.tif");
	
	// // Rename folder if contains .nam file
	nbDir=0;
	for(i=0;i<curFilesFolders.length;i++){
		if(File.isDirectory(dir+curFilesFolders[i])){
			nbDir++;
		}
	}
	cntDir=0;
	for(i=0;i<curFilesFolders.length;i++){
		slices=0;
		if(File.isDirectory(dir+curFilesFolders[i])){
			cntDir++;
			print(cntDir+"/"+nbDir+" Exploring "+curFilesFolders[i]);
			j=0;
			tifFileFound=0;
			tifFileName=substring(curFilesFolders[i],0,lengthOf(curFilesFolders[i])-1)+".tif";
			curDirPath=dir+curFilesFolders[i];
			if (!File.exists(curDirPath+tifFileName)){//Multi channel movie
				tifFiles=getFileList(dir+curFilesFolders[i]);
				j=0;
				while(tifFileFound==0&&j<tifFiles.length){
					if (matches(tifFiles[j],"^.*("+patternChannelName+").*\.tif")){
						tifFileName=tifFiles[j];
						tifFileFound=1;
					}
					j++;
				}
				
			}
			else{
				tifFileFound=1;
			}
			curDirPath=dir+curFilesFolders[i];
			DIC=0;
			if (tifFileFound){
				run("Bio-Formats Importer", "open=["+curDirPath+tifFileName+"] view=Hyperstack stack_order=XYCZT");
				//run("Bio-Formats Importer", "open=["+dir+curFilesFolders[i]+subFiles[j]+"] view=Hyperstack stack_order=XYCZT"); //slower than previous line
				getDimensions(width, height, channels, nbSlices, nbFrames);
				if (DIC==0){
					imageName1=getTitle();
					run("32-bit");
					if(!Stack.isHyperstack){
						run("Stack to Hyperstack...", "order=xyczt(default) channels=1 slices="+nbSlices+" frames="+nbFrames+" display=Color");
						print("ImageJ internal error but problem corrected!");
					}
					if (channels>1){
						run("Make Composite");
					}
					
					imageCalculator("Subtract stack", imageName1,"AverageCameraNoise.tif");
					imageName2=getTitle();
					imageCalculator("Divide stack", imageName2,"AverageUnevenIllumination.tif");					
					saveAs("Tiff",dir+curFilesFolders[i]+substring(tifFileName,0,lengthOf(tifFileName)-4)+"HS.tif");
					getDimensions(width, height, channels, slices, frames);
					imageName3=getTitle();
					run("32-bit");
					run("Z Project...", "start=1 stop="+slices+" projection=[Sum Slices] all");
					imageName4=getTitle();
					selectWindow(imageName3);
					run("Close");
					selectWindow(imageName4);
					saveAs("Tiff", dir+curFilesFolders[i]+substring(tifFileName,0,lengthOf(tifFileName)-4)+"_SUM_Corrected.tif");
				}
				run("Close");
				myGarbageCollector();
			}
			else{
				print(" Tif file "+tifFileName+" not found");
			}
			
		}
	}
	selectWindow("AverageCameraNoise.tif");
	close();
	selectWindow("AverageUnevenIllumination.tif");
	close();
	setBatchMode(false);
	print("All Done!");
	myGarbageCollector();
	
}

macro "Spread ROI for Photobleaching Analysis"{
	//Spreads the current ROI to all the images of the stack and saves it as "RoiForPhotoBleaching.zip" in the image folder
	curImageName=getTitle();
	roiManager("reset");
	selectWindow(curImageName);
	dir=getDirectory("image");
	Stack.getDimensions(imWidth, imHeight, nbChannels, tmpNull, nbFrames);
	x=newArray(nbFrames);
	y=newArray(nbFrames);
	for (i=1;i<=nbFrames;i++){
		Stack.setFrame(i);
		run("Measure");
		roiManager("Add");
		tmpInd=roiManager("count")-1;
		roiManager("Select", tmpInd);
		curROIName=getInfo("roi.name");
		newROIName=curROIName+"-"+i;
		roiManager("Rename", newROIName);	
		x[i-1]=i;
		y[i-1]=getResult("Mean",nResults-1);
	}
	roiManager("Deselect");
	filename=dir+"RoiForPhotoBleaching.zip";
	roiManager("Save",filename);
	if (photobleachingEq=="y = a+b*exp(-c*x)"){
		equation = "y = a+b*exp(-c*x)";
		a0=y[nSlices-1];
		b0=y[0]-y[nSlices-1];
		c0=0.01;
		initialGuesses = newArray(a0,b0,c0);
	}
	else if (photobleachingEq=="y = a*exp(-b*x)"){
		equation = "y = a*exp(-b*x)";
		a0=y[0];
		b0=0.01;
		initialGuesses = newArray(a0,b0);
	}

	Fit.logResults;
	Fit.doFit(equation, x, y, initialGuesses);
	Fit.plot;
}



/**********************************************************
****************** Patch Finder ***************************
***********************************************************/
macro '** Patch Finder **'{} 

macro 'Start tracking [T]'{
	reduceField(0.5);
	setPhotoBleachingCorrection();
	run("Fire");
	run("Brightness/Contrast...");
	run("Enhance Contrast", "saturated=0.1");
}

macro 'Find patch candidates [I]'{
	checkMainImageName();
	selectImage(mainImageId);
	wait(IJSynchronizationLag);
	run("Select None");
	print("Starting the patch finder...");
	eval("python", File.openAsString(getDirectory("macros")+"toolsets"+fileSep+"macros for PatchTrackingTools"+fileSep+"patchFinder.ijm.py"));
}

macro 'Curate patch candidates [i]'{
	print("Starting montages...");
	Dialog.create("Patch curation");
	Dialog.addMessage("One montage will be created per Roiset whose name starts with '"+ROIFileNamepattern+"'");
	Dialog.addMessage("Press [SHIFT+B] to remove this Roiset from analysis (an underscore '_' will be added at the beginning of its name)");
	Dialog.addMessage("Press [SHIFT+H] to tag this Roiset from further modification analysis ('M_' will be added at the beginning of its name)");
	Dialog.show();
	  
	checkMainImageName();
	selectImage(mainImageId);
	idMain=getImageID();
	run("Select None");
	run("Close all except current [x]");
	myGarbageCollector();
	dir=getDirectory("image");
	getMinAndMax(minIntensity, maxIntensity);
	getLut(reds, greens, blues);
	Stack.getDimensions(imWidth, imHeight, nbChannels, nbSlices, nbFrames) 	
	list = getFileList(dir);
	//print(list.length);
	lastImgY=0;
	lastImgHeight=0;
	for (i=0; i<list.length; i++) {
        if (startsWith(list[i], ROIFileNamepattern)){
        	selectImage(mainImageId);
        	run("Duplicate...", "title=["+mainImageName+"] duplicate ");  
           	idDuplicate=getImageID();
           	roiManager("Reset");
           	roiManager("Open", dir+list[i]);
           	selectImage(idDuplicate);
			roiManager("select",roiManager("count")-1);
			Stack.getPosition(lastChannelNb, lastSliceNb, lastFrameNb);
			roiManager("select",0);
			Stack.getPosition(firstChannelNb, firstSliceNb, firstFrameNb);
			enlargeNpixels=6;
			run("Enlarge...", "enlarge="+enlargeNpixels+" pixel");
			run("Crop");  
			idCrop=getImageID();
			run("Make Montage...", "columns="+(lastFrameNb-firstFrameNb+1)+" rows=1 scale="+scaleMontageAllSlices+" first="+firstFrameNb+" last="+lastFrameNb);
			yloc=lastImgY+lastImgHeight- floor((lastImgY+lastImgHeight)/(screenHeight-lastImgHeight))*(screenHeight-lastImgHeight);
			setLocation(0, yloc);
			getLocationAndSize(lastImgX, lastImgY, lastImgWidth, lastImgHeight);
			selectImage(idDuplicate);
			close();
			rename(list[i]);
        }
     }
	
	myGarbageCollector();
	selectImage(idMain);
	print("Finished");
}

macro 'Remove bad Roisets [B]'{
	idCurrentImage=getImageID();
	windowName=getTitle();
	if (startsWith(windowName,ROIFileNamepattern)){
		selectImage(mainImageId);
		idMain=getImageID();
		dir=getDirectory("image");
		initialRoisetName=dir+windowName;
		newRoiset=windowName;
		success=0;
		while (success==0){
			newRoiset="_"+newRoiset;
			newRoisetName=dir+newRoiset;
			success=File.rename(initialRoisetName, newRoisetName); 
		}
		selectImage(idCurrentImage);
		rename(newRoiset);
	}
}

macro 'Tag Roisets to modify [H]'{
	idCurrentImage=getImageID();
	windowName=getTitle();
	if (startsWith(windowName,ROIFileNamepattern)){
		selectImage(mainImageId);
		idMain=getImageID();
		dir=getDirectory("image");
		initialRoisetName=dir+windowName;
		newRoiset=windowName;
		success=0;
		while (success==0){
			newRoiset="M_"+newRoiset;
			newRoisetName=dir+newRoiset;
			success=File.rename(initialRoisetName, newRoisetName); 
		}
		selectImage(idCurrentImage);
		rename(newRoiset);
	}
}

macro 'Save Current Roi [J]'{
	checkMainImageName();
	selectImage(mainImageId);
	run("SingleRoiSaver ");
}



/********************************************************** 
********************* Patch tracking **********************
***********************************************************/
macro '** Patch tracking **'{} 

macro 'Make montage one row per slice [j]'{
	checkMainImageName();
	dirloc=getDirectory("image");
	selectImage(mainImageId);
	idMain=mainImageId;
	Stack.getPosition(curChannelNb, curSliceNb, curFrameNb);
	roiNb=roiManager("index");
	curROIname="Current Selection";
	if (roiNb!=-1){
		curROIname=getInfo("roi.name");
	}
	else{
		getSelectionBounds(xSelect, ySelect, widthSelect, heightSelect);
	}
	
	HSImageName=substring(mainImageName,0,lengthOf(mainImageName)-18)+"HS.tif";
	open(dirloc+fileSep+HSImageName);
	idHS=getImageID();
	selectImage(idHS);	
	Stack.getDimensions(width, height, channelsNb, slicesNb, framesNb);
	if (roiNb!=-1){
		roiManager("select",roiNb);		
		enlargeNpixels=6;
		run("Enlarge...", "enlarge="+enlargeNpixels+" pixel");
	}
	else{
		run("Specify...", "width="+widthSelect+" height="+heightSelect+" x="+xSelect+" y="+ySelect+" slice="+curFrameNb);
	}
	beginFrame=maxOf(1,curFrameNb-plusMinusTime);
	endFrame=minOf(framesNb,curFrameNb+plusMinusTime);
	setBatchMode(true);
	run("Duplicate...", "title=["+HSImageName+"] duplicate ");
	duplicateHSid=idHS;
	makeMontageAllSlices(duplicateHSid,beginFrame,endFrame,scaleMontageAllSlices);
	setBatchMode(false);
	rename(curROIname);
	idMontage=getImageID();
	selectImage(idHS);
	close();
	myGarbageCollector();
}

macro 'Deslice Around ROI [d]'{
	
	/////////////////////////////////////////////////////
	//  Java code to get screen dimensions without the taskbars!
	//  Rectangle maxBounds = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
	/////////////////////////////////////////////////
	
	checkMainImageName();
	selectImage(mainImageId);
	// Uncomment next line to zoom in automatically before deslicing
	//run("To Selection");
	run("Close all except current [x]");
	wait(IJSynchronizationLag);
	myGarbageCollector();
	imageName=getTitle();
	checkMainImageName();
	wait(IJSynchronizationLag);
	
	setBatchMode(true);
	dirloc=getDirectory("image");
	getMinAndMax(minIntensity, maxIntensity);
	
	Stack.getPosition(curChannelNb, curSliceNb, curFrameNb);
	
	run("Appearance...", "  antialiased menu=12");
	getSelectionBounds(xSelect, ySelect, widthSelect, heightSelect);
	selectWindow(imageName);
	getLocationAndSize(x, y, imgWidth, imgHeight);
	// Uncomment next line to move the image in the bottom left corner before deslicing
	//setLocation(0, screenHeight-imgHeight);
	getLut(reds, greens, blues);
	HSImageName=substring(imageName,0,lengthOf(imageName)-18)+"HS.tif";
	open(dirloc+fileSep+HSImageName);
	HSImageID=getImageID();
	HSImageWindowName=getTitle();
	Stack.getDimensions(imWidth, imHeight, nbChannels, nbSlices, nbFrames) 
	makeRectangle(xSelect, ySelect, widthSelect, heightSelect);
	enlargeNpixels=8;
	run("Enlarge...", "enlarge="+enlargeNpixels+" pixel");
	offSetXReducedSlices=maxOf(xSelect-enlargeNpixels,0);
	offSetYReducedSlices=maxOf(ySelect-enlargeNpixels,0);
	wait(IJSynchronizationLag);
	run("Crop");
	rename("cropped");
	croppedHSid=getImageID();

	wait(IJSynchronizationLag);
	
	Stack.getDimensions(width, height, channelsNb, slicesNb, framesNb);

	if (makeAllSliceMontageWhenDeslicing){
		Stack.getDimensions(tmpiniWidth, tmpiniHeight, tmpiniNbChannels, tmpiniNbSlices, tmpiniNbFrames);
		Stack.setFrame(curFrameNb);
		run("Select All");
		run("Enlarge...", "enlarge=-2 pixel");
		beginFrame=maxOf(1,curFrameNb-plusMinusTime);
		endFrame=minOf(tmpiniNbFrames,curFrameNb+plusMinusTime);
		idMontage=makeMontageAllSlices(croppedHSid,beginFrame,endFrame,scaleMontageAllSlices);
		selectImage(idMontage);
	}
	nameMontage=getTitle();
	selectImage(croppedHSid);
	
	nbSlices=slicesNb;
	heightSlices=round((y)/channelsNb);
	widthSlices=round(imgWidth*heightSlices/imgHeight/channelsNb);
	
	idComboName=makeMontageDeslice(croppedHSid,heightSlices,widthSlices,channelsNb,slicesNb);	
	comboName=getTitle();
	selectImage("cropped");
	close();
	setBatchMode("exit and display"); 

	//Show montage if it exists
	if (makeAllSliceMontageWhenDeslicing){
		selectImage(nameMontage);
		//run("OptimizeContrast [o]");
		getLocationAndSize(xMont, yMont, imgWidthMont, imgHeightMont);
		setLocation(x+imgWidth,screenHeight-imgHeightMont);
	}
	selectImage(idComboName);
	Stack.setDimensions(1, 1, nbFrames);
	rename(substring(imageName,0,lengthOf(imageName)-18)+"HS-1.tif");
	sliceWindowIDArray=getImageID();
	widthSlices=round(widthSelect*y/heightSelect*slicesNb);
	setLocation(0, 0,widthSlices,y);
	getLocationAndSize(newx, newy, newimgWidth, newimgHeight);
	
	run("Grays");
	run("Invert LUT");
	resetMinAndMax();
	run("Enhance Contrast", "saturated=0.7");

	selectImage(mainImageId);
	
	wait(IJSynchronizationLag);
	run("Get Intensities [g]");
}

macro "Recenter next slice [n]" {
	selectImage(mainImageId);
	wait(IJSynchronizationLag);
	Stack.getPosition(channelIni, sliceIni, frameIni);
	roiManager("Update");
	count=roiManager("count");
	roiManager("select",count-1);
	
	Stack.setChannel(channelIni);
	curImageName=getTitle();
	setTool(0);
	plus=2;
	getVoxelSize(imagepw, imageph, imagepd,imageunit);
	run("Set Scale...", "distance=1 known=1 pixel=1 unit=pixel");
	run("Set Measurements...", "area mean centroid center bounding slice redirect=None decimal=5");
	run("Measure");
	x=getResult("X",nResults-1);
	y=getResult("Y",nResults-1);
	newX=round(x);
	newY=round(y);
	curROI=count-1;
	Stack.setChannel(channelIni);
	
	startSlice=getSliceNumber(); // Need absolute slice position in stack (not HS) for "Specify..."
	Stack.getDimensions(imWidth, imHeight, nbChannels, tmpNull, nbFrames);
	curSlice=startSlice+nbChannels;
	
	if(curSlice<=nSlices){		
		setSlice(curSlice);
		run("Specify...", "width="+diameter+" height="+diameter+" x="+(newX)+" y="+(newY)+" slice="+curSlice+" oval centered");
		roiManager("Add");
		curROI=curROI+1;
		roiManager("Select",curROI);
		Stack.setChannel(channelIni);
		registrationRecentering();
		selectImage(curImageName);
		roiManager("Update");
		
		run("Clear Results");
		
		if (nImages>1){
			getSlicesIntensities(nbSlices,offSetXReducedSlices,offSetYReducedSlices,-1,true);
		}
		run("Set Scale...", "distance="+(1/imagepw)+" known=1 pixel=1 unit=microns global");
	}
}

macro "Recenter current slice [r]" {
	registrationRecentering();
	run("Get Intensities [g]");
}

function registrationRecentering(){
	nameImage=getTitle();
	setBatchMode(true);
	getSelectionBounds(x, y, width, height);
	List.setMeasurements;//
	maxInRoi=List.getValue("Max");//
	newImage("Gauss", "32-bit Black", sizeWindowRecentering, sizeWindowRecentering, 1);
	makeRectangle(round(sizeWindowRecentering/2)-1,round(sizeWindowRecentering/2)-1, 1, 1);
	run("Add...", "value=255");
	run("Select None");
	run("Gaussian Blur...", "sigma="+(width/2/3));
	List.setMeasurements;//
	maxInGaussian=List.getValue("Max");//
	run("Multiply...", "value="+(maxInRoi/maxInGaussian));
	
	run("FFT");
	rename("FFT Gauss");
	
	
	selectImage(nameImage);
	//TODO measure
	//getSelectionBounds(x, y, width, height);
	enlargePx=round((sizeWindowRecentering-width)/2);
	run("Enlarge...", "enlarge="+enlargePx+" pixel");
	run("To Bounding Box");
	run("FFT");
	rename("FFT data");	
	
	run("FD Math...", "image1=[FFT data] operation=Correlate image2=[FFT Gauss] result=[Registration] do");
	getDimensions(widthReg, heightReg, channelsReg, slicesReg, framesReg);
	
	run("Select None");
	run("Measure");
	intensity=getResult("Mean",nResults-1);
	run("Divide...", "value="+(intensity)+" slice");
	// Show maximum
	run("Select None");
	run("Find Maxima...", "noise=1 output=[Point Selection]");
	getSelectionBounds(xM, yM, wM, hM);
	
	dx=xM-round(widthReg/2);
	dy=yM-round(heightReg/2);
	
	selectImage(nameImage);
	
	makeOval(x+dx, y+dy, width, height);
	
	selectImage("FFT data");
	close();
	selectImage("Registration");
	close();
	selectImage("FFT Gauss");
	close();
	selectImage("Gauss");
	close();
	setBatchMode("exit and display");
}

macro 'Update [u]'{ 
	roiManager("Update");
	i=roiManager("index");
	count=roiManager("count")
	if (i<count-1){
		roiManager("select",i+1);
	}
	if (mainImageName!=""){
		HSname=substring(mainImageName,0,lengthOf(mainImageName)-18)+"HS";
		if (isOpen(HSname+"-1.tif")){
			selectImage(mainImageId);
			getSlicesIntensities(nbSlices,offSetXReducedSlices,offSetYReducedSlices,-1,true);
		}
	}
}

macro 'Regular Update ROIManager [w]'{ 
	roiManager("Update");
}

macro 'Get Intensities [g]'{
	selectImage(mainImageId);
	getSlicesIntensities(nbSlices,offSetXReducedSlices,offSetYReducedSlices,-1,false);
}

macro 'Mark ROI as bad/good [b]' {
	index=roiManager("index");
	curName=call("ij.plugin.frame.RoiManager.getName", index);
	if (endsWith(curName,"#x")){
		roiManager("Rename", substring(curName,0,lengthOf(curName)-2));
		roiManager("Set Color", "#80ffff");
	}
	else{
		roiManager("Rename", curName+"#x");
		roiManager("Set Color", "red");
	}
}

macro "Background ROI" {
	isStack=Stack.isHyperstack;
	Stack.getPosition(channelIni, sliceIni, frameIni);
	if (isStack){
		Stack.setChannel(channelIni);
	}
	getVoxelSize(imagepw, imageph, imagepd,imageunit);
	run("Set Scale...", "distance=1 known=1 pixel=1 unit=pixel");
	count=roiManager("count");
	
	for (i=0; i<count; i++){
		roiManager("Select", i);
		if (isStack){
			Stack.setChannel(channelIni);
		}
		tmpROIName=getInfo("roi.name");
		run("Enlarge...", "enlarge="+dia2);
		roiManager("Add");
		tmpInd=roiManager("count")-1;
		roiManager("Select", tmpInd);
		if (isStack){
			Stack.setChannel(channelIni);
		}
		bgROIName=getInfo("roi.name");
		newROIName=bgROIName+"BG";
		if (endsWith(tmpROIName,"#x")){
			newROIName=newROIName+"#x";
		}
		roiManager("Rename", newROIName);		
	}
	roiManager("Deselect");
	run("Set Scale...", "distance="+(1/imagepw)+" known=1 pixel=1 unit=microns global");
	roiManager("Select",count);
	if (isStack){
		Stack.setChannel(channelIni);
	}
	run("Get Intensities [g]");
}

macro 'Spread Cytoplasmic Background Roi (VS Style)'{
	//After tracking Roi, add one Roi in the cytoplasm on the frame with the first time point for the patch
	//run this macro to spread (copy) the cytoplasmic Roi on the next frames
	//Save this RoiSet with a name that ends with "_VS.zip"
	count=roiManager("count")-1;
	roiManager("Measure");
	firstSlice=getResult("Slice",nResults-1);
	
	for (k=1; k<count; k++){
		setSlice(firstSlice+k);
		roiManager("Add");
	}
	roiManager("Deselect");
	roiManager("Save","");
}


macro 'Save ROISet [S]'{
	roiManager("Deselect");
	roiManager("Save","");
}

macro 'Plot intensities for ROI [P]'{
	Stack.getPosition(channelIni, sliceIni, frameIni);
	getVoxelSize(imagepw, imageph, imagepd,imageunit);
	areaScaling=imagepw*imageph;
	setBatchMode(true);
	count=roiManager("count");
	x=newArray(count);
	y=newArray(count);
	z=newArray(count);
	u=newArray(count);
	for(i=0;i<count;i++){
		roiManager("select",i);
		Stack.setChannel(channelIni);
		getSlicesIntensities(nbSlices,offSetXReducedSlices,offSetYReducedSlices, -2,true);
		maxSlice=0;
		indexMaxSlice=-1;
		for (j=0;j<nbSlices;j++){
			if(intensities[j]>maxSlice){
				maxSlice=intensities[j];
				indexMaxSlice=i;
			}
		}
		resArray=getPatchIntensity(indexMaxSlice,nbSlices);
		min=resArray[2];
		max=resArray[3];
		sumBG=0;
		sumInt=0;
		for (j=min;j<=max;j++){
			sumBG=sumBG+backgrounds[j];
			sumInt=sumInt+(intensities[j]-backgrounds[j]);
		}
		
		x[i]=getResult("Slice",nResults-1);
		Stack.setChannel(channelIni);
		y[i]=resArray[0]/areaScaling;
		z[i]=sumBG/areaScaling;
		u[i]=sumInt/areaScaling;
		
	}
	xmin=x[0];
	xmax=x[0];
	ymin=y[0];
	ymax=y[0];
	umin=u[0];
	umax=u[0];
	xx=newArray(count/2);
	zz=newArray(count/2);
	uu=newArray(count/2);
	for (i=0;i<count/2;i++){
		xx[i]=x[i];
		zz[i]=z[i];
		uu[i]=u[i];
		if (x[i]<xmin){xmin=x[i];}		
		if (x[i]>xmax){xmax=x[i];}		
		if (z[i]<ymin){ymin=z[i];}		
		if (z[i]>ymax){ymax=z[i];}
		if (u[i]<umin){umin=u[i];}		
		if (u[i]>umax){umax=u[i];}		
	}
	for (i=0;i<count;i++){
		if (x[i]<xmin){xmin=x[i];}		
		if (x[i]>xmax){xmax=x[i];}		
		if (y[i]<ymin){ymin=y[i];}		
		if (y[i]>ymax){ymax=y[i];}		
	}
	selectImage(mainImageId);
	roiManager("select",0);
	Stack.setChannel(channelIni);
	setBatchMode(false);
	Plot.create("Raw intensities (3 slices)", "Time", "Uncorrected Intensity (A.U.)", x, y);
	Plot.setLimits(xmin, xmax, ymin, ymax);
	Plot.setColor("black");
	Plot.add("line", xx, zz);
	Plot.setColor("red");
	Plot.show();
	Plot.create("Patch intensity", "Time", "Intensity (A.U.)", xx, uu);
	Plot.setLimits(xmin, xmax, 0, umax);
	Plot.setColor("green");
	Plot.show();
	
}

macro "AutoRecenter" {
	// Initial ROI
	Stack.getPosition(channelIni, sliceIni, frameIni);
	plus=2;
	getVoxelSize(imagepw, imageph, imagepd,imageunit);
	run("Set Scale...", "distance=1 known=1 pixel=1 unit=pixel");
	run("Set Measurements...", "area mean centroid center bounding slice redirect=None decimal=5");
	numROIs=roiManager("count");
	roiManager("Select",numROIs-1);
	Stack.setChannel(channelIni);
	run("Measure");
	x=getResult("X",nResults-1);
	y=getResult("Y",nResults-1);
	Stack.setChannel(channelIni);
	newX=round(x);
	newY=round(y);
	curROI=numROIs-1;
	newCenter=gaussCenter(diameter,curROI,newX,newY,maxGaussFittingRuns,-1,-1); // First call for chosen ROI to Gausian
	startSlice=getSliceNumber();
	Stack.getDimensions(imWidth, imHeight, nbChannels, tmpSlices, nbFrames); 
	if(distTrack !=0){
		print(d2s(newCenter[0],2)+"\t"+d2s(newCenter[1],2)+"\n");
	}
	
	run("Specify...", "width="+diameter+" height="+diameter+" x="+round(newCenter[0]+1)+" y="+(newCenter[1]+1)+" slice="+startSlice+" oval centered");
	roiManager("Update");
	
	// FUTURE ROIs---
	for(i=startSlice;i<nSlices;i+nbChannels){
		roiManager("Select",curROI);
		Stack.setChannel(channelIni);
		run("Measure");
		x=getResult("X",0);
		y=getResult("Y",0);
		Stack.setChannel(channelIni);
		
		newX=round(x);
		newY=round(y);
		
		curSlice=i+1;
		run("Specify...", "width="+diameter+" height="+diameter+" x="+(newX+1)+" y="+(newY+1)+" slice="+curSlice+" oval centered");
		roiManager("Add");
		curROI=curROI+1;
		newCenter=gaussCenter((diameter+plus),curROI,newX,newY,maxGaussFittingRuns,-1,-1); // Refit with Gaussian
		if(distTrack != 0){
			print(d2s(newCenter[0],2)+"\t"+d2s(newCenter[1],2)+"\n");
		}
		run("Specify...", "width="+diameter+" height="+diameter+" x="+round(newCenter[0]+1)+" y="+round(newCenter[1]+1)+" slice="+curSlice+" oval centered");
		roiManager("Update");
		
		j=j+1;
		run("Clear Results");
	}
	run("Clear Results");
	selectWindow("Results");
	run("Close");
	run("Set Scale...", "distance="+(1/imagepw)+" known=1 pixel=1 unit=microns global");
}
		
macro "Follow the Point Source"{
	Stack.getPosition(channelIni, sliceIni, frameIni);
	count=roiManager("count");
	roiManager("select",0);
	Stack.setChannel(channelIni);
	for(i=0;i<count;i++){
		roiManager("select",i);
		Stack.setChannel(channelIni);
		wait(50);
	}
	roiManager("select",0);
	Stack.setChannel(channelIni);
	roiManager("Deselect");
	run("Select None");
}



/********************************************************* 
****************** Data Analysis *************************
***********************************************************/
macro '** Data Analysis **'{} 

macro 'New Data Analysis [A]'{
	
	// Need to have first in the list of ROIs the ROIs for photobleaching correction (all slices)
	// and then all the ROIs you want to measure
	
	// the following  lines are necessary to prevent a glitch in ROI selection
	
	//call("java.lang.System.gc");
	
	Stack.getPosition(channelInitial, sliceInitial, frameInitial);
	isStack=Stack.isHyperstack;
	getDimensions(width, height, channels, slices, frames);
	
	setBatchMode(batchMode);
	
		if(isOpen("ROI Manager")){
			roiManager("Deselect");
			selectWindow("ROI Manager");
			run("Close");
		}
		if(isOpen("Log")){
			selectWindow("Log");
			run("Close");
		}
		if(isOpen("Results")){
			selectWindow("Results");
			run("Close");
		}
		
		for (i=1; i<=nImages; i++){
			selectImage(i); 
			if (endsWith(getTitle(),"_SUM_Corrected.tif")){
				mainImageName=getTitle();	
			}
		}
		selectImage(mainImageName);
		
		for (chch=1;chch<=channels;chch++){
			if (isStack){
				Stack.setChannel(chch);
			}		
			
			run("Select None");
			
			print("Measuring Channel "+chch+"\n");
			print("WARNING: image "+mainImageName+" has been selected as default Sum projection image.");
			print("         If it is not the image you want to analyze, close all images except the Sum projection image you want to analyze");
			
			chNbtxt="";
			if (channels>1){
				chNbtxt="_Ch"+chch;
			}
			
			if (generateFileMSDs) 
			{
				fileNameMSD=getDirectory("image")+"AlignedMSD"+chNbtxt+".xls";
				f = File.open(fileNameMSD);
				File.close(f);
			}
			if ( generateFilePositions)
			{
				fileNameSpeed=getDirectory("image")+"AlignedPositions"+chNbtxt+".xls";
				f = File.open(fileNameSpeed);
				File.close(f);
			}
			if ( generateFileDisplacements)
			{
				fileNameDisplacement=getDirectory("image")+"AlignedDisplacements"+chNbtxt+".xls";
				f = File.open(fileNameDisplacement);
				File.close(f);
			}
			if ( generateFileIntensities)
			{
				fileNameIntensity=getDirectory("image")+"CorrectedAlignedIntensities"+chNbtxt+".xls";
				f = File.open(fileNameIntensity);
				File.close(f);
			}
			if (generateFileConcvsDist)
			{	
				fileNameConcvsDist=getDirectory("image")+"ConcentrationVsDistance"+chNbtxt+".xls";
				f = File.open(fileNameConcvsDist);
				File.close(f);
			}
			
			fileNameLog=getDirectory("image")+"Log"+chNbtxt+".txt";
			f = File.open(fileNameLog);
			File.close(f);
			
			roiManager("Reset");
			
			getVoxelSize(imagepw, imageph, imagepd,imageunit);
			run("Set Scale...", "distance=1 known=1 pixel=1 unit=pixel");
			run("Set Measurements...", "area mean centroid center bounding slice redirect=None decimal=5");
			
			listFiles=getFileList(getDirectory("image"));
			nbROIFiles=0;
			foundPhotoBleachingFile=0;
			for(i=0;i<listFiles.length;i++){// Counting the nb of ROIset files and search for Photobleaching ROIset file
				if(endsWith(listFiles[i],".zip"))
				{
					if(startsWith(listFiles[i],ROIFileNamepattern)){
						nbROIFiles++;
					}
					else if(GlobalPhotoBleachingRate==-1&&listFiles[i]=="RoiForPhotoBleaching.zip"){
						roiManager("Reset");
						roiManager("Open", getDirectory("image")+listFiles[i]);
						foundPhotoBleachingFile=1;
					}
				}
				
			}
			
			if (GlobalPhotoBleachingRate==-1&&foundPhotoBleachingFile==0){
				waitForUser("Analysis aborted!", "No PhotoBleaching ROIs found (RoiForPhotoBleaching.zip). Create a ROI file for photobleaching first!");
			}
			else if (nbROIFiles==0){
				waitForUser("Analysis aborted!","No Roi whose name starts with '"+ROIFileNamepattern+"' was found!\nCreate a Roi or change the Roi name pattern in the setup!");
			}
			else{
				AllROIOK=true;
				patchNames=newArray(nbROIFiles);
				patchDurations=newArray(nbROIFiles);
				j=0;
				tmpROINb=roiManager("count");
				for(i=0;i<listFiles.length;i++){// Gets the length of each ROIset
					if(startsWith(listFiles[i],ROIFileNamepattern)&&endsWith(listFiles[i],".zip")){
						roiManager("Open", getDirectory("image")+listFiles[i]);
						patchNames[j]=listFiles[i];
						curROINb=roiManager("count")-tmpROINb;
						patchDurations[j]=curROINb/2;
						if (floor(patchDurations[j])!=patchDurations[j]){
							print("ERROR in ROI: "+patchNames[j]);
							print("  Number of points in the ROI and the Background do not match");
							AllROIOK=false;
							exit;
						}
						tmpROINb=roiManager("count");
						j++;
					}
				}
				dbprint("here2 batchMode="+batchMode);
				// Get the names of ROIs now because calling javascripts (or just getting an ROI name??) doesn't work in batch mode
				countTmp=roiManager("count");
				dbprint(countTmp);
				ROINames=newArray(countTmp);
				for (i=0;i<countTmp;i++){
					roiManager("select",i);
					if (isStack){
						Stack.setChannel(chch);
					}
					tmpROIName=getInfo("roi.name");
					splitedName=split(tmpROIName,"#x");
					if (lengthOf(splitedName)>1)
					{
						tmpROIName=splitedName[0]+"#x";
						roiManager("Rename", tmpROIName);
						changed=true;
					}
					ROINames[i]=tmpROIName;
					dbprint(tmpROIName);
				}
				dbprint("here3");
				dbprint(batchMode);
				print("-------------");
				print("USE: For this plugin to work properly, you need to open the projected movie and have in the same folder your ROIs and the ROI for photobleaching (for all the slices)");
				print("--------------");
				
				roiManager("Deselect");
				run("Clear Results");
				run("Measure");
				// Array for photobleaching correction
				maxIntensity=getResult("Mean", 0);
				
				if(isNaN(myGetResult("Area", 0))||isNaN(myGetResult("Mean", 0))||isNaN(myGetResult("XM", 0)))//||isNaN(myGetResult("Slice", 0)))
					{aa exit("ANALYSIS ABORTED!! \n In 'Analyze -> Set Measurements...',  'Area', 'Mean Value', 'Center of mass', 'Slice Number' must be checked \n And ROIs for patches must be loaded")}
				
				//Finds number of patches
				nROIs=roiManager("count");
				nbPatch=nbROIFiles;
				
				Stack.getDimensions(imWidth, imHeight, nbChannels, tmpNull, nbFrames) 
				print("This stack has "+nbFrames+" slices and you want to measure "+nbPatch+" patches");
				
				run("Clear Results");
				
				if(getPosDataFromMaxSlice==true){
					print("Positions are calculated from the slice with the maximum intensity");
				}
				else{
					print("Positions are calculated from the sum image");
				}
				
				if (GlobalPhotoBleachingRate==-1){
					sizeResults=(nROIs-nbFrames)/2;
					offSet=nbFrames;		
				}
				else{
					sizeResults=(nROIs)/2;
					offSet=0;
				}
				offSetArrays=0;
				
				//Initialisation
				patchMaxIntensityIndex=newArray(nbPatch);
				patchIntensities=newArray(sizeResults);
				patchDisplacements=newArray(sizeResults);
				patchDisplacementspx=newArray(sizeResults);
				patchDeltaDisplacements=newArray(sizeResults);
				patchDeltaDisplacementspx=newArray(sizeResults);
				
				patchGaussianCenterSuccess=newArray(sizeResults);
				patchXBackground=newArray(sizeResults);
				patchXStdev=newArray(sizeResults);
				patchXRsquared=newArray(sizeResults);
				patchYBackground=newArray(sizeResults);
				patchYStdev=newArray(sizeResults);
				patchYRsquared=newArray(sizeResults);
				
				patchMSDs=newArray(sizeResults);
				patchMSDspx=newArray(sizeResults);
				patchPositionsX=newArray(sizeResults);
				patchPositionsY=newArray(sizeResults);
				patchPositionsXpx=newArray(sizeResults);
				patchPositionsYpx=newArray(sizeResults);
				patchQualityControl=newArray(sizeResults);
				patchMaxIntesityOnSlice=newArray(sizeResults);
				
				
				//Collected from max intensity slice
				patchDisplacementsMaxSl=newArray(sizeResults);
				patchDisplacementspxMaxSl=newArray(sizeResults);
				patchDeltaDisplacementsMaxSl=newArray(sizeResults);
				patchDeltaDisplacementspxMaxSl=newArray(sizeResults);
				
				patchGaussianCenterSuccessMaxSl=newArray(sizeResults);
				patchXBackgroundMaxSl=newArray(sizeResults);
				patchXStdevMaxSl=newArray(sizeResults);
				patchXRsquaredMaxSl=newArray(sizeResults);
				patchYBackgroundMaxSl=newArray(sizeResults);
				patchYStdevMaxSl=newArray(sizeResults);
				patchYRsquaredMaxSl=newArray(sizeResults);
				
				patchMSDsMaxSl=newArray(sizeResults);
				patchMSDspxMaxSl=newArray(sizeResults);
				patchPositionsXMaxSl=newArray(sizeResults);
				patchPositionsYMaxSl=newArray(sizeResults);
				patchPositionsXpxMaxSl=newArray(sizeResults);
				patchPositionsYpxMaxSl=newArray(sizeResults);
				
				
				
				getPixelSize(unit, pw, ph, pd); //pixel sizes in rescaled units (usually micron)
				
				dbprint("pw="+pw);
				areaScalingFactor=pw*ph;
				intensityCounter=0;
				
				// Open associated hyperstack
				HSname=substring(mainImageName,0,lengthOf(mainImageName)-18)+"HS";
				if (!isOpen(HSname+".tif")){
					open(getDirectory("image")+fileSep+HSname+".tif");
				}
				else{
					selectImage(HSname+".tif");
				}
				Stack.getDimensions(dumpwidth, dumpheight, dumpchannels, nbSlices, dumpframes)
				run("Set Scale...", "distance=1 known=1 pixel=1 unit=pixel");
				run("Select None");
				
				Stack.getDimensions(widthHS, heightHS, channelsHS, slicesHS, framesHS);
				
				
				dbprint("nbROIFiles="+nbROIFiles);
				
				if (GlobalPhotoBleachingRate==-1){
					//Get exctinction rate for Photobleaching Correction table (size of the hyperstack)
					
					x=newArray(framesHS*slicesHS);//time
					y=newArray(framesHS*slicesHS);//intensities
					for (i=0;i<framesHS;i++){
						roiManager("Select", i);
						Stack.setFrame(i+1);
						Stack.setChannel(chch);
						for (j=0;j<slicesHS;j++){
							Stack.setSlice(j+1);
							run("Measure");
							x[i*slicesHS+j]=i*slicesHS+j;
							y[i*slicesHS+j]=getResult("Mean",nResults-1);
						}
					}
					
					if (photobleachingEq=="y = a+b*exp(-c*x)"){
						equation = "y = a+b*exp(-c*x)";
						a0=y[y.length-1];
						b0=y[0]-y[y.length-1];
						c0=0.001;
						initialGuesses = newArray(a0,b0,c0);
					}
					else if (photobleachingEq=="y = a*exp(-b*x)"){
						equation = "y = a*exp(-b*x)";
						a0=y[0];
						b0=0.001;
						initialGuesses = newArray(a0,b0);
					}
					
					Fit.logResults;
					Fit.doFit(equation, x, y, initialGuesses);
					Fit.plot;
					
					rateBleaching=0;
					if (photobleachingEq=="y = a+b*exp(-c*x)"){
						rateBleaching=Fit.p(2);
					}
					else if (photobleachingEq=="y = a*exp(-b*x)"){
						rateBleaching=Fit.p(1);
					}
					
					print(" Photobleaching coefficient estimated from stack:"+rateBleaching);
					print("   Equivalent photobleaching coefficient for Z projection movie:"+(rateBleaching*slicesHS));
				}
				else{
					rateBleaching=GlobalPhotoBleachingRate;
					print(" Photobleaching coefficient set by user:"+rateBleaching);
					print("   Equivalent photobleaching coefficient for Z projection movie:"+(rateBleaching*slicesHS));
				}
				
				selectImage(mainImageName);
				
				for (patchNumber=0;patchNumber<nbPatch;patchNumber++)
				{
					selectImage(mainImageName);
					if (isStack){
						Stack.setChannel(chch);
					}
					print((patchNumber+1)+"/"+nbPatch+"  Analyzing "+patchNames[patchNumber]+" ...");
					maxIntensity=0;
					// Find patch duration
					i=0;
					
					curArea=myGetResult("Area",offSet);
					
					
					patchArea=curArea;
					patchAreapx=patchArea/areaScalingFactor; //rescaling in number of pixels
					
					displacement=0;
					displacementpx=0;
					if (isStack){
						Stack.setChannel(chch);
					}
					previousPositionXpx=myGetResult("XM", offSet);
					previousPositionYpx=myGetResult("YM", offSet);
					{
						gaussianCenter=gaussCenter(diameter+2,offSet,previousPositionXpx,previousPositionYpx,maxGaussFittingRuns,-1,-1);
					}
					previousPositionXpx=previousPositionXpx*imagepw;
					previousPositionYpx=previousPositionYpx*imageph;
					
					previousPositionX=gaussianCenter[0]*imagepw;
					previousPositionY=gaussianCenter[1]*imageph;
					
					if (endsWith(patchNames[patchNumber],"_VS.zip")){volodiaStyle=true;print("   "+patchNames[patchNumber]+" is treated as Volodia Style for background correction");}
					else {volodiaStyle=false;}
					
					
					// Initialisation for position calculated from Max intensity slice
					
					slicesData=getSlicesIntensitiesFromHS(offSet,slicesHS,patchDurations[patchNumber],rateBleaching,volodiaStyle); 
					
					
					displacementMaxSl=0;
					displacementpxMaxSl=0;
					previousPositionXpxMaxSl=slicesData[1]*imagepw;
					previousPositionYpxMaxSl=slicesData[2]*imageph;
					
					previousPositionXMaxSl=slicesData[3]*imagepw;
					previousPositionYMaxSl=slicesData[4]*imageph;
					
					
					
					for (i=0;i<patchDurations[patchNumber];i++)
					{
						
						// Get Position
						run("Clear Results");
						selectImage(mainImageName);
						roiManager("Select", offSet + i);
						if (isStack){
							Stack.setChannel(chch);
						}					
						run("Measure");
						positionXpx=getResult("XM",0);
						positionYpx=getResult("YM",0);
						selectImage(mainImageName);
						if (isStack){
							Stack.setChannel(chch);
						}
						
						getSelectionBounds(xPatchTmp, yPatchTmp, widthPatchTmp, heightPatchTmp);
						
						
						// Positions calculated from projection image if gaussian fit is performed (ie maxGaussFittingRuns>0)
						{
							gaussianCenter=gaussCenter(widthPatchTmp+2,offSet+i,positionXpx,positionYpx,maxGaussFittingRuns,-1,-1);
						}
						
						positionXpx=positionXpx*imagepw;
						positionYpx=positionYpx*imageph;
						positionX=gaussianCenter[0]*imagepw;
						positionY=gaussianCenter[1]*imageph;
						
						displacementX=positionX-previousPositionX;
						displacementY=positionY-previousPositionY;
						distance2=pow(displacementX,2)+pow(displacementY,2);
						displacement=displacement+sqrt(distance2);
						
						displacementXpx=positionXpx-previousPositionXpx;
						displacementYpx=positionYpx-previousPositionYpx;
						distance2px=pow(displacementXpx,2)+pow(displacementYpx,2);
						displacementpx=displacementpx+sqrt(distance2px);
						
						if (isStack){
							Stack.setChannel(chch);
						}
						// Intensity measurement
						sliceNumber=myGetResult("Slice",offSet+i);
						
						slicesData=getSlicesIntensitiesFromHS(offSet+i,slicesHS,patchDurations[patchNumber],rateBleaching,volodiaStyle);
						
						indexMaxInt=slicesData[0];
						tmp=getPatchIntensity(indexMaxInt,slicesHS);
						
						
						//Not working in batch mode!
						curName=ROINames[offSet+i];
						if (endsWith(curName,"#x")||((slicesHS>nbSliceMeasured)&&(indexMaxInt==0||indexMaxInt==slicesHS-1))){
							//If patch was tagged as bad or first or last slice if the nb of slices measured is less than nb of slices of movie
							tmpIntensity=0;
						}
						else{
							tmpIntensity=tmp[0];
						}				
						patchIntensityPercentage=tmp[1];
						minSlice=tmp[2];
						maxSlice=tmp[3];
						
						// Quality control for intensity calculation
						strQuality="";
						for(k=0;k<slicesHS;k++){
							strQuality=strQuality+d2s(percentages[k]*100,1)+" / ";
						}
						patchQualityControl[intensityCounter]=substring(strQuality,0,lengthOf(strQuality)-2);
						patchMaxIntesityOnSlice[intensityCounter]=indexMaxInt;
						
						tmpIntensity=tmpIntensity*1000/exposureTimeCorrection; //exposure time correction to 1 sec 
						tmpIntensity=tmpIntensity/calibrationFactor; // Conversion into number of molecules or concentration
						
						patchIntensities[intensityCounter]=tmpIntensity;		
						patchDisplacements[intensityCounter]=displacement;
						patchDeltaDisplacements[intensityCounter]=sqrt(distance2)/timeDelay;
						
						patchGaussianCenterSuccess[intensityCounter]=gaussianCenter[2];
						patchXBackground[intensityCounter]=gaussianCenter[3];
						patchXStdev[intensityCounter]=gaussianCenter[4];
						patchXRsquared[intensityCounter]=gaussianCenter[5];
						patchYBackground[intensityCounter]=gaussianCenter[6];
						patchYStdev[intensityCounter]=gaussianCenter[7];
						patchYRsquared[intensityCounter]=gaussianCenter[8];
						
						patchDisplacementspx[intensityCounter]=displacementpx;
						patchDeltaDisplacementspx[intensityCounter]=sqrt(distance2px)/timeDelay;
						
						patchPositionsX[intensityCounter]=positionX;
						patchPositionsY[intensityCounter]=positionY;
						patchPositionsXpx[intensityCounter]=positionXpx;
						patchPositionsYpx[intensityCounter]=positionYpx;
						
						// Positions calculated from Max intensity slice
						positionXpxMaxSl=slicesData[1]*imagepw;
						positionYpxMaxSl=slicesData[2]*imageph;
						positionXMaxSl=slicesData[3]*imagepw;
						positionYMaxSl=slicesData[4]*imageph;
						
						displacementXMaxSl=positionXMaxSl-previousPositionXMaxSl;
						displacementYMaxSl=positionYMaxSl-previousPositionYMaxSl;
						
						distance2MaxSl=pow(displacementXMaxSl,2)+pow(displacementYMaxSl,2);
						displacementMaxSl=displacementMaxSl+sqrt(distance2MaxSl);
						
						displacementXpxMaxSl=positionXpxMaxSl-previousPositionXpxMaxSl;
						displacementYpxMaxSl=positionYpxMaxSl-previousPositionYpxMaxSl;
						distance2pxMaxSl=pow(displacementXpxMaxSl,2)+pow(displacementYpxMaxSl,2);
						displacementpxMaxSl=displacementpxMaxSl+sqrt(distance2pxMaxSl);
						
						patchDisplacementsMaxSl[intensityCounter]=displacementMaxSl;
						patchDeltaDisplacementsMaxSl[intensityCounter]=sqrt(distance2MaxSl)/timeDelay;
						
						patchGaussianCenterSuccessMaxSl[intensityCounter]=slicesData[5];
						patchXBackgroundMaxSl[intensityCounter]=slicesData[6];
						patchXStdevMaxSl[intensityCounter]=slicesData[7];
						patchXRsquaredMaxSl[intensityCounter]=slicesData[8];
						patchYBackgroundMaxSl[intensityCounter]=slicesData[9];
						patchYStdevMaxSl[intensityCounter]=slicesData[10];
						patchYRsquaredMaxSl[intensityCounter]=slicesData[11];
						
						patchDisplacementspxMaxSl[intensityCounter]=displacementpxMaxSl;
						patchDeltaDisplacementspxMaxSl[intensityCounter]=sqrt(distance2pxMaxSl)/timeDelay;
						patchPositionsXMaxSl[intensityCounter]=positionXMaxSl;
						patchPositionsYMaxSl[intensityCounter]=positionYMaxSl;
						patchPositionsXpxMaxSl[intensityCounter]=positionXpxMaxSl;
						patchPositionsYpxMaxSl[intensityCounter]=positionYpxMaxSl;
						
						if(patchIntensities[intensityCounter]>maxIntensity)
						{	
							maxIntensity=patchIntensities[intensityCounter];
							patchMaxIntensityIndex[patchNumber]=i;
						}
						previousPositionX=positionX;
						previousPositionY=positionY;
						previousPositionXpx=positionXpx;
						previousPositionYpx=positionYpx;
						
						previousPositionXMaxSl=positionXMaxSl;
						previousPositionYMaxSl=positionYMaxSl;
						previousPositionXpxMaxSl=positionXpxMaxSl;
						previousPositionYpxMaxSl=positionYpxMaxSl;
						
						intensityCounter++;
						
					}	
					
					
					if(defaultWindowSizeForMSD==-1){
						windowSizeForMSD=0; // must be less than patchDurations[patchNumber]
					}
					else{
						windowSizeForMSD=defaultWindowSizeForMSD;
					}
					MSDpx=newArray(patchDurations[patchNumber]);
					MSDpxMaxSl=newArray(patchDurations[patchNumber]);
					NforAverage=newArray(patchDurations[patchNumber]);
					for (i=0;i<patchDurations[patchNumber];i++){
						MSDpx[i]=0;
						MSDpxMaxSl[i]=0;
						NforAverage[i]=0;
					}
					
					for (i=0;i<patchDurations[patchNumber]-windowSizeForMSD;i++) // Computation of MSDs
					{
						refXpx=patchPositionsXpx[offSetArrays+i];
						refYpx=patchPositionsYpx[offSetArrays+i];
						refXpxMaxSl=patchPositionsXpxMaxSl[offSetArrays+i];
						refYpxMaxSl=patchPositionsYpxMaxSl[offSetArrays+i];
						if(defaultWindowSizeForMSD==-1){
							endIndex=patchDurations[patchNumber]; // must be less than patchDurations[patchNumber]
						}
						else{
							endIndex=i+windowSizeForMSD;
						}
						for (j=i;j<endIndex;j++){
							r2tmppx=pow((patchPositionsXpx[offSetArrays+j]-refXpx),2)+pow((patchPositionsYpx[offSetArrays+j]-refYpx),2);
							r2tmppxMaxSl=pow((patchPositionsXpxMaxSl[offSetArrays+j]-refXpxMaxSl),2)+pow((patchPositionsYpxMaxSl[offSetArrays+j]-refYpxMaxSl),2);
							MSDpx[j-i]=MSDpx[j-i]+r2tmppx;
							MSDpxMaxSl[j-i]=MSDpxMaxSl[j-i]+r2tmppxMaxSl;
							NforAverage[j-i]=NforAverage[j-i]+1;
						}				
					}
					if(defaultWindowSizeForMSD==-1){
						endIndex=patchDurations[patchNumber]; // must be less than patchDurations[patchNumber]
					}
					else{
						endIndex=windowSizeForMSD;
					}
					for (i=0;i<endIndex;i++) // Average of MSDs
					{
						patchMSDspx[offSetArrays+i]=MSDpx[i]/NforAverage[i];
						patchMSDspxMaxSl[offSetArrays+i]=MSDpxMaxSl[i]/NforAverage[i];
					}
					
					offSet=offSet+2*patchDurations[patchNumber];
					offSetArrays=offSetArrays+patchDurations[patchNumber];
					
				}
				
				maxPatchDurationIndex=0;
				maxPatchDuration=0;
				for (i=0;i<nbPatch;i++)
				{
					if(patchDurations[i]>maxPatchDuration){
						maxPatchDuration=patchDurations[i];
						maxPatchDurationIndex=i;
					}
				}
				
				// Get the reference timecourse for alignment:
				firstIndex=0;
				referencePatch=newArray(maxPatchDuration);
				for (i=0;i<maxPatchDurationIndex;i++){
					firstIndex=firstIndex+patchDurations[i];
				}
				for (i=0;i<maxPatchDuration;i++){
					referencePatch[i]=patchIntensities[firstIndex+i];
				}
				print("  Data aligned on "+patchNames[maxPatchDurationIndex]);
				
				// Enlarge referencePatch
				bigMaxPatchDuration=maxPatchDuration+2*extraTimePointsEachSide;
				bigReferencePatch=newArray(bigMaxPatchDuration);
				copyArrayIntoBiggerArray(referencePatch,bigReferencePatch,extraTimePointsEachSide);
				biggestTotalTime=bigMaxPatchDuration;
				
				//New tables of data with aligned values
				alignedPatchIntensities=newArray(nbPatch*(biggestTotalTime));
				alignedPatchDisplacements=newArray(nbPatch*(biggestTotalTime));
				alignedPatchDisplacementspx=newArray(nbPatch*(biggestTotalTime));
				alignedPatchDeltaDisplacements=newArray(nbPatch*(biggestTotalTime));
				alignedPatchDeltaDisplacementspx=newArray(nbPatch*(biggestTotalTime));
				alignedPatchMSDs=newArray(nbPatch*(biggestTotalTime));
				alignedPatchMSDspx=newArray(nbPatch*(biggestTotalTime));
				alignedPatchPositionsX=newArray(nbPatch*(biggestTotalTime));
				alignedPatchPositionsY=newArray(nbPatch*(biggestTotalTime));
				alignedPatchPositionsXpx=newArray(nbPatch*(biggestTotalTime));
				alignedPatchPositionsYpx=newArray(nbPatch*(biggestTotalTime));
				alignedPatchQualityControl=newArray(nbPatch*(biggestTotalTime));
				alignedPatchMaxIntesityOnSlice=newArray(nbPatch*(biggestTotalTime));
				
				alignedPatchGaussianCenterSuccess=newArray(nbPatch*(biggestTotalTime));
				alignedPatchXBackground=newArray(nbPatch*(biggestTotalTime));
				alignedPatchXStdev=newArray(nbPatch*(biggestTotalTime));
				alignedPatchXRsquared=newArray(nbPatch*(biggestTotalTime));
				alignedPatchYBackground=newArray(nbPatch*(biggestTotalTime));
				alignedPatchYStdev=newArray(nbPatch*(biggestTotalTime));
				alignedPatchYRsquared=newArray(nbPatch*(biggestTotalTime));
				
				
				// Positions calculated from Max intensity slice
				alignedPatchDisplacementsMaxSl=newArray(nbPatch*(biggestTotalTime));
				alignedPatchDisplacementspxMaxSl=newArray(nbPatch*(biggestTotalTime));
				alignedPatchDeltaDisplacementsMaxSl=newArray(nbPatch*(biggestTotalTime));
				alignedPatchDeltaDisplacementspxMaxSl=newArray(nbPatch*(biggestTotalTime));
				alignedPatchMSDsMaxSl=newArray(nbPatch*(biggestTotalTime));
				alignedPatchMSDspxMaxSl=newArray(nbPatch*(biggestTotalTime));
				alignedPatchPositionsXMaxSl=newArray(nbPatch*(biggestTotalTime));
				alignedPatchPositionsYMaxSl=newArray(nbPatch*(biggestTotalTime));
				alignedPatchPositionsXpxMaxSl=newArray(nbPatch*(biggestTotalTime));
				alignedPatchPositionsYpxMaxSl=newArray(nbPatch*(biggestTotalTime));
				
				alignedPatchGaussianCenterSuccessMaxSl=newArray(nbPatch*(biggestTotalTime));
				alignedPatchXBackgroundMaxSl=newArray(nbPatch*(biggestTotalTime));
				alignedPatchXStdevMaxSl=newArray(nbPatch*(biggestTotalTime));
				alignedPatchXRsquaredMaxSl=newArray(nbPatch*(biggestTotalTime));
				alignedPatchYBackgroundMaxSl=newArray(nbPatch*(biggestTotalTime));
				alignedPatchYStdevMaxSl=newArray(nbPatch*(biggestTotalTime));
				alignedPatchYRsquaredMaxSl=newArray(nbPatch*(biggestTotalTime));
				
				oldTableCounter=0;
				oldOffSet=0;
				newOffSet=0;
				for (i=0;i<nbPatch;i++)
				{
					//new index of the first timepoint of the patch
					//firstIndex=i*(biggestTotalTime)+biggestMaxIntensityIndex-patchMaxIntensityIndex[i];
					//Transforming current patch array into an array with the same size as the longest patch
					
					newOffSet=i*biggestTotalTime;
					tmpPatchIntensities=subsetEnlargeAndShiftArray(patchIntensities,oldOffSet,patchDurations[i],biggestTotalTime,0);
					//var res;
					if(alignment==ON_WHOLE_TRACK){
						//print("Alignment on whole tracks");
						beginAlign=0;
						endAlign=bigReferencePatch.length-1;
						//dbprint("Patch: "+i);
						res=getBestAlignment(tmpPatchIntensities,bigReferencePatch,beginAlign,endAlign,patchDurations[i]);
					}
					else if(alignment==ON_BEGINNING){
						//print("Alignment on track beginnings");
						beginAlign=0;
						endAlign=minOf(nbPointForAlignment,bigReferencePatch.length-1);
						res=getBestAlignment(tmpPatchIntensities,bigReferencePatch,beginAlign,endAlign,patchDurations[i]);
					}
					else if(alignment==ON_END){
						//print("Alignment on track ends");
						beginAlign=maxOf(0,bigReferencePatch.length-nbPointForAlignment-1);
						endAlign=bigReferencePatch.length-1;
						res=getBestAlignment(tmpPatchIntensities,bigReferencePatch,beginAlign,endAlign,patchDurations[i]);
					}
					shift=res[0];
					shiftedArrayIntensities=shiftArray(tmpPatchIntensities,shift);
					
					dbprint("shift="+shift);
					
					
					copyArrayIntoBiggerArray(shiftedArrayIntensities,alignedPatchIntensities,newOffSet);
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchDisplacements,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchDisplacements,newOffSet);
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchDisplacementspx,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchDisplacementspx,newOffSet);
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchDeltaDisplacements,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchDeltaDisplacements,newOffSet);
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchDeltaDisplacementspx,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchDeltaDisplacementspx,newOffSet);
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchMSDs,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchMSDs,newOffSet);
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchMSDspx,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchMSDspx,newOffSet);
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchPositionsX,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchPositionsX,newOffSet);
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchPositionsY,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchPositionsY,newOffSet);
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchPositionsXpx,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchPositionsXpx,newOffSet);
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchPositionsYpx,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchPositionsYpx,newOffSet);
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchQualityControl,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchQualityControl,newOffSet);
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchMaxIntesityOnSlice,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchMaxIntesityOnSlice,newOffSet);
					
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchGaussianCenterSuccess,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchGaussianCenterSuccess,newOffSet);
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchXBackground,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchXBackground,newOffSet);
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchXStdev,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchXStdev,newOffSet);
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchXRsquared,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchXRsquared,newOffSet);
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchYBackground,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchYBackground,newOffSet);
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchYStdev,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchYStdev,newOffSet);
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchYRsquared,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchYRsquared,newOffSet);
					
					
					// Calculated from Max intensity Slice
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchDisplacementsMaxSl,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchDisplacementsMaxSl,newOffSet);
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchDisplacementspxMaxSl,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchDisplacementspxMaxSl,newOffSet);
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchDeltaDisplacementsMaxSl,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchDeltaDisplacementsMaxSl,newOffSet);
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchDeltaDisplacementspxMaxSl,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchDeltaDisplacementspxMaxSl,newOffSet);
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchMSDsMaxSl,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchMSDsMaxSl,newOffSet);
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchMSDspxMaxSl,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchMSDspxMaxSl,newOffSet);
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchPositionsXMaxSl,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchPositionsXMaxSl,newOffSet);
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchPositionsYMaxSl,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchPositionsYMaxSl,newOffSet);
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchPositionsXpxMaxSl,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchPositionsXpxMaxSl,newOffSet);
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchPositionsYpxMaxSl,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchPositionsYpxMaxSl,newOffSet);
					
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchGaussianCenterSuccessMaxSl,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchGaussianCenterSuccessMaxSl,newOffSet);
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchXBackgroundMaxSl,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchXBackgroundMaxSl,newOffSet);
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchXStdevMaxSl,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchXStdevMaxSl,newOffSet);
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchXRsquaredMaxSl,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchXRsquaredMaxSl,newOffSet);
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchYBackgroundMaxSl,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchYBackgroundMaxSl,newOffSet);
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchYStdevMaxSl,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchYStdevMaxSl,newOffSet);
					copyArrayIntoBiggerArray(subsetEnlargeAndShiftArray(patchYRsquaredMaxSl,oldOffSet,patchDurations[i],biggestTotalTime,shift),alignedPatchYRsquaredMaxSl,newOffSet);
					
					
					oldOffSet=oldOffSet+patchDurations[i];
					
				}
				
				
				
				///	        ///
				//  Print File  //
				///	        ///
				
				
				//Convert position data to print into data calculated from Maximum intensity slice 
				if(getPosDataFromMaxSlice==true){
					alignedPatchDisplacements=alignedPatchDisplacementsMaxSl;
					alignedPatchDisplacementspx=alignedPatchDisplacementspxMaxSl;
					alignedPatchDeltaDisplacements=alignedPatchDeltaDisplacementsMaxSl;
					alignedPatchDeltaDisplacementspx=alignedPatchDeltaDisplacementspxMaxSl;
					
					alignedPatchMSDs=alignedPatchMSDsMaxSl;
					alignedPatchMSDspx=alignedPatchMSDspxMaxSl;
					alignedPatchPositionsX=alignedPatchPositionsXMaxSl;
					alignedPatchPositionsY=alignedPatchPositionsYMaxSl;
					alignedPatchPositionsXpx=alignedPatchPositionsXpxMaxSl;
					alignedPatchPositionsYpx=alignedPatchPositionsYpxMaxSl;
					
					alignedPatchGaussianCenterSuccess=alignedPatchGaussianCenterSuccessMaxSl;
					alignedPatchXBackground=alignedPatchXBackgroundMaxSl;
					alignedPatchXStdev=alignedPatchXStdevMaxSl;
					alignedPatchXRsquared=alignedPatchXRsquaredMaxSl;
					alignedPatchYBackground=alignedPatchYBackgroundMaxSl;
					alignedPatchYStdev=alignedPatchYStdevMaxSl;
					alignedPatchYRsquared=alignedPatchYRsquaredMaxSl;
					
				}
				
				////////////////  Intensities  /////////////////////////////////////////////////
				if ( generateFileIntensities)
				{
					f = File.open(fileNameIntensity);
					lineToPrint=" TimePointNb \t time \t";
					for (i=0;i<nbPatch;i++) // Patch intensities (titles)
					{
						n=i+1;
						lineToPrint=lineToPrint+patchNames[i]+"\t";
					}
					lineToPrint=lineToPrint+"\t"+"\t";
					for (i=0;i<nbPatch;i++) // Patch quality (titles)
					{
						n=i+1;
						lineToPrint=lineToPrint+patchNames[i]+" Quality \t"+patchNames[i]+" IndexMax \t";
					}
					lineToPrint=lineToPrint+"\t Photobleaching rate used: \t "+rateBleaching;
					print(f,lineToPrint);
					
					for (j=0;j<biggestTotalTime;j++)
					{
						lineToPrint=d2s(j,0)+"\t"+(j*timeDelay)+"\t";
						for (i=0;i<nbPatch;i++)
						{
							lineToPrint=lineToPrint+d2sIfNot0(alignedPatchIntensities[i*(biggestTotalTime)+j])+"\t";
						}
						lineToPrint=lineToPrint+"\t"+"\t";
						for (i=0;i<nbPatch;i++)
						{
							tmpSliceNb=alignedPatchMaxIntesityOnSlice[i*(biggestTotalTime)+j]+1;
							if(alignedPatchQualityControl[i*(biggestTotalTime)+j]!=0){
								lineToPrint=lineToPrint+"\'"+alignedPatchQualityControl[i*(biggestTotalTime)+j]+"\'"+"\t"+tmpSliceNb+"\t";
							}
							else{
								lineToPrint=lineToPrint+"\t \t ";
							}
						}
						print(f,lineToPrint);
					}
					File.close(f);
					print(fileNameIntensity+" saved successfully!");
				}
				///////////////////////////////////////////////////////////////////////////////////
				
				
				///////////////  Displacements  ////////////////////////////////////////////////
				if ( generateFileDisplacements)
				{
					pxResolution=1;
					f = File.open(fileNameDisplacement);
					lineToPrint=" TimePointNb \t time \t";
					for (i=0;i<nbPatch;i++)
					{
						n=i+1;
						lineToPrint=lineToPrint+" Displacement "+patchNames[i]+"\t";
					}
					if (pxResolution)
					{
						lineToPrint=lineToPrint+"\t"+" time \t";
						for (i=0;i<nbPatch;i++)
						{
							n=i+1;
							lineToPrint=lineToPrint+"px resolution Displacement Patch  "+d2s(n,0)+"\t";
							lineToPrint=lineToPrint+"GaussianAlignSuccess \t XBackgound Value \t XStdev \t XR2 \t YBackgound Value \t YStdev \t YR2 \t \t";
						}
					}
					print(f,lineToPrint);
					
					
					for (j=0;j<biggestTotalTime;j++)
					{
						lineToPrint=d2s(j,0)+"\t"+(j*timeDelay)+"\t";
						for (i=0;i<nbPatch;i++)
						{
							lineToPrint=lineToPrint+d2sIfNot0(alignedPatchDisplacements[i*(biggestTotalTime)+j])+"\t";
						}
						if (pxResolution)
						{
							lineToPrint=lineToPrint+"\t"+(j*timeDelay)+"\t";
							for (i=0;i<nbPatch;i++)
							{
								lineToPrint=lineToPrint+d2sIfNot0(alignedPatchDisplacementspx[i*(biggestTotalTime)+j])+"\t";
								
								lineToPrint=lineToPrint+d2s(alignedPatchGaussianCenterSuccess[i*(biggestTotalTime)+j],0)+"\t";
								lineToPrint=lineToPrint+d2sIfNot0(alignedPatchXBackground[i*(biggestTotalTime)+j])+"\t";
								lineToPrint=lineToPrint+d2sIfNot0(alignedPatchXStdev[i*(biggestTotalTime)+j])+"\t";
								lineToPrint=lineToPrint+d2sIfNot0(alignedPatchXRsquared[i*(biggestTotalTime)+j])+"\t";
								lineToPrint=lineToPrint+d2sIfNot0(alignedPatchYBackground[i*(biggestTotalTime)+j])+"\t";
								lineToPrint=lineToPrint+d2sIfNot0(alignedPatchYStdev[i*(biggestTotalTime)+j])+"\t";
								lineToPrint=lineToPrint+d2sIfNot0(alignedPatchYRsquared[i*(biggestTotalTime)+j])+"\t \t";
							}				
						}
						print(f,lineToPrint);
					}
					
					File.close(f);
					print(fileNameDisplacement+" saved successfully!");
				}
				///////////////////////////////////////////////////////////////////////////////////
				
				
				///////////////  MSD  ////////////////////////////////////////////////
				if ( generateFileMSDs) 
				{
					f = File.open(fileNameMSD);
					lineToPrint=" TimePointNb \t time \t";
					for (i=0;i<nbPatch;i++)
					{
						n=i+1;
						lineToPrint=lineToPrint+" MSD "+patchNames[i]+"\t";
					}
					lineToPrint=lineToPrint+"\t \t";
					for (i=0;i<nbPatch;i++)
					{
						n=i+1;
						lineToPrint=lineToPrint+" MSDpx "+patchNames[i]+"\t";
					}
					print(f,lineToPrint);
					
					
					for (j=0;j<biggestTotalTime;j++)
					{
						lineToPrint=d2s(j,0)+"\t"+(j*timeDelay)+"\t";
						for (i=0;i<nbPatch;i++)
						{
							lineToPrint=lineToPrint+d2sIfNot0(alignedPatchMSDs[i*(biggestTotalTime)+j])+"\t";
						}
						lineToPrint=lineToPrint+"\t \t";
						for (i=0;i<nbPatch;i++)
						{
							lineToPrint=lineToPrint+d2sIfNot0(alignedPatchMSDspx[i*(biggestTotalTime)+j])+"\t";
						}
						print(f,lineToPrint);
					}
					File.close(f);
					print(fileNameMSD+" saved successfully!");
				}
				///////////////////////////////////////////////////////////////////////////////////
				
				///////////////  Positions  ////////////////////////////////////////////////
				if ( generateFilePositions)
				{
					f = File.open(fileNameSpeed);
					for (i=0;i<nbPatch;i++){
						lineToPrint=" TimePointNb \t time \t";
						lineToPrint=lineToPrint+" X "+patchNames[i]+"\t Xpx \t Y \t Ypx \t \t Distance from starting point \t Distance from starting point px \t \t";
						lineToPrint=lineToPrint+"GaussianAlignSuccess \t XBackgound Value \t XStdev \t XR2 \t YBackgound Value \t YStdev \t YR2 \t \t";
						
						print(f,lineToPrint);
						foundFirstPoint=false;
						indexFirstPoint=0;
						for (j=0;j<biggestTotalTime;j++)
						{
							lineToPrint=d2s(j,0)+"\t"+(j*timeDelay)+"\t";
							{
								if(alignedPatchPositionsX[i*(biggestTotalTime)+j]!=0&&foundFirstPoint==false)
								{//find the first real time point for patch tracking
									foundFirstPoint=true;
									indexFirstPoint=j;
								}
								else if(alignedPatchPositionsX[i*(biggestTotalTime)+j]==0&&foundFirstPoint==true)
								{// finds the last one. WARNING:the name is the same as the first point. Knows the difference from the context...
									indexFirstPoint=j;
								}
								distanceFromStartPoint=sqrt(pow((alignedPatchPositionsX[i*(biggestTotalTime)+j]-alignedPatchPositionsX[i*(biggestTotalTime)+indexFirstPoint]),2)+pow((alignedPatchPositionsY[i*(biggestTotalTime)+j]-alignedPatchPositionsY[i*(biggestTotalTime)+indexFirstPoint]),2));
								distanceFromStartPointpx=sqrt(pow((alignedPatchPositionsXpx[i*(biggestTotalTime)+j]-alignedPatchPositionsXpx[i*(biggestTotalTime)+indexFirstPoint]),2)+pow((alignedPatchPositionsYpx[i*(biggestTotalTime)+j]-alignedPatchPositionsYpx[i*(biggestTotalTime)+indexFirstPoint]),2));
								lineToPrint=lineToPrint+d2sIfNot0(alignedPatchPositionsX[i*(biggestTotalTime)+j])+"\t";
								lineToPrint=lineToPrint+d2sIfNot0(alignedPatchPositionsXpx[i*(biggestTotalTime)+j])+"\t";
								lineToPrint=lineToPrint+d2sIfNot0(alignedPatchPositionsY[i*(biggestTotalTime)+j])+"\t";
								lineToPrint=lineToPrint+d2sIfNot0(alignedPatchPositionsYpx[i*(biggestTotalTime)+j])+"\t \t";
								
								lineToPrint=lineToPrint+d2sIfNot0(distanceFromStartPoint)+"\t";
								lineToPrint=lineToPrint+d2sIfNot0(distanceFromStartPointpx)+"\t \t";
								
								lineToPrint=lineToPrint+d2s(alignedPatchGaussianCenterSuccess[i*(biggestTotalTime)+j],0)+"\t";
								lineToPrint=lineToPrint+d2sIfNot0(alignedPatchXBackground[i*(biggestTotalTime)+j])+"\t";
								lineToPrint=lineToPrint+d2sIfNot0(alignedPatchXStdev[i*(biggestTotalTime)+j])+"\t";
								lineToPrint=lineToPrint+d2sIfNot0(alignedPatchXRsquared[i*(biggestTotalTime)+j])+"\t";
								lineToPrint=lineToPrint+d2sIfNot0(alignedPatchYBackground[i*(biggestTotalTime)+j])+"\t";
								lineToPrint=lineToPrint+d2sIfNot0(alignedPatchYStdev[i*(biggestTotalTime)+j])+"\t";
								lineToPrint=lineToPrint+d2sIfNot0(alignedPatchYRsquared[i*(biggestTotalTime)+j])+"\t \t";
							}
							print(f,lineToPrint);
							
						}
						print(f,"  ");
					}
					File.close(f);
					print(fileNameSpeed+" saved successfully!");
					
				}
				///////////////////////////////////////////////////////////////////////////////////
				
				///////////////  Concentrations vs distance  ////////////////////////////////////////////////
				if (generateFileConcvsDist)
				{
					f = File.open(fileNameConcvsDist);
					
					//Get index of first points for each patch
					indexFirstPoints=newArray(nbPatch);
					indexLastPoints=newArray(nbPatch);
					for (i=0;i<nbPatch;i++){
						foundFirstPoint=false;
						foundLastPoint=false;
						indexFirstPoint=0;
						indexLastPoint=0;
						for (j=0;j<biggestTotalTime;j++)
						{
							if(alignedPatchPositionsX[i*(biggestTotalTime)+j]!=0&&foundFirstPoint==false)
							{//find the first real time point for patch tracking
								foundFirstPoint=true;
								indexFirstPoint=j;
							}
							else if(alignedPatchPositionsX[i*(biggestTotalTime)+j]==0&&foundFirstPoint==true&&foundLastPoint==false)
							{// finds the last one: already found the first one so next time it's 0, it's the last one
								indexLastPoint=j-1;
								foundLastPoint=true;
							}
						}
						if(foundLastPoint==false){
							indexLastPoint=biggestTotalTime-1;
						}
						indexFirstPoints[i]=indexFirstPoint;
						indexLastPoints[i]=indexLastPoint;
					}
					
					lineToPrint=" TimePointNb \t time \t";
					for (i=0;i<nbPatch;i++) // Patch intensities (titles)
					{
						n=i+1;
						lineToPrint=lineToPrint+patchNames[i]+" Distance px \t"+patchNames[i]+" Path Length \t MSD \t Speed \t Concentration \t";
						if(positionsInConcVsDist){
							lineToPrint=lineToPrint+"X position \t Y position \t";
						}
						lineToPrint=lineToPrint+" \t";
					}
					print(f,lineToPrint);
					
					for (j=0;j<biggestTotalTime;j++)
					{
						lineToPrint=d2s(j,0)+"\t"+(j*timeDelay)+"\t";
						for (i=0;i<nbPatch;i++)
						{
							indexFirstPoint=indexFirstPoints[i];
							indexLastPoint=indexLastPoints[i];
							curIndex=i*(biggestTotalTime)+j;
							if((j>=indexFirstPoint)&&(j<=indexLastPoint)){
								distanceFromStartPointpx=sqrt(pow((alignedPatchPositionsXpx[i*(biggestTotalTime)+j]-alignedPatchPositionsXpx[i*(biggestTotalTime)+indexFirstPoint]),2)+pow((alignedPatchPositionsYpx[i*(biggestTotalTime)+j]-alignedPatchPositionsYpx[i*(biggestTotalTime)+indexFirstPoint]),2));
							}
							else{
								distanceFromStartPointpx=0;
							}
							lineToPrint=lineToPrint+d2sIfNot0(distanceFromStartPointpx)+"\t"+d2sIfNot0(alignedPatchDisplacementspx[i*(biggestTotalTime)+j])+"\t"+d2sIfNot0(alignedPatchMSDspx[i*(biggestTotalTime)+j])+"\t"+d2sIfNot0(alignedPatchDeltaDisplacementspx[i*(biggestTotalTime)+j])+"\t"+d2sIfNot0(alignedPatchIntensities[i*(biggestTotalTime)+j])+"\t";
							if(positionsInConcVsDist){
								lineToPrint=lineToPrint+d2sIfNot0(alignedPatchPositionsX[i*(biggestTotalTime)+j])+"\t"+d2sIfNot0(alignedPatchPositionsY[i*(biggestTotalTime)+j])+"\t";
							}
							lineToPrint=lineToPrint+" \t";
						}
						print(f,lineToPrint);
					}
					File.close(f);
					print(fileNameConcvsDist+" saved successfully!");			
				}
				///////////////////////////////////////////////////////////////////////////////////
				fileNameLog=getDirectory("image")+"Log.txt";
				selectWindow("Log");
				saveAs("Text", fileNameLog);
				
				run("Set Scale...", "distance="+(1/imagepw)+" known=1 pixel=1 unit=microns global");
				
				print("WARNING: Save this file as a valid excel file and with a different name next time you open it with excel!!");
			}
			
			myGarbageCollector();
		}
	setBatchMode(false);
	Stack.setPosition(channelInitial, sliceInitial, frameInitial);
}

macro "Setup"{	
	Dialog.create("Setup PatchTrackingTools (version "+version+")");
	Dialog.addNumber("Diameter of the ROI for the patch ", diameter);
	Dialog.addNumber("Diameter increase for the ROI for background ", dia2);
	Dialog.addNumber("Exposure Time (ms): ", exposureTimeCorrection);
	Dialog.addNumber("1 �M (or 1 molecule) is equivalent to a corrected intensity of (AU): ", calibrationFactor);
	Dialog.addNumber("Time delay (s): ", timeDelay);
	Dialog.addCheckbox("Use the slice with the maximum intensity for position measurement:", getPosDataFromMaxSlice);
	Dialog.addCheckbox("Save file with aligned intensities", generateFileIntensities);
	Dialog.addCheckbox("Save file with aligned displacements", generateFileDisplacements);
	Dialog.addCheckbox("Save file with aligned MSDs", generateFileMSDs);
	Dialog.addCheckbox("Save file with aligned positions", generateFilePositions);
	Dialog.addCheckbox("Save file with concentrations vs distances", generateFileConcvsDist);
	Dialog.addString("ROI name pattern (ex: <pattern>[number].zip) ", ROIFileNamepattern);
	Dialog.addNumber("Number of slices measured: ", nbSliceMeasured);
	Dialog.addCheckbox("Batch Mode",batchMode);
	Dialog.addNumber("Photobleaching rate for Hyperstack (-1 if estimated through RoiForPhotoBleaching.zip): ", GlobalPhotoBleachingRate);
	Dialog.addNumber("Window for averaging MSDs (-1 to use for the size of each ROISet): ", defaultWindowSizeForMSD);
	Dialog.addNumber("Max number of tries for Gaussian Fit ", maxGaussFittingRuns);
	Dialog.addNumber("Main channel number (type 0 for 1st channel)", mainChannelNumber);
	Dialog.addCheckbox("Check this box if data are from iQ10.1 software (uncheck if using iQ2)", iQ10Version);
	Dialog.addString("Channel pattern (ex: <protocol>[channelPattern]<endChannel>.tif) ", patternChannelName);
	Dialog.addChoice("Equation for photobleaching fit", newArray("y = a+b*exp(-c*x)","y = a*exp(-b*x)"),photobleachingEq);
	if(alignment==ON_WHOLE_TRACK){
		alignmentTxT="on whole track";
	}
	else if(alignment==ON_BEGINNING){
		alignmentTxT="on beginning";
	}
	else if(alignment==ON_END){
		alignmentTxT="on end";
	}	
	Dialog.addChoice("Alignment ", newArray("on whole track","on beginning","on end"),alignmentTxT);
	Dialog.addNumber("Number of points for alignment ", nbPointForAlignment);
	Dialog.addCheckbox("Make montage when deslicing",makeAllSliceMontageWhenDeslicing);
	Dialog.addNumber("Number of frames after and before the current roi for the montage",plusMinusTime);
	Dialog.show();
	diameter=Dialog.getNumber();
	dia2=Dialog.getNumber();
	exposureTimeCorrection = Dialog.getNumber();
	calibrationFactor=Dialog.getNumber();
	timeDelay = Dialog.getNumber();
	getPosDataFromMaxSlice=Dialog.getCheckbox();
	generateFileIntensities=Dialog.getCheckbox();
	generateFileDisplacements=Dialog.getCheckbox();
	generateFileMSDs=Dialog.getCheckbox();
	generateFilePositions=Dialog.getCheckbox();
	generateFileConcvsDist=Dialog.getCheckbox();
	ROIFileNamepattern=Dialog.getString();
	nbSliceMeasured=Dialog.getNumber();
	batchMode=Dialog.getCheckbox();
	GlobalPhotoBleachingRate=Dialog.getNumber();
	defaultWindowSizeForMSD=Dialog.getNumber();
	maxGaussFittingRuns=Dialog.getNumber();
	mainChannelNumber=Dialog.getNumber();
	iQ10Version=Dialog.getCheckbox();
	patternChannelName=Dialog.getString();
	photobleachingEq=Dialog.getChoice();
	alignmentTxt=Dialog.getChoice();
	if(startsWith(alignmentTxt,"on whole track")){
		alignment=ON_WHOLE_TRACK;
	}
	else if(startsWith(alignmentTxt,"on beginning")){
		alignment=ON_BEGINNING;
		}
	else if(startsWith(alignmentTxt,"on end")){
		alignment=ON_END;
	}
	nbPointForAlignment=Dialog.getNumber();
	makeAllSliceMontageWhenDeslicing=Dialog.getCheckbox();
	plusMinusTime=Dialog.getNumber();
}

/********************************************************* 
*********************   Display ***************************
***********************************************************/
macro '** Display **'{} 

macro 'OptimizeContrast [o]'{
	if (LUTnb==1){
		LUTnb=2;
		run("Grays");
		run("Invert LUT"); //run("Rainbow RGB");
		run("Enhance Contrast", "saturated=0.5");
	}
	else if (LUTnb==2){
		LUTnb=3;
		//run("Fire");
		run("Ice");
		run("Enhance Contrast", "saturated=0.5");
	}
	else if (LUTnb==3){
		LUTnb=1;
		run("Grays");
		//run("Invert LUT"); //run("Rainbow RGB");
		run("Enhance Contrast", "saturated=0.5");
	}
}

function getColorOverlay(LUT,highlight){
	if (LUT==1){ // Inverted Grays
		if (highlight){
			return "blue";
		}
		else{
			return "red";
		}
	}
	else if (LUT==2){ // Fire/Ice
		if (highlight){
			return "gray";
		}
		else{
			return "black";
		}
	}
	else if (LUT==3){// Grays
		if (highlight){
			return "yellow";
		}
		else{
			return "red";
		}
	}
}

macro 'Same Contrast For All [O]'{
	selectImage(mainImageId);
	imageNameRoot=substring(mainImageName,0,lengthOf(mainImageName)-18)+"HS";
	Stack.getDimensions(imWidth, imHeight, nbChannels, tmpNull, nbFrames);
	Stack.getPosition(curChannelNb, curSliceNb, curFrameNb);
	imageNb=1;
	for (ch=1;ch<=1;ch++){
		selectImage(mainImageId);
		Stack.setChannel(ch);
		getMinAndMax(minIntensity, maxIntensity);
		getLut(reds, greens, blues);
		for (sl=1; sl<=1; sl++) {
			selectWindow(imageNameRoot+"-"+imageNb+".tif");
			run("Invert LUT");
			setMinAndMax(minIntensity/nbSlices, maxIntensity/nbSlices);
			setLut(reds, greens, blues);
			imageNb++;
		}
	}
	selectImage(mainImageId);
	Stack.setPosition(curChannelNb, curSliceNb, curFrameNb);
}

macro 'Reduce field to 50% of illumination'{
	reduceField(0.5);
	selectImage(mainImageId);
}

function reduceField(percentage){
	getLut(tmpreds, tmpgreens, tmpblues);
	getMinAndMax(tmpminIntensity, tmpmaxIntensity);
	checkMainImageName();
	selectImage(mainImageId);
	idMain=getImageID();
	dirImageName=getDirectory("image");
	parentDirImageName=File.getParent(dirImageName);
	open(parentDirImageName+fileSep+"AverageUnevenIllumination.tif");
	selectWindow("AverageUnevenIllumination.tif");
	idUneven=getImageID();
	run("Macro...", "code=v=(v>"+percentage+")");
	imageCalculator("Multiply stack",mainImageName,"AverageUnevenIllumination.tif");
	selectImage(idUneven);
	close();
	selectImage(idMain);
	setLut(tmpreds, tmpgreens, tmpblues);
	setMinAndMax(tmpminIntensity, tmpmaxIntensity);
}

macro 'Close all except current [x]'{
	// My old version
	/*
	curImageID=getImageID();
	while (nImages>1) { 
		selectImage(floor(nImages*random)+1); 
		if (getImageID()!=curImageID){
			close(); 
		}
	}*/

	// ImageJ new version
	close("\\Others");
	
	myGarbageCollector();
}

macro 'Close all [X]'{
	// My old version
	/*
	while (nImages>0) { 
		selectImage(floor(nImages*random)+1); 
		close(); 
	}*/

	// ImageJ new version
	close("*");
	
	myGarbageCollector();
}

macro 'Zoom to selection [0]'{
	checkMainImageName();
	run("To Selection");
	run("Out");
	run("Out");
}

macro 'Set photobleaching Correction [p]' {
	setPhotoBleachingCorrection();
}

macro 'Cancel photobleaching Correction' {
	cancelPhotoBleachingCorrection();
}



/********************************************************* 
********************* Player ******************************
***********************************************************/
macro '** Player **'{} 

macro 'Play [y]'{
	Stack.getDimensions(imWidth, imHeight, nbChannels, tmpNull, nbFrames); 
	playing=!playing;
	maxcnt=4*nbFrames;
	cnt=0;
	//sliceNb=getSliceNumber();
	Stack.getPosition(curChannelNb, curSliceNb, curFrameNb);
	while(playing&&cnt<maxcnt){
		Stack.getPosition(tmpNull, tmpNull0, curFrameNb);
		if (curFrameNb==nbFrames){
			for (i=1; i<=nImages; i++) {
				selectImage(i);
				Stack.setPosition(curChannelNb, curSliceNb, 1);
			}
		}
		else{
			run('Next Frame [l]');
		}
		wait(1000/imagePerSec);
		cnt++;
	}
	playing=0;
}

macro 'Slower [f]'{
	imagePerSec=maxOf(1,imagePerSec-1);
}

macro 'Faster [F]'{
	imagePerSec=imagePerSec+1;
}

macro '1st Frame [1]'{
	checkMainImageName();
	Stack.getPosition(curChannelNb, curSliceNb, curFrameNb);
	curImageName=mainImageName;
	for (i=1; i<=nImages; i++) {
		selectImage(i);
		Stack.setPosition(curChannelNb, curSliceNb, 1);
	}
	selectImage(curImageName);
}

macro 'Previous Frame [k]'{
	checkMainImageName();
	curImageName=mainImageName;
	Stack.getPosition(curChannelNb, curSliceNb, curFrameNb);
	previousFrameNb=maxOf(curFrameNb-1,1);
	Stack.setPosition(curChannelNb, curSliceNb, previousFrameNb);
	if (isOpen(sliceWindowIDArray)){
		selectImage(sliceWindowIDArray);
		Stack.setPosition(curChannelNb, curSliceNb, previousFrameNb);
	}
	selectImage(curImageName);
}

macro 'Next Frame [l]'{
	checkMainImageName();
	curImageName=mainImageName;
	Stack.getPosition(curChannelNb, curSliceNb, curFrameNb);
	Stack.getDimensions(imWidth, imHeight, nbChannels, tmpNull, nbFrames) 
	nextFrameNb=minOf(curFrameNb+1,nbFrames);
	Stack.setPosition(curChannelNb, curSliceNb, nextFrameNb);
	if (isOpen(sliceWindowIDArray)){
		selectImage(sliceWindowIDArray);
		Stack.setPosition(curChannelNb, curSliceNb, nextFrameNb);
	}
	selectImage(curImageName);
}

macro 'Previous ROI [K]'{
	checkMainImageName();
	i=roiManager("index");
	count=roiManager("count");
	if (i>0){
		roiManager("select",i-1);
	}
	HSname=substring(mainImageName,0,lengthOf(mainImageName)-18)+"HS";
	if (isOpen(HSname+"-1.tif")){
		Overlay.remove;
		getSlicesIntensities(nbSlices,offSetXReducedSlices,offSetYReducedSlices,-1,true);
	}
	selectImage(mainImageName);
}

macro 'Next ROI [L]'{ 
	checkMainImageName();
	i=roiManager("index");
	count=roiManager("count");
	if (i<count-1){
		roiManager("select",i+1);
	}
	HSname=substring(mainImageName,0,lengthOf(mainImageName)-18)+"HS";
	if (isOpen(HSname+"-1.tif")){
		Overlay.remove;
		getSlicesIntensities(nbSlices,offSetXReducedSlices,offSetYReducedSlices,-1,true);
	}
	selectImage(mainImageId);
		
}

/********************************************************* 
********************* Other Tools ***********************
***********************************************************/
macro '** Other Tools **'{} 


macro 'Version'{
	print("PatchTrackingTools version "+version);
}

macro 'Garbage collector [C]' {
	myGarbageCollector();
}

macro 'Make Montage [M]' {// Multi channel montage
	Stack.getDimensions(iniWidth, iniHeight, iniNbChannels, iniNbSlices, iniNbFrames);
	frameForContrast=newArray(iniNbChannels);
	Dialog.create("Setup Montage");
	Dialog.addNumber("First frame of the montage ", beginFrame);
	Dialog.addNumber("Last frame of the montage ", endFrame);
	Dialog.addNumber("Scale", scaleMontage);
	for(ch=0;ch<iniNbChannels;ch++){
		Dialog.addNumber("Frame for contrast setup for channel "+(ch+1), frameForContrast[ch]);
	}
	Dialog.addMessage("   Note: type 0 for current contrast in a channel");
	Dialog.show();
	beginFrame=Dialog.getNumber();
	endFrame=Dialog.getNumber();
	scaleMontage=Dialog.getNumber();
	for(ch=0;ch<iniNbChannels;ch++){
		frameForContrast[ch]=Dialog.getNumber();
	}
	setBatchMode(true);
	imageTitle=getTitle();
	run("Duplicate...",  "title=["+imageTitle+"] duplicate");
	CroppedStackID=getImageID();
	columns=endFrame-beginFrame+1;
	Stack.getDimensions(tmpimWidth, tmpimHeight, tmpnbChannels, tmpnbSlices, tmpnbFrames);
	Stack.getPosition(tmpChannelNb, tmpSliceNb, tmpFrameNb);
	idMontages=newArray(tmpnbChannels+1);
	if (frameForContrast[0]!=0)
	{
		for (ch=0;ch<tmpnbChannels;ch++){
			Stack.setPosition(ch+1, tmpSliceNb, frameForContrast[ch]);
			resetMinAndMax();
			run("Enhance Contrast", "saturated=0.35");
		}
	}
	for (ch=0;ch<tmpnbChannels;ch++){//Makes 1 montage per channel
		selectImage(CroppedStackID);
		Stack.setPosition(ch+1, tmpSliceNb, tmpFrameNb);
		run("Reduce Dimensionality...", "  frames keep");
		tmpReducedStackID=getImageID();
		run("Grays");
		run("Invert LUT");
		if (frameForContrast[0]!=0)
		{
			Stack.setPosition(1, tmpSliceNb, frameForContrast[ch]);
			resetMinAndMax();
			run("Enhance Contrast", "saturated=0.35");
		}
		run("Make Montage...", "columns="+columns+" rows=1 scale="+scaleMontage+" first="+beginFrame+" last="+endFrame+" increment=1 border=0 font=12");
		run("RGB Color");
		curTitle=getTitle();
		curID=getImageID();
		idMontages[ch]=curID;
		rename(curTitle+curID);
		selectImage(tmpReducedStackID);
		close();
	}
	//Makes 1 montage for composite stack
	selectImage(CroppedStackID);
	run("Stack to RGB", "frames keep");
	rgbImageID=getImageID();
	run("Make Montage...", "columns="+columns+" rows=1 scale="+scaleMontage+" first="+beginFrame+" last="+endFrame+" increment=1 border=0 font=12");
	curTitle=getTitle();
	curID=getImageID();
	idMontages[tmpnbChannels]=curID;
	rename(curTitle+curID);
	combinedMontageID=idMontages[0];
	selectImage(CroppedStackID);
	close();
	selectImage(rgbImageID);
	close();
	
	for (i=1;i<idMontages.length;i++){// Combine all montages
		selectImage(combinedMontageID);
		CombinedMontageTitle=getTitle();
		selectImage(idMontages[i]);
		curMontageTitle=getTitle();
		run("Combine...", "stack1=["+CombinedMontageTitle+"] stack2=["+curMontageTitle+"] combine");
		combinedMontageID=getImageID();
	}
	setBatchMode(false);
}

macro 'Crop and save uncorrected HS and _SUM_Corrected file in new folder [R]'{
	runMacro(getDirectory("macros")+fileSep+"toolsets"+fileSep+"macros for PatchTrackingTools"+fileSep+"CropAndSaveHSAndSUM_Corrected.ijm");
}


macro 'Circle cells'{ //CMC
	// Image Process
	title=getTitle();
	Stack.getPosition(channelIni, sliceIni, frameIni);
	selectWindow(title);
	run("8-bit");
	run("FJ Laplacian", "compute smoothing=5 detect");
	rename("Edges");
	setAutoThreshold();
	run("Analyze Particles...", "size=1300-6000 pixel circularity=0.30-1.00 show=Nothing exclude clear include add");
	setOption("Show All",false);
	
	// Convex Hull selections
	count=roiManager("count");
	for(i=0;i<count;i++){
		roiManager("select",0);
		Stack.setChannel(channelIni);
		run("Convex Hull");
		roiManager("add");
		roiManager("select", 0);
		roiManager("delete");
	}
	roiManager("Deselect");
	setOption("Show All",false);
	
	// Remove outliers
	selectWindow(title);
	roiManager("Deselect");
	run("Set Measurements...", "area mean standard min integrated redirect=None decimal=5");
	run("Measure");
	mean=newArray(count);
	curSum=0;
	curDev =0;
	for(i=0;i<count;i++){
		mean[i]=getResult("Mean",i);
		Stack.setChannel(channelIni);
		curSum+= mean[i];
	}
	
	newMean=curSum/count;
	for(i=0;i<count;i++){
		curDev+=(mean[i]-newMean)*(mean[i]-newMean);
	}
	
	sd=sqrt(curDev/(count-1));
	maxAllow=newMean+2*sd;
	numX=0;
	
	for(i=0;i<count;i++){
		if (mean[i]>maxAllow){
			roiManager("Select",i);
			Stack.setChannel(channelIni);
			roiManager("Rename", "X");
			numX+=1;
		}	
	}
	
	for(i=0;i<numX;i++){
		curName ="";
		newCount=count-i;
		j=0;
		do {
			curName=call("ij.plugin.frame.RoiManager.getName", j);
			j++;
		} while (j<newCount && curName !="X");
		if(curName=="X"){
			roiManager("Select",j-1);
			Stack.setChannel(channelIni);
			roiManager("Delete");
		}
	}
	
	
	// Rename new ROIS
	curCount = roiManager("count");
	
	// Rename ROIs by cell number
	for(i=0;i<curCount;i++){
		roiManager("select",i);
		Stack.setChannel(channelIni);
		roiManager("rename",i+1);
	}
	
	roiManager("Deselect");
	run("Select None");
	selectWindow(title);
	setOption("ShowAll",true);
}

macro 'Correct for Photobleaching (Estimated from the main sum projection stack)'{
	correctForPhotobleaching();
}

function correctForPhotobleaching(){
	if (photoBleachingSet==true)
	{
		print("Correcting for Photobleaching "+getTitle());	
		curImageName=getTitle();
		Stack.getPosition(curChannel, curSlice, curFrame);
		Stack.getDimensions(imWidth, imHeight, nbChannels, tmpNull, nbFrames);
		selectImage(curImageName);
		for (i=2; i<=nbFrames; i++) {
			Stack.setPosition(curChannel, curSlice, i);
			run("Multiply...", "value="+photoBleachingCorrection[i-1]+" slice");
		}
		Stack.getPosition(curChannel, curSlice, curFrame);
	}
}

function setPhotoBleachingCorrection(){
	photoBleachingSet=true;
	curImageName=getTitle();
	Stack.getPosition(curChannel, curSlice, curFrame);
	Stack.getDimensions(imWidth, imHeight, nbChannels, tmpNull, nbFrames);
	setBatchMode(true);
	photoBleachingCorrection=newArray(nbFrames);
	selection=false;
	if (selectionType()!=-1){selection=true;}
	if(selection){
		getSelectionCoordinates(xSelection, ySelection); // Gets the current selection to set it back at the end
		typeSelection = selectionType();
	}
	Stack.setPosition(curChannel, curSlice, 1);
	run("Select All");
	run("Measure");
	photoBleachingCorrection[0]=getResult("Mean",nResults-1);
	
	print(nbChannels+" "+getTitle());
	Stack.setChannel(curChannel);
	for (i=2; i<=nbFrames; i++) {
		Stack.setPosition(curChannel, curSlice, i);
		if (GlobalPhotoBleachingRate==-1){
			run("Measure");
			photoBleachingCorrection[i-1]=photoBleachingCorrection[0]/getResult("Mean",nResults-1);
		}
		else{
			photoBleachingCorrection[i-1]=exp(GlobalPhotoBleachingRate*(i-1)*nbSlices);
		}
		run("Multiply...", "value="+photoBleachingCorrection[i-1]+" slice");
	}
	setBatchMode(false);
	Stack.setPosition(curChannel, curSlice, curFrame);
	selectImage(curImageName);
	if(selection){
		makeSelection(typeSelection, xSelection, ySelection);//Set back the selection
	}
	else{
		makeRectangle(0,0,0,0);
	}
}

function cancelPhotoBleachingCorrection(){
	Stack.getPosition(curChannel, curSlice, curFrame);
	Stack.getDimensions(imWidth, imHeight, nbChannels, tmpNull, nbFrames);
	checkMainImageName();
	selection=false;
	if (selectionType()!=-1){selection=true;}
	if (photoBleachingSet==true){ // Cancel the rough photobleaching correction in case it was done on this image	
		if(selection){
			getSelectionCoordinates(xSelection, ySelection); // Gets the current selection to set it back at the end
			typeSelection = selectionType();
		}
		run("Select All");
		for (i=2; i<=nbFrames; i++) {
			Stack.setPosition(curChannel, curSlice, i);
			print(photoBleachingCorrection[i-1]);
			run("Divide...", "value="+photoBleachingCorrection[i-1]+" slice");
		}
		Stack.setPosition(curChannel, curSlice, curFrame);
		if(selection){
			makeSelection(typeSelection, xSelection, ySelection);//Set back the selection
		}
		else{
			makeRectangle(0,0,0,0);
		}
	}
}


macro 'Correct for Photobleaching (Estimated from the current selection)'{
	correctCurrentImageForPhotobleaching();
}

function correctCurrentImageForPhotobleaching(){ //Independetly from the global bleaching correction 
	curImageName=getTitle();
	Stack.getPosition(curChannel, curSlice, curFrame);
	Stack.getDimensions(imWidth, imHeight, nbChannels, tmpNull, nbFrames);
	setBatchMode(true);
	selection=false;
	if (selectionType()==-1){run("Select All");}
	roiManager("Add");
	curBleach=newArray(nbFrames);
	index=newArray(nbFrames);
	toPlot=newArray(nbFrames);
	Stack.setPosition(curChannel, curSlice, 1);
	run("Measure");
	curBleach[0]=getResult("Mean",nResults-1);
	toPlot[0]=1;
	index[0]=0;
	print(nbChannels+" "+getTitle());
	Stack.setChannel(curChannel);
	nbRoi=roiManager("count");
	for (i=2; i<=nbFrames; i++) {
		roiManager("Select",nbRoi-1);
		Stack.setPosition(curChannel, curSlice, i);
		run("Measure");
		curBleach[i-1]=curBleach[0]/getResult("Mean",nResults-1);
		toPlot[i-1]=1/curBleach[i-1];
		index[i-1]=i-1;
		run("Select All");
		run("Multiply...", "value="+curBleach[i-1]+" slice");
	}
	setBatchMode(false);
	roiManager("Select",nbRoi-1);
	roiManager("Delete");
	Stack.setPosition(curChannel, curSlice, curFrame);
	
	Plot.create("Intensity", "Time", "Intensity (A.U.)", index, toPlot);
	Plot.setColor("red");
	Plot.show();
}

macro 'Change RoiNames'{//Rename all rois in a folder with their position information
	getPixelSize(unit, pixelWidth, pixelHeight);
	selectImage(mainImageId);
	dir=getDirectory("image");
	imageHeight=getHeight();
	imageWidth=getWidth();
	listFiles=getFileList(getDirectory("image"));
	for(i=0;i<listFiles.length;i++){// Counting the nb of ROIset files and search for Photobleaching ROIset file
		tmp=split(listFiles[i],".");
		if(endsWith(listFiles[i],".zip")&&startsWith(listFiles[i],ROIFileNamepattern)&&lengthOf(tmp)==2)
		{
			roiManager("Reset");
			roiManager("Open", getDirectory("image")+listFiles[i]);
			curROINb=roiManager("select",0);
			run("Measure");
			timepoint=getResult("Slice",nResults-1);
			Xpc=(getResult("X",nResults-1)/pixelWidth)/imageWidth*100;
			Ypc=(getResult("Y",nResults-1)/pixelHeight)/imageHeight*100;
			newName=tmp[0]+"."+timepoint+"."+d2s(Xpc,0)+"."+d2s(Ypc,0)+".zip";
			success=File.rename(dir+fileSep+listFiles[i],dir+fileSep+newName);
			if(success){
				print(listFiles[i]+" changed into "+newName);
			}
			else{
				print("Failed to rename "+listFiles[i]+" into "+newName);
			}	
		}
		
	}
}

macro 'Synthesis Patch'{// Fills the RoiManager with one Roi for each of the Roisets in the main image folder 
	listFiles=getFileList(getDirectory("image"));
	nbROIFiles=0;
	roinb=0;
	roiManager("Reset");
	for(i=0;i<listFiles.length;i++){
		if(endsWith(listFiles[i],".zip"))
		{
			if(startsWith(listFiles[i],ROIFileNamepattern)){
				nbROIFiles++;
				roiManager("Open", getDirectory("image")+listFiles[i]);
				roiManager("select",roinb);
				tmpROIName=getInfo("roi.name");
				splitRoiName=split(tmpROIName,"-");
				splitRoiSetName=substring(listFiles[i],0,lengthOf(listFiles[i])-4);
				newROIName=splitRoiSetName+"-"+splitRoiName[0];
				roiManager("Rename", newROIName);
				run("Remove Extra Time Points");
				wait(1000);
				roinb++;
			}
		}	
	}
}

macro 'printExposureTime'{ // print in the log window the sposure times for all the movies in the folder
	dir=getDirectory("Select Directory for Image Conversion:");
	print("dir:"+dir);
	curFilesFolders=getFileList(dir);
	var sep=File.separator();
	separator=sep+sep;
	success=0;
	print("Exposure=");
	for(i=0;i<curFilesFolders.length;i++){
		if(File.isDirectory(dir+curFilesFolders[i])){
			curDir=dir+curFilesFolders[i];
			subFiles=getFileList(curDir);
			for(j=0;j<subFiles.length;j++){
				file=subFiles[j];
				if(endsWith(file,".txt")){
					strFile=File.openAsString(curDir+file);
					lines=split(strFile,"\n");
					nLines=lengthOf(lines);
					found=0;
					j=0;
					while (found==0&&j<nLines)// scale
					{
						if (startsWith(lines[j],"Exposure Time - Actual=")){
							found=1;
						}
						else{
							j++;
						}
					}
					xLine=split(lines[j],"=");
					expTime=xLine[1];
					print(expTime+" "+file);
				}
			}
			
		}
	}
}


macro 'List all tools and shortcuts'{
	path=getDirectory("macros")+fileSep+"toolsets"+fileSep+"PatchTrackingTools.txt";
	strFile=File.openAsString(path);
	lines=split(strFile,"\n");
	nLines=lengthOf(lines);
	for (i=0;i<nLines;i++){
		if (matches(lines[i],"^\t*(macro).*")){//All macros
			sp=split(lines[i],"\'\"");
			print(sp[1]);
		}
	}
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// FUNCTIONS
// Gauss Fit Track                          
// Chad McCormick
// 2009 April 17 Pollard Lab
// Julien Berro (recursivity & quality control)
// 2009 July 17 Pollard Lab
function gaussCenter(diam, curROI, newX, newY,maxNbOfRun,guessBG,frameNb){
	// Deprecated ?
	// Get the coordinates (in pixels unit) of the peak of the gaussians after projection on X and Y
	// Scale must set in pixel before (?????)
	// frameNb is the frame nb in stack or the absolute slice nb in hyperstack (or slice nb as if the HS was a stack): (framenb-1)*slices+sliceNb
	Stack.getPosition(curChannel, curSlice, curFrame);
	if (maxNbOfRun>0)
	{
		run("Set Measurements...", "area mean centroid center bounding slice redirect=None decimal=5");
		xCenter=newX+10*diam; // subpx res X: on purpose far from newX just to run the gaussian fit
		yCenter=newY+10*diam;
		iter=1;
		xBg=-1;
		xStdev=-1;
		xRsquared=-1;
		yBg=-1;
		yStdev=-1;
		yRsquared=-1;
		defEnlarge=-1;			
		while ((abs(xCenter-newX)>diam||abs(yCenter-newY)>diam)&&iter>=0&&(diam+iter*defEnlarge>0)) //Check if the new coordiantes are not crazy
		{
			
			newDiam=diam+iter*defEnlarge;
			
			roiManager("select",curROI);
			Stack.setChannel(curChannel);
			if (frameNb==-1){
				Stack.getPosition(curChannel, curSlice, curFrame);
			}
			else{
				Stack.getPosition(curChannel, curSlice, curFrame);
				curFrame=frameNb;
				Stack.setPosition(curChannel, curSlice, curFrame);
			}
			run("Enlarge...", "enlarge="+defEnlarge);
			run("Specify...", "width="+newDiam+" height="+newDiam+" x="+round(newX)+" y="+round(newY)+" slice="+curSlice+" centered");
			run("Measure");
			
			startX=getResult("BX",nResults-1);
			startY=getResult("BY",nResults-1);
			Stack.setChannel(curChannel);
			run("Clear Results");
			xValues=newArray(newDiam);
			yValues=newArray(newDiam);
			for(i=0;i<newDiam;i++){
				xValues[i]=i;
				yValues[i]=i;
			}
			
			sumGaussX=getProfile();
			setKeyDown("alt");
			sumGaussY=getProfile();
			setKeyDown("none");
			
			if (guessBG!=-1){
				dIni=guessBG;
				equationGaussian="y="+dIni+"+(b-"+dIni+")*exp(-(x-c)*(x-c)/(2*a*a))";
				//0.a. stdev
				//1.b. max
				//2.c. center
			}
			else{
				equationGaussian="y=d+(b-d)*exp(-(x-c)*(x-c)/(2*a*a))";
				dIni=maxOf(sumGaussX[0],sumGaussX[lengthOf(sumGaussX)-1]);
				//0.a. stdev
				//1.b. max
				//2.c. center
				//3.d. bg
			}
			
			bIni=maxOfArray(sumGaussX);
			cIni=lengthOf(sumGaussX)/2;
			aIni=lengthOf(sumGaussX)/2;
			initialGuessX=newArray(aIni,bIni,cIni,dIni);
			Fit.doFit(equationGaussian,xValues, sumGaussX,initialGuessX);
			if (guessBG!=-1){
				xBg=guessBG;
			}
			else{
				xBg=Fit.p(3);
			}
			xCenter=startX+Fit.p(2);
			xStdev=Fit.p(0);
			xRsquared=Fit.rSquared;
			
			bIni=maxOfArray(sumGaussY);
			cIni=lengthOf(sumGaussY)/2;
			aIni=lengthOf(sumGaussY)/2;
			initialGuessY=newArray(aIni,bIni,cIni,dIni);
			Fit.doFit(equationGaussian,yValues,sumGaussY,initialGuessY);
			
			if (guessBG!=-1){
				yBg=guessBG;
			}
			else{
				yBg=Fit.p(3);
			}
			yCenter=startY+Fit.p(2);
			yStdev=Fit.p(0);
			yRsquared=Fit.rSquared;
			
			selectImage(mainImageId);
		
			gaussCenterCoord=newArray(xCenter,yCenter,maxNbOfRun,xBg,xStdev,xRsquared,yBg,yStdev,yRsquared);
			if(xRsquared<0.85||yRsquared<0.85||xBg<0||yBg<0){
				if(maxNbOfRun>1)
				{
					gaussCenterCoord=gaussCenter(diam+diameterIncrease, curROI, newX, newY,maxNbOfRun-1,guessBG,frameNb);
				}
				else
				{
					print("   WARNING: Slice "+curSlice+" ROI n�"+ curROI+" - Bad Gaussian fit (R^2x="+xRsquared+" R^2y="+yRsquared+")");
					print("	    Mass center coordinates used instead!" );
					xCenter=newX;
					yCenter=newY;
					gaussCenterCoord=newArray(newX,newY,0,xBg,xStdev,xRsquared,yBg,yStdev,yRsquared);
				}
			}
			
			iter=iter-1;
		}
		if(abs(xCenter-newX)>diam||abs(yCenter-newY)>diam)
		{
			if(maxNbOfRun>1)
			{
				gaussCenterCoord=gaussCenter(diam+diameterIncrease, curROI, newX, newY,maxNbOfRun-1,guessBG,frameNb);
			}
			else
			{
				print("WARNING!!: Gaussian fit didn't work for ROI number "+ curROI);
				print("	Mass center coordinates used instead!" );
				xCenter=newX;
				yCenter=newY;
				gaussCenterCoord=newArray(newX,newY,0,xBg,xStdev,xRsquared,yBg,yStdev,yRsquared);
			}
		}
	}
	else
	{
		gaussCenterCoord=newArray(newX,newY,0,0,0,0,0,0,0);
	}
	return gaussCenterCoord;
	
}

function d2sIfNot0(num) {
	str="";
	if(num!=0)
		{str=d2s(num,4);}
	return str;
}

function myGetResult(property,ROINb){
	Stack.getPosition(channelTmp, sliceTmp, frameTmp);
	isStack=Stack.isHyperstack;
	roiManager("Select",ROINb);
	if (isStack){
		Stack.setChannel(channelTmp);
	}
	run("Measure");
	res=getResult(property,nResults-1);
	return res;
	
}

function getSlicesIntensities(nbZSlices,xOffset,yOffset, offSetPatch,fromROIManager){
	//offSetPatch is the nb of ROI when tracking a patch (index of the first background), 
	//    -1 if not in postprocessing (takes a region 2px bigger in diameter for background correction)
	//    -2 for raw data: No background correction at all
	// fromROIManager is a boolean to know if the intensities is measures from a ROI from ROIManager (true) or a random ROI (false) 
	curImageName=mainImageName;
	HSname=substring(curImageName,0,lengthOf(curImageName)-18)+"HS";
	tmpname=getTitle();
	Stack.getPosition(curChannel, curSlice, curFrame);
	Stack.getDimensions(imWidth, imHeight, nbChannels, tmpNull, nbFrames);
	curROIIndex=roiManager("index");
	dbprint("curROIIndex="+curROIIndex);
	if (fromROIManager==true&&curROIIndex>0){// if an ROI is selected, check if it is marked as bad
		badROI=isBadROI(curROIIndex); 
	} 
	else{
		badROI=false;
	}
	getSelectionBounds(x, y, width, height);
	firstSliceName=HSname+"-1.tif";
	if (isOpen(firstSliceName)){
		selectImage(firstSliceName);
		Overlay.remove;
	
		for (channel=1;channel<=nbChannels;channel++){
			intensities=newArray(nbZSlices);
			percentages=newArray(nbZSlices);
			backgrounds=newArray(nbZSlices);
			run("Measure");                              
			sum=0;
			maxSlice=0;
			indexMaxSlice=-1;
			
			Stack.getDimensions(widthMontage, heightMontage, tmpNull1, tmpNull2, tmpNull3);
			widthSlice=round(widthMontage/nbZSlices);
			heightSlice=round(heightMontage/nbChannels);
			for(i=1; i<=nbZSlices; i++){
				Stack.setPosition(curChannel, curSlice, curFrame);
				curColor=getValue("color.foreground");
				makeOval(x-xOffset+(i-1)*widthSlice, y-yOffset+(channel-1)*heightSlice, width, height);
				run("Measure");
				patchMean=getResult("Mean",nResults-1);
				patchArea=getResult("Area",nResults-1);
				totalPatchInt=getResult("Mean",nResults-1)*patchArea;
				Stack.setChannel(curChannel);
				if (offSetPatch==-2){// Raw data: No background correction at all
					intensities[i-1]=totalPatchInt;
					run("Enlarge...", "enlarge="+dia2+" pixel");
					run("Measure");
					BackgroundMean=getResult("Mean",nResults-1);
					BackgroundArea=getResult("Area",nResults-1);
					Stack.setChannel(curChannel);
					BackgroundPerUnitArea=(BackgroundArea*BackgroundMean-totalPatchInt)/(BackgroundArea-patchArea);
					backgrounds[i-1]=BackgroundPerUnitArea*patchArea;
					run("Enlarge...", "enlarge=-"+dia2+" pixel");			
				}
				else{ 
					if (offSetPatch==-1){
						run("Enlarge...", "enlarge="+dia2+" pixel");
					}
					else{
						curROIIndex=roiManager("index");
						roiManager("select",curROIIndex+offSetPatch);
						Stack.setChannel(curChannel);
					}
					run("Measure");
					BackgroundMean=getResult("Mean",nResults-1);
					BackgroundArea=getResult("Area",nResults-1);
					Stack.setChannel(curChannel);
					BackgroundPerUnitArea=(BackgroundArea*BackgroundMean-totalPatchInt)/(BackgroundArea-patchArea);
					backgrounds[i-1]=BackgroundPerUnitArea*patchArea;
					intensities[i-1]=totalPatchInt-BackgroundPerUnitArea*patchArea;
					
				}
				if (intensities[i-1]<0){
					intensities[i-1]=0;
				}
				
				sum=sum+intensities[i-1];
				if(intensities[i-1]>maxSlice){
					maxSlice=intensities[i-1];
					indexMaxSlice=i;
				}
				if (offSetPatch==-1){
					run("Enlarge...", "enlarge=-"+dia2+" pixel");
				}
				else if (offSetPatch!=-2){
					curROIIndex=roiManager("index");
					roiManager("select",curROIIndex-offSetPatch);
					Stack.setChannel(curChannel);	
				}
				
				run("Add Selection...");//Overlay.addSelection
				Overlay.show;
			}
			for(i=1; i<=nbZSlices; i++){
				percentages[i-1]=intensities[i-1]/sum;
			}
			
			for(i=1; i<=nbZSlices; i++){
				percentages[i-1]=intensities[i-1]/sum;
				infos=d2s(percentages[i-1]*100,0)+"\%";
				setColor("blue");
				if (i==indexMaxSlice){
					infos=infos+" X";
					setColor("red");
				}
				if (badROI==true){infos=infos+" Bad!!";}
				setMetadata("Label", infos);
				setFont("SanSerif", 3, "antialiased");
				xText=round((i-1+0.02)*widthSlice); // Left
				yText=round((channel)*heightSlice); //middle-ish
				Overlay.drawString(infos, xText, yText);
				Overlay.show;
			}
		}
	}
	run("Select None");
	selectImage(curImageName);
}


function getSlicesIntensitiesFromHS(ROIIndex,nbZSlices, offSetPatch,rateBleaching,volodiaStyle){
	// assumes that 1. the HS file is open, 
	//		2. the projected image is selected,
	//		/////////3. a ROI is selected in the ROI manager
	// offSetPatch is the nb of ROI when tracking a patch (index of the first background), -1 if not in postprocessing
	// RETURNS 1. index of slice with max intensity
	// 	   2. coordinates of center of mass of the patch on the slice with max intensity
	//	   3. coordinates of center of the patch after gaussian fitting on the slice with max intensity
	// MODIFIES global tables intensities[] and percentages[] 
	
	
	//Projection image must be selected before the use of
	curImageName=getTitle();
	Stack.getPosition(channelIni, sliceIni, frameIni);
	run("Measure");
	Stack.getDimensions(tmpimWidth, tmpimHeight, tmpnbChannels, tmpnbSlices, tmpnbFrames);
	RoiFrameNb=frameIni;
	
	HSname=substring(curImageName,0,lengthOf(curImageName)-18)+"HS";
	selectImage(HSname+".tif");
	roiManager("Select",ROIIndex);
	Stack.setChannel(channelIni);
	intensities=newArray(nbZSlices);
	bgIntensities=newArray(nbZSlices);
	percentages=newArray(nbZSlices);
	run("Measure");
	sum=0;
	maxSlice=0;
	indexMaxSlice=-1; // values from 0 to nbSlices-1
	
	
	getSelectionBounds(x, y, width, height);
	//move ROI for cropped
	for(i=1; i<=nbZSlices; i++){
		Stack.setFrame(RoiFrameNb);
		Stack.setSlice(i);
		Stack.setChannel(channelIni);
		run("Measure");
		patchMean=getResult("Mean",nResults-1);
		patchArea=getResult("Area",nResults-1);
		totalPatchInt=getResult("Mean",nResults-1)*patchArea;
		
		if (offSetPatch==-1){
			run("Enlarge...", "enlarge="+dia2+" pixel");
		}
		else{
			roiManager("select",ROIIndex+offSetPatch);
			Stack.setChannel(channelIni);
			Stack.setFrame(RoiFrameNb);
			Stack.setSlice(i);
		}   

		run("Measure");
		BackgroundMean=getResult("Mean",nResults-1);
		BackgroundArea=getResult("Area",nResults-1);
		Stack.setChannel(channelIni);
		if (volodiaStyle==true){
			BackgroundPerUnitArea=BackgroundMean;
		}
		else{
			BackgroundPerUnitArea=(BackgroundArea*BackgroundMean-totalPatchInt)/(BackgroundArea-patchArea);
		}
		
		totInt=totalPatchInt-BackgroundPerUnitArea*patchArea;
		intensities[i-1]=totInt;
		bgIntensities[i-1]=BackgroundPerUnitArea;
		
		//Correction for photoBleaching
		corFactor=1/exp(-rateBleaching*((RoiFrameNb-1)*nbZSlices+i-1));
		intensities[i-1]=intensities[i-1]*corFactor;		
		sum=sum+intensities[i-1];
		if(intensities[i-1]>maxSlice){
			maxSlice=intensities[i-1];
			indexMaxSlice=i-1;//Changed here
		}
		if (offSetPatch==-1){
			run("Enlarge...", "enlarge=-"+dia2+" pixel");
		}
		else{
			roiManager("select",ROIIndex);
			Stack.setChannel(channelIni);	
		}
	}
	for(i=1; i<=nbZSlices; i++){
		percentages[i-1]=intensities[i-1]/sum;
	}
	
	// Get position of patch on max Slice
	Stack.setFrame(RoiFrameNb);
	Stack.setSlice(indexMaxSlice+1);
	Stack.setChannel(channelIni);

	run("Measure");
	positionXpx=getResult("XM",nResults-1);
	positionYpx=getResult("YM",nResults-1);
	Stack.setChannel(channelIni);
	if (indexMaxSlice==-1){
		indexMaxSlice=floor(nbZSlices/2)+1;
		print("  Warning: All the corrected intensities are negative at some point");
	}
	{
		gaussianCenter=gaussCenter(width+2,ROIIndex,positionXpx,positionYpx,maxGaussFittingRuns,bgIntensities[indexMaxSlice],((RoiFrameNb-1)*nbSlices+indexMaxSlice+1)); 
	}
	result=newArray(gaussianCenter.length+3);
	
	result[0]=indexMaxSlice;
	result[1]=positionXpx;
	result[2]=positionYpx;
	for (i=0;i<gaussianCenter.length;i++){
		result[i+3]=gaussianCenter[i];
	}
	selectImage(curImageName);
	if (Stack.isHyperstack){
		Stack.setChannel(channelIni);
	}
	
	return result;
	
}

function getPatchIntensity(indexMaxSlice,nbSlicesHS){
	// Sums the intensities of the nbSliceMeasured slices around the slice of index indexMaxSlice
	// Need to have getSlicesIntensitiesFromHS() run first to update the global variables intensities[] and percentages[]
	// RETURNS array with
	//	0. sum of the intensities of nbSliceMeasured slices around indexMaxSlice
	//	1. the corresponding percentage of total fluorescence
	//	2. the index of the first slice measured
	//	3. the index of the last slice measured
	
	// Get the index of the slices to sum
	min=-1;
	max=-1;
	if (nbSliceMeasured>nbSlicesHS){
		Dialog.create("Error!!");
		Dialog.addMessage("The number of slices measured should be smaller than the number of z-slices. Please change this parameter in the setup");
		Dialog.show();			
	}
	else{
		if (floor(nbSliceMeasured/2)*2==nbSliceMeasured){ //i.e. nbSliceMeasured is even
			//Check which neighbor slice has higher intensity
			if (indexMaxSlice==0) { min=0; max=nbSliceMeasured} // The 1st slice has the max intensity
			else if (indexMaxSlice==nbSlicesHS-1) {max=indexMaxSlice; min=indexMaxSlice-nbSliceMeasured} // the last slice has the max intensity
			else{
				if (intensities[indexMaxSlice-1] < intensities[indexMaxSlice+1]){
					max=indexMaxSlice+nbSliceMeasured/2;
					min=indexMaxSlice-nbSliceMeasured/2+1;
				}
				else{
					max=indexMaxSlice+nbSliceMeasured/2-1;
					min=indexMaxSlice-nbSliceMeasured/2;
				}
			}
		}
		else{ // nbSliceMeasured is odd
			max=indexMaxSlice+(nbSliceMeasured-1)/2;
			min=indexMaxSlice-(nbSliceMeasured-1)/2;
		}
		//Change min and max if they are out of boundaries [0..nbSlicesHS-1]
		if (max>nbSlicesHS-1){
			max=nbSlicesHS-1;
			min=max-nbSliceMeasured+1;
		}
		else if (min<0){
			min=0;
			max=nbSliceMeasured-1;
		}
		
		// Collect data
		sumIntensities=0;
		sumPercentages=0;
		for (i=min;i<=max;i++){
			sumIntensities=sumIntensities+intensities[i];
			sumPercentages=sumPercentages+percentages[i];
		}
		
		return newArray(sumIntensities,sumPercentages,min,max);
	}	
}




function add0(n){//adds a zero behind a number smaller than 9
	if (n==0){n="00";}	
	else if (n==1){n="01";}	
	else if (n==2){n="02";}	
	else if (n==3){n="03";}	
	else if (n==4){n="04";}	
	else if (n==5){n="05";}	
	else if (n==6){n="06";}	
	else if (n==7){n="07";}	
	else if (n==8){n="08";}	
	else if (n==9){n="09";}
	return n;	
}


function myImporter(curDir,file){// Faster than LOCI importer. Macros work faster too.
	txtFile=substring(file,0,lengthOf(file)-4)+".txt";
	open(curDir+file); //open(dir+curFilesFolders[i]+subFiles[j]);
	
	//Getting Number of slices, nb of frames and dimensions
	strFile=File.openAsString(curDir+txtFile);
	lines=split(strFile,"\n");
	nLines=lengthOf(lines);
	found=0;
	j=0;
	while (found==0&&j<nLines)// scale
	{
		if (startsWith(lines[j],"x : ")){
			found=1;
		}
		else{
			j++;
		}
	}
	xLine=split(lines[j]," ");
	scale=xLine[4];
	j++;
	found=0;
	while (found==0&&j<nLines) //slices
	{
		if (startsWith(lines[j],"Z : ")){
			found=1;
		}
		else{
			j++;
		}
	}
	if (found==1){
		zLine=split(lines[j]," ");
		nbSlices=zLine[2];
		j++;
		found=0;
		while (found==0&&j<nLines)//frames
		{
			if (startsWith(lines[j],"Time : ")){
				found=1;
			}
			else{
				j++;
			}
		}
		if (found==1){
			frameLine=split(lines[j]," ");
			nbFrames=frameLine[2];
		}
		else{
			nbFrames=1;
		}
		run("Stack to Hyperstack...", "order=xyczt(default) channels=1 slices="+nbSlices+" frames="+nbFrames+" display=Color");
		run("Set Scale...", "distance=1 known="+scale+" pixel=1 unit=micron global");
		DIC=0;
	}
	else{
		DIC=1;
	}                                           
	return DIC;
}

function isBadROI(index){
	roiManager("select",index);
	tmpROIName=getInfo("roi.name");
	if (endsWith(tmpROIName,"#x")){
		dbprint("badROI: "+tmpROIName);
		return true;
	}
	else {return false;}
}

function myGarbageCollector(){
	//run("MemoryCleaner ");  // My old garbage collector
	run("Collect Garbage"); // New Fiji access to Java's garbage collector	
}

function dbprint(txt){
	if (dbg==true){
		print(txt);
	}
}

// Function for data alignment
function scoreArrays(array1,array2,begin,end){
	score=0;
	goodScore=false;
	nbEval=0;
	for (i=begin;i<=end;i++){
		if(array1[i]!=0&&array2[i]!=0){
			score=score+pow((array1[i]-array2[i]),2);
			goodScore=true;
			nbEval++;
		}
	}
	if(goodScore==false){
	return -1;}
	else{
		score=score/nbEval;
		return score;
	}
}

function getMaxShifts(array){
	// Gets the maximum left and rigth shifts for the data
	i=0;
	flag=true;
	while(i<array.length&&flag){// check the 1st non zero value
		flag=(array[i]==0);
		i++;
	}
	min=i-1;
	
	if (min==array.length-1){//if array only contains zeros
		min=0;
		max=0;
	}
	else{
		i=array.length-1;
		while(i>0&&array[i]==0){// check the last non zero value
			i=i-1;
		}
		max=array.length-1-i;
	}
	return newArray(-min,max);
}

function shiftArray(array,shift){
	// Returns an array whose data are shifted 
	shiftedArray=newArray(array.length);
	for(i=0;i<array.length;i++)
	{
		if ((i-shift<array.length)&&(i-shift>=0)){
			shiftedArray[i]=array[i-shift];
		}
		else{
			shiftedArray[i]=0;
		}
	}
	return shiftedArray;
}

function getBestAlignment(array1,refArray,begin,end,curPatchDuration){
	// Calculates the best alignment for the 2 arrays
	minScore=pow(10,10);
	shiftsArray1=getMaxShifts(array1);
	// WARNING: Avoid using shiftsArray1[1] directly: might cause problems for 2 color movies (since it truncates data if patch is on top or bottom slice)
	// same kind of problem is possible with shiftsArray1[0] too
	minShift=shiftsArray1[0];
	maxShift=array1.length-curPatchDuration;
	
	for (i=minShift;i<=maxShift;i++){
		shiftedArray1=shiftArray(array1,i);
		score=scoreArrays(shiftedArray1,refArray,begin,end);
		if(score<minScore&&score!=-1){
			minScore=score;
			bestShift=i;
			bestShiftedArray1=shiftedArray1;
		}
	}
	if (minScore==pow(10,10)){
		return newArray(0,-1);
	}
	else{
		return newArray(bestShift,minScore);
	}
}

function subsetEnlargeAndShiftArray(array,offSet,duration,newSize,shift){//Get subset of array from offset to offset+duration, enlarge it to newSize and shift it
	tmpPatch=newArray(newSize);
	for (j=0;j<duration;j++){
		tmpPatch[j]=array[offSet+j];
	}
	for (j=duration;j<newSize-duration;j++){
		tmpPatch[j]=0;
	}
	shiftedArray=shiftArray(tmpPatch,shift);	
	return shiftedArray;	
}


function truncateArray(array,first,last){
	resArray=newArray(last-first);
	for(i=0;i<resArray.length;i++){
		resArray[i]=array[first+i];
	}
	
	return resArray;
}

function copyArrayIntoBiggerArray(smallArray,bigArray,offset){
	for (j=0;j<smallArray.length;j++)
	{
		bigArray[offset+j]=smallArray[j];
	}
}

function printArray(array){
	for (j=0;j<array.length;j++)
	{
		print(j+": "+array[j]);
	}
}

function addToArray(originalArray, newValue){
	lengthOriginal=originalArray.length;
	outputArray=newArray(lengthOriginal+1);
	for (i=0;i<lengthOriginal;i++){
		outputArray[i]=originalArray[i];
	}
	outputArray[lengthOriginal]=newValue;
	return outputArray;
}

function addToArrayIfNewValue(originalArray, newValue){
	lengthOriginal=originalArray.length;
	outputArray=newArray(lengthOriginal+1);
	found=0;
	for (i=0;i<lengthOriginal;i++){
		if (originalArray[i]==newValue){
			found=1;
		}
	}
	if (found==1){
		return originalArray;
	}
	else{
		originalArray=addToArray(originalArray, newValue);
		return originalArray;
	}
	
}

function getProtocolFilesList(fileList){
	protocolFileArray=newArray(0);
	for (i=0;i<fileList.length;i++){
		if (endsWith(fileList[i],".txt")){
			tifFileName=substring(fileList[i],0,lengthOf(fileList[i])-3)+"tif";
			if (isValueInArray(fileList,tifFileName)==false){
				protocolFileArray=addToArray(protocolFileArray,fileList[i]);
			}
		}
	}
	return protocolFileArray;
}



function isValueInArray(array,value){
	for (i=0;i<array.length;i++){
		if(array[i]==value){
			return true;
		}
	}
	return false;
}



/********************************************************* 
********************* More Functions... *******************
***********************************************************/


function getPropertyFromTxtFile(filePath, propertyNamePattern, afterMarkup, beforeMarkup){
	// Screen the file for the lines starting with "propertyNamePattern" 
	// between the lines containing	"afterPatternInFile" and "beforePatternInFile"
	
	strFile=File.openAsString(filePath);
	lines=split(strFile,"\n");
	nLines=lengthOf(lines);
	foundAfterMarkup=0;
	foundBeforeMarkup=0;
	foundPropertyNamePattern=0;
	j=0;
	resultArray=newArray(0);
	while (foundAfterMarkup==0&&j<nLines&&foundBeforeMarkup==0)//find pattern after which to search 
	{
		if (startsWith(lines[j],afterMarkup)){
			foundAfterMarkup=1;
		}
		if (startsWith(lines[j],beforeMarkup)){
			foundBeforeMarkup=1;
		}
		j++;
	}
	if (foundBeforeMarkup==1){
		print ("Begin markup not found");
		return newArray(0);
	}
	else if (foundAfterMarkup==1){
		while (j<nLines&&foundBeforeMarkup==0){
			if (matches(lines[j],"^\t*("+propertyNamePattern+").*")){// Line starts with propertyNamePattern with maybe tabulations before
				ind=indexOf(lines[j],propertyNamePattern);
				propertyValue=substring(lines[j],ind+lengthOf(propertyNamePattern),lengthOf(lines[j]));
				resultArray=addToArray(resultArray,propertyValue);
			}
			else if (startsWith(lines[j],beforeMarkup)){
				foundBeforeMarkup=1;
			}
			j++;
		}
		return resultArray;
	}
	else {
		print ("No markup found");
		return newArray(0);
	}
}

function setupChannel(imagePath){
	getDimensions(width, height, channels, slices, frames);
	filePath=substring(imagePath,0,lengthOf(imagePath)-3)+"txt";
	channelNames=getPropertyFromTxtFile(filePath, "Channel - ","[Protocol Description]","[Protocol Description End]");
	if (channelNames.length==0){
		channelNames=getPropertyFromTxtFile(filePath, "Move Channel - ","[Protocol Description]","[Protocol Description End]");
	}
	if (channelNames.length==0){
		channelNames=newArray(channels);
		for (i=0;i<channels;i++){
			channelNames[i]="Channel "+(i+1);
		}
	}
	textChannelNames="Which Channel do you want to use? \n";
	for (i=0;i<channels;i++){
		textChannelNames=textChannelNames+(i+1)+": "+channelNames[i]+"\n";
	}
	channelNumber=0;
	Dialog.create("Setup Channel to use");
	Dialog.addMessage(textChannelNames);
	Dialog.addNumber("Channel to use ", channelNumber);
	Dialog.show();
	channelNumber=Dialog.getNumber();
	return channelNumber;
}


function findProtocols(dir0,fileName){
	path=dir0+fileName;
	strFile=File.openAsString(path);
	lines=split(strFile,"\n");
	name=substring(fileName,0,lengthOf(fileName)-4);
	nLines=lengthOf(lines);
	j=1;
	protocolList=newArray(0);
	while (j<nLines)
	{
		propertyNamePattern="Move Channel - ";
		propertyNamePattern2="Channel - ";
		if (matches(lines[j],"^\t*("+propertyNamePattern+").*")||matches(lines[j],"^\t*("+propertyNamePattern2+").*")){
			ind=indexOf(lines[j],propertyNamePattern);
			protocolName=substring(lines[j],ind+lengthOf(propertyNamePattern),lengthOf(lines[j]));
			protocolList=addToArray(protocolList, protocolName);
		}
		j++;
	}
	return protocolList;
}

function addSlicesToSingleChannelHyperstack(nbSlicesToAddBefore,nbSlicesToAddAfter){
	Stack.getDimensions(imWidth, imHeight,curNbChannels, curNbSlices, curNbFrames);
	run("Hyperstack to Stack");
	f=curNbFrames-1;
	while(f>=0){
		setSlice(curNbSlices*(f+1));
		for(i=0;i<nbSlicesToAddAfter;i++){
			run("Add Slice");
		}
		if(f==0){
			run("Reverse");
			setSlice(nSlices);
			for(i=0;i<nbSlicesToAddBefore;i++){
				run("Add Slice");
			}
			run("Reverse");
		}
		else{
			setSlice(curNbSlices*f);
			for(i=0;i<nbSlicesToAddBefore;i++){
				run("Add Slice");
			}
		}
		f=f-1;
	}
	
	newSliceNb=curNbSlices+nbSlicesToAddBefore+nbSlicesToAddAfter;
	run("Stack to Hyperstack...", "order=xyczt(default) channels="+curNbChannels+" slices="+newSliceNb+" frames="+curNbFrames+" display=Grayscale");
}

function saveMacroFiles(archive){ 
	// Save all the files necessary for the Patch tracking macro toolset into one folder
	// If archive==1, it creates a folder with date and time and saves the files in it
	//     otherwise it saves the files at the root 
	//dirSource=getDirectory("Select ImageJ directory:");
	dirSource=getDirectory("imagej");
	dirDestination=getDirectory("Select backup directory:");
	print(dirSource);
	print(dirDestination);
	if (archive==1){
		getDateAndTime(year, month, dayOfWeek, dayOfMonth, hour, minute, second, msec);
		dirDestination=dirDestination+fileSep+"Archives";
		File.makeDirectory(dirDestination);
		dirDestination=dirDestination+fileSep+toString(year);
		dirDestination=dirDestination+toString(add0(parseInt(month)+1))+formatNicelyNumber(dayOfMonth)+".";
		dirDestination=dirDestination+formatNicelyNumber(hour)+formatNicelyNumber(minute)+formatNicelyNumber(second)+fileSep;
		File.makeDirectory(dirDestination);
	}
	
	sourceFile=dirSource+"macros"+fileSep+"copyFilesForPatchTrackingMacros.txt";
	destFile=dirDestination+"copyFilesForPatchTrackingMacros.txt";
	print(sourceFile);
	print(destFile);
	
	copy(sourceFile,destFile,1);
	sourceFile=dirSource+"macros"+fileSep+"toolsets"+fileSep+"SemiManualTracking.iQ2Data.dev.txt";
	destFile=dirDestination+"SemiManualTracking.iQ2Data.dev.txt";
	copy(sourceFile,destFile,1);
	sourceFile=dirSource+"macros"+fileSep+"toolsets"+fileSep+"Doc Patch Tracking.docx";
	destFile=dirDestination+"Doc Patch Tracking.docx";
	copy(sourceFile,destFile,1);
	sourceFile=dirSource+"macros"+fileSep+"PatchTrackingBar.txt";
	destFile=dirDestination+"PatchTrackingBar.txt";
	copy(sourceFile,destFile,1);
	sourceFile=dirSource+"plugins"+fileSep+"ReorganizeFilesiQ2New.class";
	destFile=dirDestination+"ReorganizeFilesiQ2New.class";
	copy(sourceFile,destFile,1);
	sourceFile=dirSource+"plugins"+fileSep+"ReorganizeFilesiQ2New$1.class";
	destFile=dirDestination+"ReorganizeFilesiQ2New$1.class";
	copy(sourceFile,destFile,1);
	sourceFile=dirSource+"plugins"+fileSep+"AutoRecenter_.class";
	destFile=dirDestination+"AutoRecenter_.class";
	copy(sourceFile,destFile,1);
	sourceFile=dirSource+"plugins"+fileSep+"AutoRecenter_Z.class";
	destFile=dirDestination+"AutoRecenter_Z.class";
	copy(sourceFile,destFile,1);
	File.makeDirectory(dirDestination+"PatchTrackingBar"+fileSep);
	curFiles=getFileList(dirSource+"plugins"+fileSep+"ActionBar"+fileSep+"icons"+fileSep+"PatchTrackingBar");
	nFiles=curFiles.length;
	print(nFiles);
	for(i=0;i<nFiles;i++){
		pathSourceFile=dirSource+"plugins"+fileSep+"ActionBar"+fileSep+"icons"+fileSep+"PatchTrackingBar"+fileSep+curFiles[i];
		pathDestinationFile=dirDestination+"PatchTrackingBar"+fileSep+curFiles[i];
		copy(pathSourceFile,pathDestinationFile,1);
	}
}

function installMacroFiles(overWrite){ 
	// Installs all the files necessary for the Patch tracking macro toolset
	// If overWrite==0, it will create a copy of the old file with and .date.time for new extension
	//     otherwise it just overwrites the files

	dirSource=getDirectory("Select the folder with the files to install:");
	dirImageJ=getDirectory("imagej");
	
	flagCHKPaths=1;
	flagCHKPaths=flagCHKPaths*checkIfPathExists(dirImageJ,"macros");
	tmpPath="macros"+fileSep+"toolsets";
	flagCHKPaths=flagCHKPaths*checkIfPathExists(dirImageJ,tmpPath);
	tmpPath="plugins";
	flagCHKPaths=flagCHKPaths*checkIfPathExists(dirImageJ,tmpPath);
	tmpPath="plugins"+fileSep+"ActionBar";
	flagCHKPaths=flagCHKPaths*checkIfPathExists(dirImageJ,tmpPath);
	tmpPath="plugins"+fileSep+"ActionBar"+fileSep+"icons";
	flagCHKPaths=flagCHKPaths*checkIfPathExists(dirImageJ,tmpPath);
	
	if (flagCHKPaths){
		
	/*	if (archive==1){
			getDateAndTime(year, month, dayOfWeek, dayOfMonth, hour, minute, second, msec);
			dirDestination=dirDestination+fileSep+"Archives";
			File.makeDirectory(dirDestination);
			dirDestination=dirDestination+fileSep+toString(year);
			dirDestination=dirDestination+toString(add0(parseInt(month)+1))+formatNicelyNumber(dayOfMonth)+".";
			dirDestination=dirDestination+formatNicelyNumber(hour)+formatNicelyNumber(minute)+formatNicelyNumber(second)+fileSep;
			File.makeDirectory(dirDestination);
			//print(dirDestination);
		}
		//dirSource=dirSource+fileSep;
		//dirDestination=dirDestination+fileSep;
	*/	
		destFile=dirImageJ+"macros"+fileSep+"copyFilesForPatchTrackingMacros.txt";
		sourceFile=dirSource+"copyFilesForPatchTrackingMacros.txt";
		copy(sourceFile,destFile,overWrite);
		destFile=dirImageJ+"macros"+fileSep+"toolsets"+fileSep+"SemiManualTracking.iQ2Data.dev.txt";
		sourceFile=dirSource+"SemiManualTracking.iQ2Data.dev.txt";
		copy(sourceFile,destFile,overWrite);
		destFile=dirImageJ+"macros"+fileSep+"toolsets"+fileSep+"Doc Patch Tracking.docx";
		sourceFile=dirSource+"Doc Patch Tracking.docx";
		copy(sourceFile,destFile,overWrite);
		destFile=dirImageJ+"macros"+fileSep+"PatchTrackingBar.txt";
		sourceFile=dirSource+"PatchTrackingBar.txt";
		copy(sourceFile,destFile,overWrite);
		destFile=dirImageJ+"plugins"+fileSep+"ReorganizeFilesiQ2New.class";
		sourceFile=dirSource+"ReorganizeFilesiQ2New.class";
		copy(sourceFile,destFile,overWrite);
		destFile=dirImageJ+"plugins"+fileSep+"ReorganizeFilesiQ2New$1.class";
		sourceFile=dirSource+"ReorganizeFilesiQ2New$1.class";
		copy(sourceFile,destFile,overWrite);
		destFile=dirImageJ+"plugins"+fileSep+"AutoRecenter_.class";
		sourceFile=dirSource+"AutoRecenter_.class";
		copy(sourceFile,destFile,overWrite);
		destFile=dirImageJ+"plugins"+fileSep+"AutoRecenter_Z.class";
		sourceFile=dirSource+"AutoRecenter_Z.class";
		copy(sourceFile,destFile,overWrite);
		destDir=dirImageJ+"plugins"+fileSep+"ActionBar"+fileSep+"icons"+fileSep+"PatchTrackingBar"+fileSep;
		File.makeDirectory(destDir);
		curFiles=getFileList(dirSource+"PatchTrackingBar");
		nFiles=curFiles.length;
		print(nFiles);
		for(i=0;i<nFiles;i++){
			destFile=dirImageJ+"plugins"+fileSep+"ActionBar"+fileSep+"icons"+fileSep+"PatchTrackingBar"+fileSep+curFiles[i];
			sourceFile=dirSource+"PatchTrackingBar"+fileSep+curFiles[i];
			copy(sourceFile,destFile,1);
		}
	}
}
   
function rawCopy(oldPath, newPath){//Copy/overwrite file from oldPath into newPath
	OS=getOS();
	print("Copying "+oldPath);
	print("  to "+newPath);
		
	if (OS=="MAC_Linux"){
		exec("cp", oldPath,newPath);
	}
	else if (OS=="Windows"){
		exec("cmd","/C","copy /y",oldPath,newPath);
	}
	else{
		txt="OS not recognized. \n Try to copy files manually: \n";
		txt=txt+"  Source: "+oldPath+"\n";
		txt=txt+"  Destination: "+newPath;
		showMessage(txt);
	}
}

function copy(oldPath, newPath, OW){//Copy file from oldPath to newPath. Overwrite the newPath if OW==1, saves the old version with .date.time extension if already exists and OW==0
	if (File.exists(newPath)&&OW==0){
		getDateAndTime(year, month, dayOfWeek, dayOfMonth, hour, minute, second, msec);
		dateTime=toString(year);
		dateTime=dateTime+toString(add0(parseInt(month)+1));
		dateTime=dateTime+formatNicelyNumber(dayOfMonth);
		dateTime=dateTime+"."+formatNicelyNumber(hour);
		dateTime=dateTime+formatNicelyNumber(minute);
		dateTime=dateTime+formatNicelyNumber(second);
		oldNewPath=newPath+"."+dateTime;
		rawCopy(newPath,oldNewPath);
		print("File "+newPath+" already existed:");
		print("    It has been renamed "+newPath+"."+dateTime);
	}
	rawCopy(oldPath, newPath);

}


function getOS(){
	sep=File.separator();
	
	if (sep=="\\"){
		return "Windows";
	}
	else if (sep=="\/"){
		return "MAC_Linux";
	}
	else{
		showMessage("System Not recognized...");
		return "";
	}
}

function formatNicelyNumber(n){
	return toString(add0(parseInt(n)));
}

function checkIfPathExists(path,subPath){
	fullPath=path+subPath;
	if (!File.exists(fullPath)){ 
		txt=fullPath+" does not exist. \n";
		txt=txt+"Make sure "+path+" is your ImageJ folder \n";
		txt=txt+"and the ActionBar plugin is installed properly...";
		showMessage(txt);
		return 0;
	}
	else{
		return 1;
	}
}

//CAN BE DELETED
function modifyImageRendering(modifyRenderDesliceFlag){
	if (modifyRenderDesliceFlag){
		run("Gaussian Blur...", "sigma=1 stack");
	}
}

function makeMontage(imageID,beginFrame,endFrame,scaleMontage){//From MakeMontageAroundROI.txt macro
	// Makes a montage 
	// The stack should be open and an ROI should be drawn 
	selectImage(imageID);
	imageTitle=getTitle();

	Stack.getDimensions(oriimWidth, oriimHeight, orinbChannels, orinbSlices, orinbFrames);
	
	run("Duplicate...", "title=["+imageTitle+"] duplicate channels=1-"+orinbChannels+" frames="+beginFrame+"-"+endFrame);
	CroppedStackID=getImageID();
	columns=endFrame-beginFrame+1;
	Stack.getDimensions(tmpimWidth, tmpimHeight, tmpnbChannels, tmpnbSlices, tmpnbFrames);
	Stack.getPosition(tmpChannelNb, tmpSliceNb, tmpFrameNb);
	
	Stack.setPosition(tmpChannelNb, tmpSliceNb, tmpFrameNb);
	idMontages=newArray(tmpnbSlices);

	selectImage(CroppedStackID);

	for (sl=0;sl<tmpnbSlices;sl++){//Makes 1 line montage per slice
		selectImage(CroppedStackID);
		Stack.setPosition(tmpChannelNb, sl+1, tmpFrameNb);
		run("Reduce Dimensionality...", " channels frames keep");
		tmpReducedStackID=getImageID();
		run("Grays");
		run("Invert LUT");
		
		run("Make Montage...", "columns="+columns+" rows=1 scale="+scaleMontage+" first=1 last="+tmpnbFrames+" increment=1 border=0 font=12");

		curTitle=getTitle();
		curID=getImageID();
		idMontages[sl]=curID;
		rename(curTitle+curID);
		selectImage(tmpReducedStackID);
		close();
	}
	
	combinedMontageID=idMontages[0];
	for (i=1;i<idMontages.length;i++){// Combine all montagess
		selectImage(combinedMontageID);
		CombinedMontageTitle=getTitle();
		selectImage(idMontages[i]);
		curMontageTitle=getTitle();
		//print(combinedMontageID+" with "+idMontages[i]);
		//print("stack1="+CombinedMontageTitle+" stack2="+curMontageTitle);
		run("Combine...", "stack1=["+CombinedMontageTitle+"] stack2=["+curMontageTitle+"] combine");
		combinedMontageID=getImageID();
	}
	rename("Montage");
	selectImage(CroppedStackID);
	close();
	
	
	selectImage(combinedMontageID);
	
	// New contrast enhancer	
	if (tmpnbChannels>1){
		run("Make Composite", "display=Composite");
		Stack.setDisplayMode("composite");
		combinedMontageID=getImageID();
		for (ch=1;ch<=tmpnbChannels;ch++){
			selectImage(imageID);
			Stack.setChannel(ch);
			getLut(reds, greens, blues);
			getMinAndMax(minIntensity, maxIntensity);
			selectImage(combinedMontageID);
			Stack.setChannel(ch);
			setLut(reds, greens, blues);
			getMinAndMax(minIntensity, maxIntensity);
		}
	}
	else{
		selectImage(imageID);
		getLut(reds, greens, blues);
		getMinAndMax(minIntensity, maxIntensity);
		selectImage(combinedMontageID);
		setLut(reds, greens, blues);	
		setMinAndMax(minIntensity, maxIntensity);
	}
	
	
	return combinedMontageID;
}




function makeMontageAllSlices(imageID,beginFrame,endFrame,scaleMontage){//From MakeMontageAroundROI.txt macro
	//Makes a montage with as many rows as slices in the hyperstack
	// The hyperstack should be open and an ROI should be drawn 
		
	selectImage(imageID);
	imageTitle=getTitle();

	Stack.getDimensions(oriimWidth, oriimHeight, orinbChannels, orinbSlices, orinbFrames);
	
	run("Duplicate...", "title=["+imageTitle+"] duplicate channels=1-"+orinbChannels+" frames="+beginFrame+"-"+endFrame);
	CroppedStackID=getImageID();
	columns=endFrame-beginFrame+1;
	Stack.getDimensions(tmpimWidth, tmpimHeight, tmpnbChannels, tmpnbSlices, tmpnbFrames);
	Stack.getPosition(tmpChannelNb, tmpSliceNb, tmpFrameNb);
	
	Stack.setPosition(tmpChannelNb, tmpSliceNb, tmpFrameNb);
	idMontages=newArray(tmpnbSlices);

	selectImage(CroppedStackID);

	for (sl=0;sl<tmpnbSlices;sl++){//Makes 1 line montage per slice
		selectImage(CroppedStackID);
		Stack.setPosition(tmpChannelNb, sl+1, tmpFrameNb);
		run("Reduce Dimensionality...", " channels frames keep");
		tmpReducedStackID=getImageID();
		run("Grays");
		run("Invert LUT");
		
		run("Make Montage...", "columns="+columns+" rows=1 scale="+scaleMontage+" first=1 last="+tmpnbFrames+" increment=1 border=0 font=12");

		curTitle=getTitle();
		curID=getImageID();
		idMontages[sl]=curID;
		rename(curTitle+curID);
		selectImage(tmpReducedStackID);
		close();
	}
	
	combinedMontageID=idMontages[0];
	for (i=1;i<idMontages.length;i++){// Combine all montagess
		selectImage(combinedMontageID);
		CombinedMontageTitle=getTitle();
		selectImage(idMontages[i]);
		curMontageTitle=getTitle();
		run("Combine...", "stack1=["+CombinedMontageTitle+"] stack2=["+curMontageTitle+"] combine");
		combinedMontageID=getImageID();
	}
	rename("Montage");
	
	selectImage(CroppedStackID);
	close();
	
	
	selectImage(combinedMontageID);
	
	// New contrast enhancer	
	if (tmpnbChannels>1){
		run("Make Composite", "display=Composite");
		Stack.setDisplayMode("composite");
		combinedMontageID=getImageID();
		for (ch=1;ch<=tmpnbChannels;ch++){
			selectImage(imageID);
			Stack.setChannel(ch);
			getLut(reds, greens, blues);
			getMinAndMax(minIntensity, maxIntensity);
			selectImage(combinedMontageID);
			Stack.setChannel(ch);
			setLut(reds, greens, blues);
			getMinAndMax(minIntensity, maxIntensity);
		}
	}
	else{
		selectImage(imageID);
		getLut(reds, greens, blues);
		getMinAndMax(minIntensity, maxIntensity);
		selectImage(combinedMontageID);
		setLut(reds, greens, blues);	
		setMinAndMax(minIntensity, maxIntensity);
	}
	
	
	return combinedMontageID;
}


function makeMontageDeslice(HSImageID,heightSlices,widthSlices,channelsNb,slicesNb){
	//selectImage(HSImageID);
	cpt=0;
	for (ch=0;ch<channelsNb;ch++){
		for (i=0;i<slicesNb;i++){
			selectImage(HSImageID);
			Stack.setPosition(ch+1, i+1, 1);
			run("Reduce Dimensionality...", "  frames keep");
			//Draw border 
			getDimensions(widthBorder, heightBorder, channelsBorder, slicesBorder, framesBorder);
			setLineWidth(1);
			setColor(0);
			for (j=0;j<framesNb;j++){
				Stack.setFrame(j);
				drawRect(0, 0, widthBorder, heightBorder);
			}
			//Combine z-slices
			curImageName=getTitle();
			if (i==0){
				rename("temp"+cpt);
				curImageName=getTitle();
				previousImageName=curImageName;
				previousImageID=getImageID();
			}
			else{
				selectImage(previousImageID);
				run("Combine...", "stack1=["+previousImageName+"] stack2=["+curImageName+"]");
				rename("temp"+cpt);			
				previousImageName=getTitle();
				previousImageID=getImageID();
			}
			Stack.setPosition(1, 1, curFrameNb);
			cpt=cpt+1;
		}
		selectImage(previousImageID);
		rename("ch"+ch);
		
		//Combine channels (from combined z-slices)
		curComboName=getTitle();
		if (ch==0){
			previousComboName=curComboName;
		}
		else{
			run("Combine...", "stack1=["+previousComboName+"] stack2=["+curComboName+"]  combine");
			previousComboName=getTitle();
		}
	}
	
	
	selectImage(previousComboName);
	
	rename("Desliced");
	idComboName=getImageID();
	selectImage(idComboName);
	
	return idComboName;
}


function averageOfArray(array){
	sum=0;
	for (i=0;i<array.length;i++){
		sum=sum+array[i];
	}
	return (sum/array.length);
}

function stdevOfArray(array){
	average=averageOfArray(array);
	sum=0;
	for (i=0;i<array.length;i++){
		sum=sum+pow((array[i]-average),2);
	}
	return sqrt(sum/array.length);
}


function minOfArray(array){
	if(array.length>0){
		min=array[0];
		for (i=0;i<lengthOf(array);i++){
			if (array[i]<min){
				min=array[i];
			}
		}
	return min;}
	else{
		return 1/0;
	}
}

function maxOfArray(array){
	if(array.length>0){
		max=array[0];
		for (i=0;i<lengthOf(array);i++){
			if (array[i]>max){
				max=array[i];
			}
		}
	return max;}
	else{
		return 1/0;
	}
}

function checkMainImageName(){
	if(mainImageName==""||mainImageId==-1||!isOpen(mainImageName)||!isOpen(mainImageId))
	{
		setMainImageName();
	}
}

function setMainImageName(){
	imageName=getTitle();
	found=true;
	if(!endsWith(imageName,'_SUM_Corrected.tif')){ // search for an appropriate image
		found=false;
		i=1;
		while(i<=nImages&&!found){
			selectImage(i);
			if (endsWith(getTitle(),'_SUM_Corrected.tif')){
				found=true;
				imageName=getTitle();
			}
			i++;
		}
	}
	if(found){
		mainImageName=imageName;
		mainImageId=getImageID();
		photoBleachingCorrection=0;
		photoBleachingSet=false;
	}
	else{
		waitForUser("No sum projection image found... \n Close all images, reload the toolset and try again!");
	}
}





/********************************************************* 
******* DO NOT USE: Tests and development ****************
***********************************************************/
macro '** DO NOT USE: Tests and development **'{} 


function measureCenterPixel(){
	getSelectionBounds(x, y, w, h);
	xCenter=floor(x+w/2);
	yCenter=floor(y+h/2);
	centerInt=getPixel(xCenter,yCenter);
	return centerInt;
}



function findMaxIntensityPatch(ROIpath,centerValue){
	roiManager("Reset");
	open(ROIpath);
	countROI=roiManager("count");
	intensityValues=newArray(countROI/2);
	if (centerValue){
		for (i=0;i<countROI/2;i++){
			roiManager("Select",i);
			intensityValues[i]=measureCenterPixel();
		}
		max=maxOfArray(intensityValues);
	}
	else{
		for (i=0;i<countROI/2;i++){
			roiManager("Select",i);
			roiManager("Measure");
			intensityValues[i]=getResult("Mean",nResults-1)*getResult("Area",nResults-1);
		}
		max=maxOfArray(intensityValues)/getResult("Area",nResults-1);
	}
	roiManager("Deselect");
	return max;
}

function  findThresholdValues(ROIpath,pcThreshold,centerValue,mode){
	ROIFiles=getFileList(ROIpath);
	print(ROIpath);
	maxs=newArray(0);
	for(i=0;i<ROIFiles.length;i++){
		if(endsWith(ROIFiles[i],".zip")&&startsWith(ROIFiles[i],ROIFileNamepattern)){
			curRoiFile=ROIpath+fileSep+ROIFiles[i];
			tmpRes=findMaxIntensityPatch(curRoiFile,centerValue);
			maxs=addToArray(maxs,tmpRes);
		}
	}
	for (i=0;i<maxs.length;i++){
		print("maxs["+i+"]: "+maxs[i]);
	}
	
	print("size max: "+maxs.length);
	averageMaxs=averageOfArray(maxs);
	stdevMaxs=stdevOfArray(maxs);
	max=maxOfArray(maxs);
	min=minOfArray(maxs);
	print("averageMaxs="+averageMaxs);
	print("stdevMaxs="+stdevMaxs);
	print("max="+max);
	print("min="+min);
	
	if (mode=="MaxMin"){
		// Max/min +/- percentage
		upper=max*(1+pcThreshold);
		lower=min*(1-pcThreshold);
	}
	else if (mode=="AveragePercentage"){
		//Average +/- percentage
		upper=averageMaxs*(1+pcThreshold);
		lower=averageMaxs*(1-pcThreshold);
	}
	else if (mode=="AverageSD"){
		//Average +/- 2 SD
		nSD=1;
		upper=(averageMaxs+1*stdevMaxs);
		lower=(averageMaxs-1*stdevMaxs);
	}
	return newArray(lower,upper);
}


function checkIfSpotLooksLikeAPatch(diam, curROI, newX, newY,maxNbOfRun,guessBG,frameNb){
	minRFit=0.85;
	maxStDev=diameter/4;
	//function gaussCenter(diam, curROI, newX, newY,maxNbOfRun,guessBG,frameNb){
	//gaussCenterCoord=newArray(newX,newY,0,xBg,xStdev,xRsquared,yBg,yStdev,yRsquared);
	
	gaussCenterCoord=gaussCenter(diam, curROI, newX, newY,maxNbOfRun,guessBG,frameNb);
	//[0]=xCenter,[1]=yCenter,[2]=maxNbOfRun,[3]=xBg,[4]=xStdev,[5]=xRsquared,[6]=yBg,[7]=yStdev,[8]=yRsquared);
	xCenter=gaussCenterCoord[0];
	yCenter=gaussCenterCoord[1];
	maxNbOfRun=gaussCenterCoord[2];
	xBg=gaussCenterCoord[3];
	xStdev=gaussCenterCoord[4];
	xRsquared=gaussCenterCoord[5];
	yBg=gaussCenterCoord[6];
	yStdev=gaussCenterCoord[7];
	yRsquared=gaussCenterCoord[8];
	print("xCenter="+xCenter);
	print("yCenter="+yCenter);
	print("maxNbOfRun="+maxNbOfRun);
	print("xBg="+xBg);
	print("xStdev="+xStdev);
	print("xRsquared="+xRsquared);
	print("yBg="+yBg);
	print("yStdev="+yStdev);
	print("yRsquared="+yRsquared);
	if(xRsquared<minRFit||yRsquared<minRFit||xBg<0||yBg<0||xStdev>maxStDev||yStdev>maxStDev){
		return false;
	}
	else{
		return true;
	}
}

macro 'enlarge All[9]'{
	getVoxelSize(imagepw, imageph, imagepd,imageunit);
	run("Set Scale...", "distance=1 known=1 pixel=1 unit=pixel");
	setBatchMode(true);
	count=roiManager("count");
	for (i=0;i<count;i++){
		circleExpandROI(i,diameter);
		roiManager("Update");
	}
	run("Set Scale...", "distance="+(1/imagepw)+" known=1 pixel=1 unit=microns global");
}

function circleExpandROI(nROI,newSize){
	roiManager("select",nROI);
	roiManager("Measure");
	x=getResult("X",nResults-1);
	y=getResult("Y",nResults-1);
	slice=getResult("Slice",nResults-1);
	run("Specify...", "width="+newSize+" height="+newSize+" x="+x+" y="+y+" slice="+slice+" oval centered");
}

macro 'Backup files for Patch Tracking Macros'{  
	saveMacroFiles(0);//1 for archiving, 0 to overwrite
}

macro 'install Patch Tracking Macros'{
	installMacroFiles(0);//0 for backig up old version, 1 to overwrite
}

macro 'Save display setup #1 [E]'{
	getLut(redsSetup1, greensSetup1, bluesSetup1);
	getMinAndMax(minIntensitySetup1, maxIntensitySetup1);
}

macro 'Set display setup #1 [e]'{
	if (redsSetup1!=0){
		setLut(redsSetup1, greensSetup1, bluesSetup1);
		setMinAndMax(minIntensitySetup1, maxIntensitySetup1);
	}
	else{
		print("No display saved yet!");
	}	
}






