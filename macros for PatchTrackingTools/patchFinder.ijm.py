######################
# PatchFinder.ijm.py
# v151216
# December, 16 2015
# Julien Berro
# Yale University
#-------------------------
# This FIJI/ImageJ script uses Trackmate libraries to automatically finds tracks of spots fit for counting number of molecules using quantitative microscopy methods.
# It is used within the PatchTrackingTools toolset developped by the Berro Lab (http://campuspress.yale.edu/berrolab/publications/software/)
# Tested on Windows 7, ImageJ 1.50e, Fiji, Trackmate v2.8.1
#-------------------------

# Todo list:
# - Improve navigation (previous button, completion bar)
# - Live display of log window
# - Remove patches with odd shapes (usually 2 overlapping patches)
##################

 


from fiji.plugin.trackmate import Model 
from fiji.plugin.trackmate import Settings 
from fiji.plugin.trackmate import TrackMate 
from fiji.plugin.trackmate import SelectionModel 
from fiji.plugin.trackmate import Logger 
from fiji.plugin.trackmate.detection import LogDetectorFactory 
from fiji.plugin.trackmate.detection import BlockLogDetectorFactory 
from fiji.plugin.trackmate.tracking.sparselap import SparseLAPTrackerFactory 
from fiji.plugin.trackmate.tracking import LAPUtils 
from ij import IJ 
import fiji.plugin.trackmate.visualization.hyperstack.HyperStackDisplayer as HyperStackDisplayer 
import fiji.plugin.trackmate.features.FeatureFilter as FeatureFilter 
import sys 
import fiji.plugin.trackmate.features.track.TrackDurationAnalyzer as TrackDurationAnalyzer 
 
import copy 
from fiji.plugin.trackmate import SpotCollection 
from fiji.plugin.trackmate import Spot 
import fiji.plugin.trackmate.features.ModelFeatureUpdater as ModelFeatureUpdater 
import fiji.plugin.trackmate.features.SpotFeatureCalculator as SpotFeatureCalculator 
import fiji.plugin.trackmate.features.spot.SpotContrastAndSNRAnalyzerFactory as SpotContrastAndSNRAnalyzerFactory 
import fiji.plugin.trackmate.features.spot.SpotIntensityAnalyzerFactory as SpotIntensityAnalyzerFactory 
import fiji.plugin.trackmate.features.spot.SpotRadiusEstimatorFactory as SpotRadiusEstimatorFactory 
import fiji.plugin.trackmate.features.spot.SpotIntensityAnalyzer as SpotIntensityAnalyzer 
import fiji.plugin.trackmate.features.spot.SpotMorphologyAnalyzerFactory as SpotMorphologyAnalyzerFactory 
import fiji.plugin.trackmate.features.track.TrackSpeedStatisticsAnalyzer as TrackSpeedStatisticsAnalyzer 
 
import ij.gui.OvalRoi as OvalRoi 
import ij.gui.WaitForUserDialog as WaitForUserDialog 
import ij.plugin.frame.RoiManager as RoiManager 
import ij.ImageJ as ImageJ 
from ij.gui import DialogListener, GenericDialog

import fiji.plugin.trackmate.gui.panels.detector.LogDetectorConfigurationPanel as LogDetectorConfigurationPanel
import javax.swing.JFrame as JFrame
import javax.swing.JPanel as JPanel
import javax.swing.JDialog as JDialog
import javax.swing.JButton as JButton
import java.awt.BorderLayout as BorderLayout
import java.awt.Dimension as Dimension 
import javax.swing.Box as Box
import javax.swing.BoxLayout as BoxLayout
import java.awt.Component as Component
import fiji.util.NumberParser as NumberParser
import fiji.plugin.trackmate.gui.panels.tracker.SimpleLAPTrackerSettingsPanel as SimpleLAPTrackerSettingsPanel 
import fiji.plugin.trackmate.gui.panels.tracker.LAPTrackerSettingsPanel as LAPTrackerSettingsPanel 
import java.awt.GridBagConstraints as GridBagConstraints
import java.awt.GridBagLayout as GridBagLayout
import java.awt.Insets as Insets
import javax.swing.JLabel as JLabel
import javax.swing.SwingConstants as SwingConstants 
import fiji.plugin.trackmate.gui.panels.components.JNumericTextField as JNumericTextField
import fiji.plugin.trackmate.gui.TrackMateWizard.BIG_FONT as BIG_FONT
import fiji.plugin.trackmate.gui.TrackMateWizard.FONT as FONT
import fiji.plugin.trackmate.gui.TrackMateWizard.TEXTFIELD_DIMENSION as TEXTFIELD_DIMENSION
import java.awt.Font as Font

global panelTracker, radiusSpot, radiusForBGEstimation, radiusSafety, linkingDistance, thresholdDiameterRatio, minSquareDistance, threshold, minNbSpotsPerTrack, maxRelativeNbBadSpotsPerTrack, settings
global panelOtherParameters
#panelOtherParameters=None;


if 1: #regular param 
	radiusSpot=.3
	#radiusSpot=.455 
	radiusForBGEstimation=.13 
	radiusSafety=.05
	#radiusSafety=0.
	linkingDistance=.5 
	thresholdDiameterRatio=1.5 # NOT USED RIGHT NOW: will reject spots whose estimated diameter is larger than thresholdDiameterRatio*2*radiusSpot 
	threshold=100. 
	minNbSpotsPerTrack=4 
	maxRelativeNbBadSpotsPerTrack=.3 

if 0: # test params
	radiusSpot=.3
	radiusForBGEstimation=0. 
	radiusSafety=0.
	linkingDistance=0. 
	thresholdDiameterRatio=0 # will reject spots whose estimated diameter is larger than thresholdDiameterRatio*2*radiusSpot 
	threshold=100. 
	minNbSpotsPerTrack=0 
	maxRelativeNbBadSpotsPerTrack=0. 


#-----------------------------------------------------------
def calculateMinSquareDistance(radiusSpot,radiusForBGEstimation,radiusSafety):
	minSqDist=(2*(radiusSpot+radiusForBGEstimation+radiusSafety))**2 # original
	#minSqDist=(2*radiusSpot+radiusForBGEstimation+radiusSafety)**2
	return minSqDist

#def performMyTracking(imp,model,settings,radiusForBGEstimation,radiusSafety,thresholdDiameterRatio,minSquareDistance,minNbSpotsPerTrack,maxRelativeNbBadSpotsPerTrack): 
def performMyTracking(minSquareDistance): 
	# Main function that performs tracking using Trackmate library

	#print(str(radiusSpot))
	#print(str(minSquareDistance))
	#print(str(settings.detectorSettings.get('DO_SUBPIXEL_LOCALIZATION')))
	#print(str(settings.detectorSettings.get('TARGET_CHANNEL')))
	#print(str(settings.detectorSettings.get('DO_SUBPIXEL_LOCALIZATION')))
	#print(str(settings.detectorSettings.get('DO_MEDIAN_FILTERING')))
	
	trackmate = TrackMate(model, settings) 
	nbIsolated=0
	nbMissingFluorescence=0
	nbNonVisible=0	   
			
	#-------- 
	# Process 
	#-------- 
		 
	ok = trackmate.checkInput() 
	if not ok: 
		sys.exit(str(trackmate.getErrorMessage())) 
		 
	ok = trackmate.process() 
	if not ok: 
		sys.exit(str(trackmate.getErrorMessage())) 
	 
	#---------------------------------- 
	# Display results on original image  
	#---------------------------------- 
	
	class MyListener (DialogListener):
		def dialogItemChanged(self, gd, event):
			IJ.log("Cleaning up the tracks...")
 
	spots=model.getSpots() 

	#------------------------------------- 
	# Tag spots too close to each other and spots with weird estimated diameters or shapes or contrasts or SNR 
	#-------------------------------------  
	if 1: 
		model.getLogger().log('  Removing spots too close to each other...')
		model.beginUpdate() 
		for frame in range(0, imp.getNFrames()): 
			#frame=0 
			#model.getLogger().log('Frame '+ str(i)) 
			spotIterator=spots.iterator( frame, 0 ) 
			#nTot=0 
			#nClose=0 
			while spotIterator.hasNext(): 
				curSpot=spotIterator.next() 
				#if (!(curSpot.getFeature('ISOLATED')) or ((curSpot.getFeature('ISOLATED')) and (curSpot.getFeature('ISOLATED')!=0))): 
				if curSpot.getFeature('VISIBILITY')==True: 
					#print(curSpot.getFeature('ESTIMATED_DIAMETER')) 
					##if curSpot.getFeature('ESTIMATED_DIAMETER')>thresholdDiameterRatio*2*radiusSpot: # Removes spots with a very large estimated diameter 
					##	curSpot.putFeature( 'WRONG_DIAMETER', 0 ) 
					##	curSpot.putFeature( 'VISIBILITY', 0) 
					#if curSpot.getFeature('aspectratio')> 
					snr=curSpot.getFeature('SNR') 
					qual=curSpot.getFeature('QUALITY') 
					#print(str(snr)) 
					#curSpotFeatures=curSpot.getFeatures() 
					#for f in curSpotFeatures: 
					#	model.getLogger().log('+++' + str(f) + '=' + str(curSpot.getFeature(f))) 
					nSpots=10 
					listCloseSpots=spots.getNClosestSpots(curSpot, frame, nSpots, 0 ) 
					it = listCloseSpots.iterator() 
					while (it.hasNext()): 
						curNeighbor=it.next() 
						if curNeighbor.ID != curSpot.ID and curNeighbor.getFeature('VISIBILITY')==True: 
							squareDist=curSpot.squareDistanceTo( curNeighbor ) 
							if squareDist<minSquareDistance: 
								curNeighbor.putFeature( 'ISOLATED', 0 ) 
								curNeighbor.putFeature( 'VISIBILITY', 0 ) 
								curSpot.putFeature( 'ISOLATED', 0 ) 
								curSpot.putFeature( 'VISIBILITY', 0 ) 
								nbIsolated=nbIsolated+2
								nbNonVisible=nbNonVisible+2
								#curSpotFeatures=curSpot.getFeatures() 
		model.endUpdate() 
	 
	#----------------------------------------------------------------- 
	# Tag spots of patches whose fluorescence spans more than 3 slices 
	#----------------------------------------------------------------- 
	if 1:
		model.getLogger().log('  Removing spots missing fluorescence...')
		# open hyperstack 
		#impHS = IJ.openImage(imageOriginalPath+imageHyperstackFileName) 
		impHS = IJ.openVirtual(imageOriginalPath+imageHyperstackFileName) 
		impHS.show()  
		model.beginUpdate() 
	 
		nSlices=impHS.getNSlices() 

		settings.setFrom(impHS) # tells tracker to work with HS 
		calibrationHS=impHS.getLocalCalibration() 
		#calibrationHS=imp.getLocalCalibration() 
		lastFrame=imp.getNFrames() 
	
		# Do it only if the movie contains more than 3 z-slices
		if nSlices>2: 	
			#Duplicate spots to all z-slices 
			for frame in range(0, lastFrame ): 
				newSpotCollection=SpotCollection() 
				spotIterator=spots.iterator( frame, 0 ) 
				while spotIterator.hasNext(): 
					curSpot=spotIterator.next() 
					if curSpot.getFeature('POSITION_Z')==0 and curSpot.getFeature('VISIBILITY')==True: 
						for z in range(1, nSlices): 
							newSpot=Spot(curSpot) 
							newSpot.putFeature('POSITION_Z', calibrationHS.getZ(z)) 
							newSpotCollection.add(newSpot,frame) 
				newSpotIterator=newSpotCollection.iterator( frame, 0 ) 
				while newSpotIterator.hasNext(): 
					model.addSpotTo(newSpotIterator.next(),frame) 
			 
			# Update all spot features values 
			trackmate.computeSpotFeatures(1) 
			model.endUpdate() 
		 
			# Perform statistics on relative intensities on different slices 
			visibleSpotOnly=0; # 1 if disregarding non-visible spots 
			for frame in range(0, lastFrame ): 
				spotIterator=spots.iterator( frame, 0 ) 
				while spotIterator.hasNext(): 
					curSpot=spotIterator.next() 
					if curSpot.getFeature('POSITION_Z')==0 and curSpot.getFeature('VISIBILITY')==True: # only look at isolated spots  
						spotsOnSlices=[None]*nSlices 
						intensities=[None]*nSlices 
						relativeIntensities=[None]*nSlices 
						spotsOnSlices[0]=curSpot 
						intensities[0]=curSpot.getFeature('TOTAL_INTENSITY') 
						maxValue=intensities[0] 
						indexMax=0 
						for z in range(1, nSlices): 
							newSpot=Spot(curSpot) 
							newSpot.putFeature('POSITION_Z', calibrationHS.getZ(z)) 
							spotsOnSlices[z]=spots.getClosestSpot(newSpot, frame, visibleSpotOnly ) 
							intensities[z]=spotsOnSlices[z].getFeature('TOTAL_INTENSITY') 
							if intensities[z]>maxValue: 
								indexMax=z 
								maxValue=intensities[z] 
						if indexMax==0 or indexMax==nSlices-1: # reject if max fluorescence on top or bottom slice 
							for z in range(0, nSlices): 
								spotsOnSlices[z].putFeature( 'ALL_INTENSITY_MEASURED', 0 ) 
								spotsOnSlices[z].putFeature( 'VISIBILITY', 0 )
							nbNonVisible=nbNonVisible+1
							nbMissingFluorescence=nbMissingFluorescence+1
			 
	#----------------------------------------------------------------- 
	# Remove tracks with too many bad spots 
	#----------------------------------------------------------------- 
	if 1: 
		model.getLogger().log('  Removing tracks with too many bad spots...')
		model.beginUpdate() 
	
		nbGoodTrack=0
		for idTrack in model.getTrackModel().trackIDs(True):
			track = model.getTrackModel().trackSpots(idTrack) 
			cnt=0 
			cntBad=0 
			for spot in track: 
				cnt=cnt+1 
				if spot.getFeature('VISIBILITY')==0: 
					cntBad=cntBad+1 
			if ((cnt-cntBad<minNbSpotsPerTrack) or (float(cntBad)/float(cnt)>maxRelativeNbBadSpotsPerTrack)): 
				model.setTrackVisibility(idTrack,False)
			else:
				nbGoodTrack=nbGoodTrack+1
	
		model.endUpdate() 
		nbVisibleTracks=(model.getTrackModel().trackIDs(True)).size()
		nbAllTracks=(model.getTrackModel().trackIDs(False)).size()
		model.getLogger().log('  '+str(nbGoodTrack)+' tracks out of '+str(nbAllTracks)+' are still good!')
	
	#---------------- 
	# Close hyperstack 
	#---------------- 
	impHS.close() 
	 
	#----------------------------------------------------------------- 
	# Transform tracks into Roisets 
	#----------------------------------------------------------------- 

	#model.getLogger().log("settings final r="+str(settings.detectorSettings.get('RADIUS')))

	
	if 1:
		model.getLogger().log('Transforming Trackmate tracks into Roisets...')
		#calibrationHS=impHS.getLocalCalibration() 
		roimanager = RoiManager.getInstance() 
		if roimanager is None: 
			roimanager = RoiManager()
		imp.show()
		cnt=0 
		for idTrack in model.getTrackModel().trackIDs(True): 
			cnt=cnt+1
			roimanager = RoiManager.getInstance() 
			if roimanager is None: 
				roimanager = RoiManager() 
			roimanager.reset() 
			track = model.getTrackModel().trackSpots(idTrack) #list in random order 
			spots=SpotCollection().fromCollection(track) 
			nSpots=spots.getNSpots(False) 
			firstFrame=spots.firstKey() 
			#print(nSpots) 
			for i in range(0,nSpots): 
				spot=spots.iterator(firstFrame+i,False).next() # assumes there is one and only one spot at each frame [no gap allowed] 
				xs=spot.getFeature('POSITION_X') 
				ys=spot.getFeature('POSITION_Y') 
				z=spot.getFeature('POSITION_Z') 
				t=int(round(spot.getFeature('FRAME')))+1 
				radius=radiusSpot 
				x=int(round(calibrationHS.getRawX(xs-radius))) 
				y=int(round(calibrationHS.getRawY(ys-radius))) 
				name=str(t)+'-'+str(y)+'-'+str(x) 
				roi = OvalRoi(x, y, calibrationHS.getRawX(radius)*2, calibrationHS.getRawY(radius)*2) 
				roi.setPosition(0, 0, t) #1st channe, 1st slice, frame t 
				imp.setT(t) # or imp.setSliceWithoutUpdate(t) 
				imp.setRoi(roi) 
				roimanager.add(imp,roi,t) 
				imp.setT(t) # or imp.setSlice(currentSlice) # important to do it again to avoid random crashes 
				if spot.getFeature('VISIBILITY')==False: # tag as bad if spot is not visible 
					index=roimanager.getCount()-1 
					roimanager.select(index) 
					newName=roimanager.getName(index)+'#x' 
					roimanager.runCommand("Rename",newName) 
			nRois=roimanager.getCount() 
			# Create Rois for background 
			radiusForBGEstimationInPx=calibrationHS.getRawX(radiusForBGEstimation); 
			for i in range(0,nRois): 
				roi=roimanager.getRoi(i).clone() 
				h=roi.getFloatHeight()+2*radiusForBGEstimationInPx 
				w=roi.getFloatWidth()+2*radiusForBGEstimationInPx 
				x=roi.getXBase()-radiusForBGEstimationInPx  
				y=roi.getYBase()-radiusForBGEstimationInPx 
				t=roi.getTPosition() 
				roiBG = OvalRoi(x, y, h, w) 
				roi.setPosition(0, 0, t) #1st channel, 1st slice, frame t 
				imp.setT(t) # or imp.setSliceWithoutUpdate(t) 
				imp.setRoi(roiBG) 
				roimanager.add(imp,roiBG,t) 
				imp.setT(t) 
				index=roimanager.getCount()-1 
				roimanager.select(index) 
				newName=roimanager.getName(i)+'BG' 
				roimanager.runCommand("Rename",newName) 
			roimanager.runCommand("Deselect"); # deselect ROIs to save them all
			roimanager.runCommand("Save", imageOriginalPath + "RoiSetAuto-"+str(cnt)+".zip"); 
			roimanager.runCommand("Select All");  
			roimanager.runCommand("Delete");  
			#imp.show(); 

		model.getLogger().log('  '+str(cnt)+' tracks saved...')
	
	 	 
	#---------------------------------- 
	# Display results on original image  
	#---------------------------------- 
	if 1:  
		selectionModel = SelectionModel(model) 
		displayer =  HyperStackDisplayer(model, selectionModel, imp) 
		displayer.render() 
		displayer.refresh() 
	 
	model.getLogger().log(str(nbIsolated)+" spots are not isolated")
	model.getLogger().log(str(nbMissingFluorescence)+" are not entirely captured")
	model.getLogger().log(str(nbNonVisible)+" spots are not visible")
	
	model.getLogger().log('Finished!') 

	frame0.dispose()
 	return;
#---------------------------------------------------





# Get currently selected image 
#imp = WindowManager.getCurrentImage() 
imp = IJ.getImage() 
#initialImage="C:\Users\jb246\Desktop\Joel's data\2015-06-04 fimbrin-GFP + linkers  (take 2) 6 slices 0.5 z step\Analyzed\2015-06-04 Sp210 488 1-cropped\2015-06-04 Sp210 488 1_SUM_Corrected.tif" 
minSquareDistance=calculateMinSquareDistance(radiusSpot,radiusForBGEstimation,radiusSafety) # min square distance between 2 spots on the same frame 
 
############## ADDED FOR ECLIPSE 
#imagejBar=ImageJ() 
#imagejBar.setVisible(True)  
################################# 
 
imp.show() 
 
# Get the image path and corresponding hyperstack name 
imageOriginalPath=imp.getOriginalFileInfo().directory 
imageOriginalFileName=imp.getOriginalFileInfo().fileName 

############## ADDED FOR ECLIPSE 
#imageOriginalPath=imageOriginalPath[:-1] # Might have to remove this line (imageOriginalPath has extra fileseparators for some reason when macro run under Eclipse) 
###################################### 

extensionProjectionName='_SUM_Corrected' 
extensionHyperstackName='HS' 
imageHyperstackFileName=imageOriginalFileName[:-len(extensionProjectionName+'.tif')]+extensionHyperstackName+'.tif' 
 
#---------------------------- 
# Create the model object now 
#---------------------------- 
# Some of the parameters we configure below need to have 
# a reference to the model at creation. So we create an 
# empty model now. 
	 
model = Model() 

calibration=imp.getLocalCalibration();
	 
# Send all messages to ImageJ log window. 
model.setLogger(Logger.IJ_LOGGER) 
model.setPhysicalUnits(calibration.getUnit(), calibration.getTimeUnit())	 
		
#------------------------ 
# Prepare settings object 
#------------------------ 
settings = Settings() 
settings.setFrom(imp) 
 
# Configure detector 
settings.detectorFactory = LogDetectorFactory() 
settings.detectorSettings = {  
	'DO_SUBPIXEL_LOCALIZATION' : True, 
	'RADIUS' : radiusSpot, 
	'TARGET_CHANNEL' : 1, 
	'THRESHOLD' : threshold, 
	'DO_MEDIAN_FILTERING' : False, 
}   
	 
# Configure spot filters - Classical filter on quality 
filter1 = FeatureFilter('QUALITY', 1, True) 
settings.addSpotFilter(filter1) 
	  
# Configure tracker  
settings.trackerFactory = SparseLAPTrackerFactory() 
settings.trackerSettings = LAPUtils.getDefaultLAPSettingsMap() # almost good enough 
settings.trackerSettings['ALLOW_TRACK_SPLITTING'] = False 
settings.trackerSettings['ALLOW_TRACK_MERGING'] = False 
settings.trackerSettings['ALLOW_GAP_CLOSING'] = False 
settings.trackerSettings['MERGING_MAX_DISTANCE'] = linkingDistance 
settings.trackerSettings['GAP_CLOSING_MAX_DISTANCE'] = linkingDistance 
settings.trackerSettings['MAX_FRAME_GAP'] = 0  
settings.trackerSettings['LINKING_MAX_DISTANCE'] = linkingDistance 
 
 
	 
# Configure track analyzers - Later on we want to filter out tracks  
# based on their displacement, so we need to state that we want  
# track displacement to be calculated. By default, out of the GUI,  
# not features are calculated.  
	 
# The displacement feature is provided by the TrackDurationAnalyzer. 
settings.addSpotAnalyzerFactory(SpotIntensityAnalyzerFactory()) 
#settings.addSpotAnalyzerFactory(SpotContrastAndSNRAnalyzerFactory())	 
settings.addSpotAnalyzerFactory(SpotRadiusEstimatorFactory()) 
#settings.addSpotAnalyzerFactory(SpotMorphologyAnalyzerFactory()) 
#settings.addTrackAnalyzer(TrackDurationAnalyzer()) 
#settings.addTrackAnalyzer(TrackSpeedStatisticsAnalyzer()) 
 
#settings.initialSpotFilterValue = 1 
	 
# Configure track filters - We want to get rid of the two immobile spots at  
# the bottom right of the image. Track displacement must be above 10 pixels. 
	 
#filter2 = FeatureFilter('TRACK_DISPLACEMENT', 10, True) 
#settings.addTrackFilter(filter2) 
	 

#------------------- 
### New config window
#------------------- 
def changePanel(self): # Change the main panel with a new one
	# NOTE: This function is currently not used but could be useful if we jump the tracker configuration panel
	global settings
	settings.detectorSettings = {  
		'DO_SUBPIXEL_LOCALIZATION' : panelConfig.jCheckSubPixel.isSelected(), 
		'RADIUS' : NumberParser.parseDouble(panelConfig.jTextFieldBlobDiameter.getText())/2, 
		'TARGET_CHANNEL' : 1, 
		'THRESHOLD' : NumberParser.parseDouble(panelConfig.jTextFieldThreshold.getText()), 
		'DO_MEDIAN_FILTERING' : panelConfig.jCheckBoxMedianFilter.isSelected(), 
	} 
	newPanel=JPanel()
	frame0.getContentPane().remove(0)
	#frame0.pack()
	#frame0.show()
	#frame0.setVisible(True)
	frame0.getContentPane().add( newPanel, BorderLayout.CENTER, 0 )	
	frame0.repaint()
	frame0.validate()
	frame0.show()
	nextButton.actionPerformed = myCancel #performMyTracking

def goToLinkingPanel(self): # Recovers detector parameters from the first panel and build a new panel using Tackmates tracker panels
	global settings, panelConfig,radiusSpot, threshold
	settings.detectorSettings = panelConfig.settings
	radiusSpot=settings.detectorSettings.get('RADIUS')
	threshold=settings.detectorSettings.get('THRESHOLD')
	#model.getLogger().log("settings r="+str(settings.detectorSettings.get('RADIUS')))
	##panelTracker=SimpleLAPTrackerSettingsPanel("Simple LAP tracker", "Info text", calibration.getUnit())
	#panelSettings={
	#	settings.trackerSettings['GAP_CLOSING_MAX_DISTANCE'] = linkingDistance 
	#	settings.trackerSettings['MAX_FRAME_GAP'] = 0  
	#	settings.trackerSettings['LINKING_MAX_DISTANCE'] = linkingDistance 
	#}
	panelTracker.setSettings(settings.trackerSettings)
	frame0.getContentPane().remove(0)
	frame0.getContentPane().add( panelTracker, BorderLayout.CENTER, 0 )	
	#frame0.pack()
	#frame0.setVisible(True)
	frame0.repaint()
	frame0.validate()
	frame0.show()
	nextButton.actionPerformed = goToOtherParameterPanel 

class otherParameterPanelBuilder(object): # Creates a panel asking for extra tracking parameters (to remove patches too close to each other and with missing fluorescence)
	## Class attributes:
	#jTextFieldRadiusForBGEstimation
	#jTextFieldRadiusSafety
	#jTextFieldMinNbSpotsPerTrack
	#jTextFieldMaxRelativeNbBadSpotsPerTrack
	#panel
		
	def __init__(self): # main class, called when buildign a new object (inspired by Trackmate's SimpleLAPTrackerSettingsPanel)
		spaceUnits=calibration.getUnit()
		newPanel=JPanel()
		newPanel.setPreferredSize(Dimension(300, 500))
		thisLayout = GridBagLayout()
		thisLayout.rowWeights = [ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0]
		thisLayout.rowHeights = [31, 50, 119, 7, 50, 50, 50, 50, 50]
		thisLayout.columnWeights = [0.0, 0.0, 0.1]
		thisLayout.columnWidths = [203, 42, 7]
		newPanel.setLayout(thisLayout);
		# Header text
		jLabel1 =  JLabel();
		newPanel.add(jLabel1,  GridBagConstraints(0, 0, 3, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,  Insets(0, 10, 0, 10), 0, 0));
		jLabel1.setFont(FONT);
		jLabel1.setText("");
		# Tracker Name
		jLabelTrackerName =  JLabel();
		newPanel.add(jLabelTrackerName,  GridBagConstraints(0, 1, 3, 1, 0.0, 0.0, GridBagConstraints.LINE_START, GridBagConstraints.NONE,  Insets(10, 20, 0, 0), 0, 0));
		jLabelTrackerName.setHorizontalTextPosition(SwingConstants.CENTER);
		jLabelTrackerName.setHorizontalAlignment(SwingConstants.CENTER);
		jLabelTrackerName.setFont(BIG_FONT);
		jLabelTrackerName.setText("Additional parameters for patch selection:");
		# Infos
		jLabelTrackerDescription =  JLabel();
		newPanel.add(jLabelTrackerDescription,  GridBagConstraints(0, 2, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,  Insets(10, 10, 10, 10), 0, 0));
		jLabelTrackerDescription.setFont(FONT.deriveFont(Font.ITALIC));
		infoText="<html> Spots too close to each other or missing fluorescnece will not be measured."
		infoText=infoText+"Width for background estimation: width of the donut that will be used to estimate the background fluorescence aroung the spots."
		infoText=infoText+"Extra safety distance: extra space required between two spots and their background donut."
		infoText=infoText+"Min measurements per track: minimum size of a track to be saved."
		infoText=infoText+"Max ratio of rejected points: maximum proportion of bad spots (too close to another one, missing fluorescence, etc) that are allowed in a track, otherwise the track is rejected.</html>"
		jLabelTrackerDescription.setText(infoText
				#.replace("<br>", "")
				.replace("<p>", "<p align=\"justify\">")
				.replace("<html>", "<html><p align=\"justify\">"));
		# 1st param text
		jLabel2 =  JLabel();
		newPanel.add(jLabel2,  GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,  Insets(0, 10, 0, 0), 0, 0));
		jLabel2.setFont(FONT);
		jLabel2.setText("Width for background estimation:");
		#jLabel2.setText("Width bg estim:");
		# 2nd param text
		jLabel3 =  JLabel();
		newPanel.add(jLabel3,  GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,  Insets(0, 10, 0, 0), 0, 0));
		jLabel3.setFont(FONT);
		jLabel3.setText("Extra safety distance:");
		#jLabel3.setText("safety:");
		# 3rd param text
		jLabel4 =  JLabel();
		newPanel.add(jLabel4,  GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,  Insets(0, 10, 0, 0), 0, 0));
		jLabel4.setFont(FONT);
		jLabel4.setText("Min measurements per track:");
		#jLabel4.setText("Min measur:");
		# 4th param text
		jLabel5 =  JLabel();
		newPanel.add(jLabel5,  GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,  Insets(0, 10, 0, 0), 0, 0));
		jLabel5.setFont(FONT);
		jLabel5.setText("Max ratio of rejected points:");
		#jLabel5.setText("Max ratio:");
		# 1st param value
		self.jTextFieldRadiusForBGEstimation =  JNumericTextField(radiusForBGEstimation);
		self.jTextFieldRadiusForBGEstimation.setMinimumSize(TEXTFIELD_DIMENSION);
		newPanel.add(self.jTextFieldRadiusForBGEstimation,  GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,  Insets(0, 0, 0, 0), 0, 0));
		self.jTextFieldRadiusForBGEstimation.setFont(FONT);
		# 2nd param value
		self.jTextFieldRadiusSafety =  JNumericTextField(radiusSafety);
		self.jTextFieldRadiusSafety.setMinimumSize(TEXTFIELD_DIMENSION);
		newPanel.add(self.jTextFieldRadiusSafety,  GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,  Insets(0, 0, 0, 0), 0, 0));
		self.jTextFieldRadiusSafety.setFont(FONT);
		# 3rd param value
		self.jTextFieldMinNbSpotsPerTrack =  JNumericTextField(str(minNbSpotsPerTrack));
		self.jTextFieldMinNbSpotsPerTrack.setMinimumSize(TEXTFIELD_DIMENSION);
		#jTextFieldMinNbSpotsPerTrack.setMinimumSize(TEXTFIELD_DIMENSION);
		newPanel.add(self.jTextFieldMinNbSpotsPerTrack,  GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,  Insets(0, 0, 0, 0), 0, 0));
		self.jTextFieldMinNbSpotsPerTrack.setFont(FONT);
		# 4th param value
		self.jTextFieldMaxRelativeNbBadSpotsPerTrack =  JNumericTextField(maxRelativeNbBadSpotsPerTrack);
		self.jTextFieldMaxRelativeNbBadSpotsPerTrack.setMinimumSize(TEXTFIELD_DIMENSION);
		newPanel.add(self.jTextFieldMaxRelativeNbBadSpotsPerTrack,  GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,  Insets(0, 0, 0, 0), 0, 0));
		self.jTextFieldMaxRelativeNbBadSpotsPerTrack.setFont(FONT);
		# 1st param unit
		jLabelRadiusForBGEstimation =  JLabel();
		newPanel.add(jLabelRadiusForBGEstimation,  GridBagConstraints(2, 4, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,  Insets(0, 0, 0, 10), 0, 0));
		jLabelRadiusForBGEstimation.setFont(FONT);
		jLabelRadiusForBGEstimation.setText(spaceUnits);
		# 2nd param unit
		jLabelRadiusSafety =  JLabel();
		newPanel.add(jLabelRadiusSafety,  GridBagConstraints(2, 5, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,  Insets(0, 0, 0, 10), 0, 0));
		jLabelRadiusSafety.setFont(FONT);
		jLabelRadiusSafety.setText(spaceUnits);
		# 3rd param unit
		jLabelMinNbSpotsPerTrack =  JLabel();
		newPanel.add(jLabelMinNbSpotsPerTrack,  GridBagConstraints(2, 6, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,  Insets(0, 0, 0, 10), 0, 0));
		jLabelMinNbSpotsPerTrack.setFont(FONT);
		# 4th param unit
		jLabelMaxRelativeNbBadSpotsPerTrack =  JLabel();
		newPanel.add(jLabelMaxRelativeNbBadSpotsPerTrack,  GridBagConstraints(2, 7, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL,  Insets(0, 0, 0, 10), 0, 0));
		jLabelMaxRelativeNbBadSpotsPerTrack.setFont(FONT);
	
		#print('inside='+str(self.jTextFieldRadiusForBGEstimation.getValue()));
		self.panel=newPanel
		
def goToOtherParameterPanel(self): # Recovers the parameters from the Tracker panel and replaces this panel with a new panel for the extra tracking parameters
	global panelOtherParameters, settings, linkingDistance
	settings.trackerSettings = panelTracker.settings
	#linkingDistance=settings.trackerSettings.get('GAP_CLOSING_MAX_DISTANCE')
	linkingDistance=settings.trackerSettings.get('LINKING_MAX_DISTANCE')
	
	panelOtherParameters=otherParameterPanelBuilder()
	frame0.getContentPane().remove(0)	
	#frame0.pack()
	#frame0.show()
	#frame0.setVisible(True)
	frame0.getContentPane().add(panelOtherParameters.panel, BorderLayout.CENTER, 0 )	
	frame0.repaint()
	frame0.validate()
	frame0.show()
	nextButton.setText("Run");
	nextButton.actionPerformed = actionPerformMyTracking #performMyTracking


def actionPerformMyTracking(self): # Recovers extra paramters and calls the tracking function
	global radiusForBGEstimation,radiusSafety,minNbSpotsPerTrack,maxRelativeNbBadSpotsPerTrack,minSquareDistance
	global panelOtherParameters, settings
	
	# 1st param value
	radiusForBGEstimation=panelOtherParameters.jTextFieldRadiusForBGEstimation.getValue() 
	# 2nd param value
	radiusSafety=panelOtherParameters.jTextFieldRadiusSafety.getValue() 
	# 3rd param value
	minNbSpotsPerTrack=panelOtherParameters.jTextFieldMinNbSpotsPerTrack.getValue() 
	# 4th param value
	maxRelativeNbBadSpotsPerTrack=panelOtherParameters.jTextFieldMaxRelativeNbBadSpotsPerTrack.getValue() 
	
	minSquareDistance=calculateMinSquareDistance(radiusSpot,radiusForBGEstimation,radiusSafety)
	
	#print(str(radiusForBGEstimation))
	#print(str(radiusSafety))
	#print(str(minNbSpotsPerTrack))
	#print(str(maxRelativeNbBadSpotsPerTrack))
	performMyTracking(minSquareDistance)

def myCancel(self): # Closes the GUI when cancel button is clicked
	frame0.dispose()


imp.show();

#---------------------------
# Creates the main GUI frame
#---------------------------
frame0 = JFrame("Patch Tracker setup")
frame0.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE) 

textInfoLogDetector="<html> Input the parameters for Trackmate's LoG detector </html>"
panelConfig=LogDetectorConfigurationPanel(settings, model, textInfoLogDetector, "Log Detector")
textInfoSimpleLAPTracker="<html>Input the parameters for Trackmate's 'simple LAP tracker'. Note that track splitting and merging are not allowed.</html>";
panelTracker=SimpleLAPTrackerSettingsPanel("Simple LAP tracker", textInfoSimpleLAPTracker, calibration.getUnit())
panelConfig.setSettings(settings.detectorSettings);
panelConfig.setOpaque(True) #content panes must be opaque

jPanelButtons = JPanel()
jPanelButtons.setLayout(BoxLayout(jPanelButtons, BoxLayout.X_AXIS))
frame0.getContentPane().add( panelConfig, BorderLayout.CENTER,0 )
frame0.getContentPane().add( jPanelButtons, BorderLayout.SOUTH, 1 )
jPanelButtons.setAlignmentX(Component.CENTER_ALIGNMENT)
nextButton = JButton("Next")
jPanelButtons.add(nextButton) #, BorderLayout.CENTER);
#jPanelButtons.add( Box.createHorizontalGlue() )			
#nextButton.setBounds(220, 2, 73, 25);
nextButton.actionPerformed = goToLinkingPanel
###nextButton.actionPerformed = changePanel
cancelButton = JButton("Cancel")
jPanelButtons.add(cancelButton) #, BorderLayout.EAST)
cancelButton.actionPerformed = myCancel
#jPanelButtons.add( Box.createHorizontalGlue() )			
#frame0.add(cancelButton, BorderLayout.EAST);
#cancelButton.actionPerformed = frame0.dispose 

# Display the window.
frame0.pack();
frame0.setVisible(True);
	
# Display the preview on the image
selectionModel = SelectionModel(model) 
displayer =  HyperStackDisplayer(model, selectionModel, imp) 
displayer.render() 
displayer.refresh()

